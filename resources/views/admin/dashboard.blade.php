@extends('admin.admin')
@section('content')
<div class="container-fluid" >

  <h2> Hello {{ Session::get('admin')->company_name}} admin</h2>
   <div class="row">
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
         <a href="https://saifagrogroup.com/admin/user-list">
            <div class="iq-card-body">
                  <div class="top-block d-flex align-items-center justify-content-between">
                     <h5>All Customers</h5>
                  </div>
             
               <h3><span class="counter">{{App\Customers::count()}}</span></h3>
            </div>
           </a>  
         </div>
      </div>
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            <div class="iq-card-body">
               <div class="top-block d-flex align-items-center justify-content-between">
                  <h5>Pending Customers</h5>
               </div>
               <h3><span class="counter">{{App\Customers::where(['status'=>0])->count()}}</span></h3>
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            <div class="iq-card-body">
               <div class="top-block d-flex align-items-center justify-content-between">
                  <h5>Pending Orders</h5>
               </div>
               <h3><span class="counter">{{App\Orders::where(['company_id'=>Session::get('admin')->id,'payment_status'=>'pending'])->count()}}</span></h3>
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            <a href="https://saifagrogroup.com/admin/order-list">
            <div class="iq-card-body">
               <div class="top-block d-flex align-items-center justify-content-between">
                  <h5>Paid Orders</h5>
               </div>
               <h3><span class="counter">{{App\Orders::where(['company_id'=>Session::get('admin')->id,'payment_status'=>'paid'])->count()}}</span></h3>
            </div>
         </a>
         </div>
      </div>
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            
            <a href="https://saifagrogroup.com/admin/company-list">
            <div class="iq-card-body">
               <div class="top-block d-flex align-items-center justify-content-between mt-1">
                  <h5>Companies</h5>
               </div>
               <h3><span class="counter">{{App\Companies::count()}}</span></h3>
            </div>
         </a>
         </div>
      </div>
      <div class="col-md-4">
         <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
            <a href="https://saifagrogroup.com/admin/product-list">
            <div class="iq-card-body">
               <div class="top-block d-flex align-items-center justify-content-between">
                  <h5>Products</h5>
               </div>
               <h3><span class="counter">{{App\Products::where(['company_id'=>Session::get('admin')->id])->count()}}</span></h3>
            </div>
         </a>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
