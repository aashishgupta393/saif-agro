<div class="iq-sidebar">
   <div class="iq-navbar-logo d-flex justify-content-between">
      <a href="{{url('/admin/')}}" class="header-logo">
      <img src="{{ URL::asset('resources/views/storefront/main_logo.png') }}" class="img-fluid rounded" alt="">
      </a>
      <div class="iq-menu-bt align-self-center">
         <div class="wrapper-menu">
            <div class="main-circle"><i class="ri-menu-line"></i></div>
            <div class="hover-circle"><i class="ri-close-fill"></i></div>
         </div>
      </div>
   </div>
   <div id="sidebar-scrollbar">
      <nav class="iq-sidebar-menu">
         <ul id="iq-sidebar-toggle" class="iq-menu">
           <li class="<?=(($page == 'dashboard') ? "active" : '')?>" >
              <a href="{{url('/admin/')}}" class="iq-waves-effect">
                <i class="las la-home iq-arrow-left"></i><span>Dashboard</span>
              </a>
           </li>
            <li class="<?=(($page == 'company-list') ? "active" : '')?>">
               <a href="#dashboard" class="iq-waves-effect" data-toggle="collapse"
               aria-expanded="<?=(($page == 'company-list') ? 'true' : 'false')?>"
               >
                 <span class="ripple rippleEffect"></span>
                 <i class="las la-building iq-arrow-left"></i>
                 <span>Companies</span>
                 <i class="ri-arrow-right-s-line iq-arrow-right"></i>
               </a>
               <ul id="dashboard" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                  </li>
                  <li class="active">
                    <a href="{{url('/admin/company-list')}}">
                      <i class="las la-th-list"></i>Companies List
                    </a>
                  </li>
               </ul>
            </li>
            <li class="<?=(($page == 'product') ? 'active-menu active' : '')?>" aria-expanded="true">
               <a href="#menu-product" class="iq-waves-effect" data-toggle="collapse"
                aria-expanded="false"    aria-expanded="<?=(($page == 'product') ? 'true' : 'false')?>"
                >
                 <i class="fa fa-product-hunt iq-arrow-left"></i><span>Products</span>
                 <i class="ri-arrow-right-s-line iq-arrow-right"></i>
               </a>
               <ul id="menu-product" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                    
                    <li class="<?=((isset($subpage) && $subpage =='add-product') ? "active active-menu" : "") ?>" >
                    <a href="{{url('/admin/add-product/')}}">
                      <i class="fa  fa-plus"></i>Add Product
                    </a>
                    </li>

                    <li class="<?=((isset($subpage) && $subpage =='import-products') ? "active active-menu" : "") ?>" >
                    <a href="{{url('/admin/upload-product/')}}">
                      <i class="fa fa-upload"></i>Import Product From CSV
                    </a>
                    </li>

                  <li class="<?=((isset($subpage) && $subpage =='products') ? "active active-menu" : "") ?>">
                    <a href="{{url('/admin/product-list/')}}">
                      <i class="las la-th-list"></i>Products
                    </a>
                  </li>
                  <li class="<?=((isset($subpage) && $subpage =='upload-image') ? "active active-menu" : "") ?>">
                    <a href="{{url('/admin/upload-image/')}}">
                      <i class="las la-th-list"></i>Upload Product Image
                    </a>
                  </li>

                  <li class="<?=((isset($subpage) && $subpage =='category') ? "active active-menu" : "") ?>">
                  <a href="{{url('/admin/category/')}}">
                      <i class="las la-th-list"></i>
                        Categories
                    </a>
                  </li>
                  <li class="<?=((isset($subpage) && $subpage =='subcategory') ? "active active-menu" : "") ?>">
                    <a href="{{url('/admin/subcategories/')}}">
                        <i class="las la-th-list"></i>
                          Sub Categories
                      </a>
                    </li>
               </ul>
            </li>
            <li  aria-expanded="true">
               <a href="#menu-users" class="iq-waves-effect" data-toggle="collapse" aria-expanded="false">
                 <i class="las la-user-tie iq-arrow-left"></i><span>Users</span>
                 <i class="ri-arrow-right-s-line iq-arrow-right"></i>
               </a>
               <ul id="menu-users" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                  <li><a href="{{url('/admin/user-list')}}"><i class="las la-th-list"></i>User List </a></li>
               </ul>
            </li>
            <li>
               <a href="#order-list" class="iq-waves-effect" data-toggle="collapse" aria-expanded="false">
                 <span class="ripple rippleEffect"></span>
                 <i class="las la-user-tie iq-arrow-left"></i>
                 <span>Orders</span>
                 <i class="ri-arrow-right-s-line iq-arrow-right"></i>
               </a>
               <ul id="order-list" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle" style="">
                  <li><a href="{{url('/admin/order-list')}}"><i class="las la-id-card-alt"></i>Orders</a></li>
               </ul>
            </li>

            <li  aria-expanded="true">
               <a href="#settings-design" class="iq-waves-effect" data-toggle="collapse" aria-expanded="false">
                 <i class="fa fa-gear iq-arrow-left"></i><span>Settings</span>
                 <i class="ri-arrow-right-s-line iq-arrow-right"></i>
               </a>

               <ul id="settings-design" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                  <li>
                    <a  href="{{url('/admin/banner')}}">
                      <i class="fa fa-picture-o"></i>Home Page Banner
                    </a>
                  </li>
               </ul>
            </li>

            <li class="<?=(($page == 'enquiries') ? "active" : '')?>" >
               <a href="{{url('/admin/enquiries')}}" class="iq-waves-effect">
                 <i class="las la-home iq-arrow-left"></i><span>Enquireis</span>
               </a>
            </li>
            <li class="<?=(($page == 'files') ? "active" : '')?>" >
               <a href="{{url('/admin/files')}}" class="iq-waves-effect">
                 <i class="las la-home iq-arrow-left"></i><span>Files</span>
               </a>
            </li>


         </ul>
      </nav>
      <div class="p-3"></div>
   </div>
</div>
