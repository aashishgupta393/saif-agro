@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" rel="stylesheet">
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Registered Users</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="home-tab-two" data-toggle="tab" href="#home-two" role="tab" aria-controls="home" aria-selected="true">
                     Pending
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="profile-tab-two" data-toggle="tab" href="#profile-two" role="tab" aria-controls="profile" aria-selected="false">
                       Approved
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="contact-tab-two" data-toggle="tab" href="#contact-two" role="tab" aria-controls="contact" aria-selected="false">
                       Rejected
                     </a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent-1">
                  <div class="tab-pane fade show active" id="home-two" role="tabpanel" aria-labelledby="home-tab-two">
                     <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Registration Date </th>
                                 <th>Contact Details</th>
                                 <th>Company Name</th>
                                 <th>Address</th>
                                 <th>GST Number</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach(App\Customers::where(['status'=>0])->orderBy('id', 'DESC')->get() as $user)
                              <tr>
                                 <td>{{$user->username}}</td>
                                   <td>{{date('d-m-Y',strtotime($user->created_at))}}</td>
                                 <td>
                                     <p>Email :{{$user->email}}</p>
                                     <p>Contact :{{$user->contact_no}}</p>
                                     <p>WhatsApp no :{{$user->whats_app_no}}</p>
                                  </td>

                                 <td>{{$user->company_address}}</td>
                                 <td>
                                   {{$user->city}},{{$user->state}} ,{{$user->zip_code}}
                                 </td>
                                 <td>{{$user->gst_no}}</td>
                                 <td style="width:20%;">
                                    <select class="form-control user-action" data-user-id="{{$user->id}}">
                                       <option value="">Pending</option>
                                       <option value="1">Approve </option>
                                       <option value="2">Disapprove</option>
                                    </select>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="profile-two" role="tabpanel" aria-labelledby="profile-tab-two">
                     <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Contact Details</th>
                                 <th>Company Address</th>
                                 <th>State</th>
                                 <th>City</th>
                                 <th>Pin code</th>
                                 <th>GST Number</th>
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach(App\Customers::where(['status'=>1])->orderBy('id', 'DESC')->get() as $user)
                              <tr>
                                 <td>{{$user->username}}</td>
                                 <td>
                                     <p>Email :{{$user->email}}</p>
                                     <p>Contact :{{$user->contact_no}}</p>
                                     <p>WhatsApp no :{{$user->whats_app_no}}</p>
                                  </td>
                                 <td>{{$user->company_address}}</td>
                                 <td>{{$user->state}}</td>
                                 <td>{{$user->city}}</td>
                                 <td>{{$user->zip_code}}</td>
                                 <td>{{$user->gst_no}}</td>
                                 <td>Approved</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="contact-two" role="tabpanel" aria-labelledby="contact-tab-two">
                     <div class="table-responsive">
                        <table id="datatable2" class="table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Contact Details</th>
                                 <th>Company Address</th>
                                 <th>State</th>
                                 <th>City</th>
                                 <th>Pin code</th>
                                 <th>GST Number</th>
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach(App\Customers::where(['status'=>2])->orderBy('id', 'DESC')->get() as $user)
                              <tr>
                                 <td>{{$user->username}}</td>
                                 <td>
                                     <p>Email :{{$user->email}}</p>
                                     <p>Contact :{{$user->contact_no}}</p>
                                     <p>WhatsApp no :{{$user->whats_app_no}}</p>
                                  </td>
                                 <td>{{$user->company_address}}</td>
                                 <td>{{$user->state}}</td>
                                 <td>{{$user->city}}</td>
                                 <td>{{$user->zip_code}}</td>
                                 <td>{{$user->gst_no}}</td>
                                 <td>Rejected</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<script>
   $(document).ready( function () {
     $('#datatable,#datatable1,#datatable2').DataTable({
       dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
     });
     $(document).on('change','.user-action',function(){
       $userid = $(this).data('user-id');
       $status = $(this).val();
         $.ajax({
         url: '{{URL("/admin/approve-user")}}',
         type: 'post',
         dataType: 'json',
         data: {user_id:$userid,status:$status},
         success: function(res) {
           alert(res.msg);
          location.reload();
         }
         })
     })
   })
</script>
@endsection
