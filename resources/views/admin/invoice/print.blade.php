<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
      <title>Invoice Print</title>
      <link href="{{ asset('public/assets/invoice/style.css') }}" rel="stylesheet" type="text/css" >
      <style>
      textarea{
        pointer-events: none;
      }
      </style>
   </head>
   <body>
      <div id="page-wrap">
         <textarea id="header">INVOICE</textarea>
         <div id="identity">
           <p>
           <span style="font-weight:700;font-size:20px">{{$company->company_name}}.</span>
           <br>
            {{$company->address}}<br>,
            {{$company->address1}},<br>
            {{$company->city}}, {{$company->state}} {{$company->zip_code}}<br>
             Phone: {{$company->company_contact_no}}<br>
             GST: {{$company->GSTIN}}<br>
           </p>
            <div id="logo">
               <img  style="width:50%;" id="image" src="https://saifagrogroup.com/resources/views/storefront/main_logo.png" alt="logo" />
            </div>
         </div>
         <div style="clear:both"></div>
         <div id="customer">
              <p>
              <span style="font-weight:700;font-size:20px">{{$customer->username}}.</span>
              <br>
              {{$customer->company_address}},
              {{$customer->city}},<br>
              {{$customer->state}},{{$customer->zip_code}},<br>
              GST:{{$customer->gst_no}}
              </p>
            <table id="meta">
               <tr>
                  <td class="meta-head">Invoice #</td>
                  <td><textarea>{{$order->order_no}}</textarea></td>
               </tr>
               <tr>
                  <td class="meta-head">Date</td>
                  <td><textarea id="date">{{date('F d,Y',strtotime($order->created_at))}}</textarea></td>
               </tr>
               <tr>
                  <td class="meta-head">Total</td>
                  <td>
                     <div class="due">&#x20b9; {{$order->order_amount}}</div>
                  </td>
               </tr>
            </table>
         </div>
         <table id="items">
            <tr>
               <th>Sr No.</th>
               <th>Description Of Goods</th>
               <th>HSN/SAC</th>
               <th>Quantity</th>
               <th>Rate</th>
               <th>Per</th>
               <th>Disc%</th>
               <th>Amount</th>
            </tr>
            @php
            $orderGST = 0;
            $withoutGstTotal = 0;
            @endphp
            @foreach($items as $ik=>$item)
            <!-- @php
            echo "<pre>";
            print_r($item->product);
            echo "</pre>";
            @endphp -->
            <!-- backend_part_no -->
            <tr class="item-row">
                <td>{{$ik+1}}</td>
                <td class="description">{{$item->product_title}}<br>{{$item->product->backend_part_no}}</td>
                <td class="description"><textarea>{{$item->product->hsn_code}}</textarea></td>

                <td class="description"><textarea class="cost">{{$item->qty}}</textarea></td>
                <td class="description"><textarea class="qty">{{$item->price}}</textarea></td>
                <td class="description"><textarea class="qty">nos</textarea></td>
                <td><textarea class="qty"></textarea></td>
                <td>
                  @php
                  $gstAmount  = $item->item_total - ($item->item_total*(100/(100+18)));
                  $orderGST+=$gstAmount;
                  $netPrice = $item->item_total - $gstAmount;
                  $withoutGstTotal+=$netPrice;
                  @endphp
                  <span class="price">
                  {{round($netPrice)}}
                </span>
              </td>
            </tr>
          @endforeach

          @if(strtolower($company->state) == strtolower($customer->state))
              <tr class="item-row">
                <td></td>
                <td class="description"><b>@CGST</b><br>
                9%</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><span class="price">{{round($orderGST)/2}}</span></td>
              </tr>
              <tr class="item-row">
                <td></td>
                <td class="description">
                  <b>@SGST</b>9%
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><span class="price">{{round($orderGST)/2}}</span></td>
              </tr>
          @else
            <tr class="item-row">
              <td></td>
              <td class="description"><b>@IGST</b>18%</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><span class="price">{{round($orderGST)}}</span></td>
            </tr>
          @endif
          <tr>
             <td colspan="5" class="blank"> </td>
             <td colspan="2" class="total-line">Subtotal</td>
             <td class="total-value">
                <div id="subtotal">&#x20b9;{{$order->subtotal}}</div>
             </td>
          </tr>
            <tr>
               <td colspan="5" class="blank"> </td>
               <td colspan="2" class="total-line">Packing Charges</td>
               <td class="total-value">
                  <div id="total">&#x20b9;{{$order->packing_charges}}</div>
               </td>
            </tr>
            <tr>
               <td colspan="5" class="blank"> </td>
               <td colspan="2" class="total-line">Total</td>
               <td class="total-value">
                  <div id="total">&#x20b9;{{$order->order_amount}}</div>
               </td>
            </tr>

         </table>
         <div id="terms">
            <h5>Terms</h5>
            <textarea>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</textarea>
         </div>
      </div>
   </body>

   <script>
      var onPrintFinished=function(printed){
        setTimeout(function(){
          window.location.href = '{{url("/admin/order-list")}}';
        },5000)
      }
      window.onload=function(){
        window.focus();
        onPrintFinished(window.print());
        window.close();
      };
   </script>
</html>
