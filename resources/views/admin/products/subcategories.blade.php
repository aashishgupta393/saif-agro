@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
@php
$masterCats=[];
  foreach(App\Categories::where(['company_id'=>Session::get('admin')->id])->get() as $cat){
    $masterCats[$cat->id]= $cat;
  }

@endphp
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12 col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">SubCategories</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="view-sub-cat1-tab" data-toggle="tab" href="#view-sub-cat-1" role="tab" aria-controls="home" aria-selected="true">
                     Sub Category 1
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="add-sub-cat1-tab" data-toggle="tab" href="#add-sub-cat1" role="tab" aria-controls="profile" aria-selected="false">
                       Add Category 1
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="view-sub-cat2-tab" data-toggle="tab" href="#view-sub-cat2" role="tab" aria-controls="profile" aria-selected="false">
                       Sub category 2
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="view-sub-cat2-tab" data-toggle="tab" href="#add-sub-cat2" role="tab" aria-controls="profile" aria-selected="false">
                       Add Sub category 2
                     </a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent-1">
                  <div class="tab-pane fade show active" id="view-sub-cat-1" role="tabpanel" aria-labelledby="view-sub-cat1-tab">
                    <div class="table-responsive">
                      <table  class="table table-striped table-bordered datatable" >
                         <thead>
                            <tr>
                              <th>Sub Category Category Id</th>
                              <th>Sub Category Title</th>
                              <th>Image</th>
                              <th>Image Name</th>
                              <th>Category Title</th>
                               <th>Product Count(Subcategory)</th>
                               <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                           @foreach(App\SubCategories::with(['category','products'])->where(['company_id'=>Session::get('admin')->id])->get() as $key=>$sb1)
                            <tr>
                             <td>{{$sb1->id}}</td>
                             <td>{{$sb1->title}}</td>
                             <td>
                               @if(!empty($sb1->image))
                                 <a href="{{$appUrl.'/public/uploads/'.$sb1->image}}" target="_blank">
                                   <img class="rounded img-fluid avatar-40" src="{{$appUrl.'/public/uploads/'.$sb1->image}}">
                                 </a>
                                 @endif
                               </td>
                             <td>
                               {{((!@getimagesize($appUrl.'/public/uploads/'.$sb1->image)) ? $sb1->image : ""  ) }}
                             </td>
                             <td>{{$sb1->category->title}}</td>
                             <td>{{count($sb1->products)}}</td>
                             <td>
                                <a style="cursor:pointer;" class="iq-bg-primary"  title="Edit" data-original-title="Edit"
                                   data-toggle="modal" data-target="#EditSubCategory1Modal{{$sb1->id}}"
                                   >
                                   <i class="ri-pencil-line"></i>
                                 </a>

                                 <div class="modal fade" id="EditSubCategory1Modal{{$sb1->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <h5 class="modal-title" id="exampleModalLabel">Update Sub Category Details</h5>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                             </button>
                                          </div>
                                          <div class="modal-body">

                                            <form method="post"  enctype="multipart/form-data" action="{{url('/admin/updatecategory')}}"
                                             >
                                                <input type="hidden" name="id" value="{{$sb1->id}}">
                                                <input type="hidden" name="table" value="sub_category1">

                                                <table class="table mb-0 table-borderless">
                                                    <tr>
                                                      <th scope="col">Category Name</th>
                                                      <td>
                                                        <input type="text" class="form-control" value="{{$sb1->title}}" name="title">
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <th scope="col">Sub Category Image </th>
                                                      <td>
                                                       <a href="{{$appUrl.'/public/uploads/'.$sb1->image}}">
                                                         <img style="width:50px" src="{{$appUrl.'/public/uploads/'.$sb1->image}}">
                                                      </a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <th scope="col">Upload New Image</th>
                                                      <td>
                                                        <input type="file" class="form-control" name="image">
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td  style="text-align:right;" colspan="2">
                                                        <button type="submit" class="btn btn-warning">
                                                           Update
                                                        </button>
                                                      </td>
                                                    </tr>
                                               </table>
                                           </form>
                                          </div>
                                          <div class="modal-footer">
                                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                               </td>
                            </tr>
                         @endforeach
                       </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="add-sub-cat1" role="tabpanel" aria-labelledby="add-sub-cat1-tab">
                      <form action="{{url('/admin/add-category')}}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email">Sub Category Name :</label>
                            <input type="text" name="cat_name" class="form-control" >
                            <input type="hidden" name="table" value="sub_category1" >
                        </div>

                        <div class="form-group">
                            <label for="email">Category Name :</label>
                            <select  name="cat_id" required class="form-control">
                              <option value=""> Please Select</option>
                              @foreach(App\Categories::where(['company_id'=>Session::get('admin')->id])->get() as $key=>$cat)
                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                              @endforeach
                            </select>
                        </div>
                         <div class="form-group">
                           <label for="email">Sub Category Image :</label>
                            <div class="custom-file">
                               <input type="file" name="banner" accept="image/x-png,image/jpeg" class="custom-file-input" id="customFile">
                               <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                         </div>
                         <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                  </div>
                  <div class="tab-pane fade" id="view-sub-cat2" role="tabpanel" aria-labelledby="view-sub-cat2-tab">
                    <div class="table-responsive">
                      <table  class="table table-striped table-bordered datatable" >
                         <thead>
                            <tr>
                              <th>Sub Category Category Id</th>
                              <th>Sub Category Title</th>
                              <th>Image</th>
                              <th>Image Name</th>

                              <th>Category Title</th>
                              <th>Subcategory 1 Title</th>
                               <th>Product Count(Subcategory)</th>
                            </tr>
                         </thead>
                         <tbody>
                           @foreach(App\SubCategories2::with(['category','subcat1','products'])->where(['company_id'=>Session::get('admin')->id])->get() as $key=>$subcategory2)
                            <tr>
                             <td>{{$subcategory2->id}}</td>
                             <td>{{$subcategory2->title}}</td>
                             <td>
                               @if(!empty($subcategory2->image))
                                 <a href="{{$appUrl.'/public/uploads/'.$subcategory2->image}}" target="_blank">
                                   <img class="rounded img-fluid avatar-40" src="{{$appUrl.'/public/uploads/'.$subcategory2->image}}">
                                 </a>
                                 @endif
                               </td>
                               <td> {{((!@getimagesize($appUrl.'/public/uploads/'.$subcategory2->image)) ? $subcategory2->image : ""  ) }}</td>
                               <td>{{$subcategory2->category->title}}</td>
                               <td>{{(isset($subcategory2->subcat1->title) ? $subcategory2->subcat1->title : "")}}</td>
                               <td>{{count($subcategory2->products)}}</td>
                            </tr>
                         @endforeach
                         </tbody>
                      </table>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="add-sub-cat2" role="tabpanel" aria-labelledby="add-sub-cat2-tab">
                      <form action="{{url('/admin/add-category')}}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email">Sub Category2 Name :</label>
                            <input type="text" name="cat_name" class="form-control" >
                            <input type="hidden" name="table" value="sub_category2" >
                        </div>

                        <div class="form-group">
                            <label for="email">Category :</label>
                            <select  name="cat_id" required class="form-control">
                              <option value=""> Please Select</option>
                              @foreach(App\Categories::where(['company_id'=>Session::get('admin')->id])->get() as $key=>$cat)
                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Sub Category :</label>
                            <select  name="sub_cat1" required class="form-control">
                              <option value=""> Please Select</option>
                              @foreach(App\SubCategories::where(['company_id'=>Session::get('admin')->id])->get() as $key=>$cat)
                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                              @endforeach
                            </select>
                        </div>
                         <div class="form-group">
                           <label for="email">Sub Category Image :</label>
                            <div class="custom-file">
                               <input type="file" name="banner" accept="image/x-png,image/jpeg" class="custom-file-input" id="customFile">
                               <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                         </div>
                         <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
</div>
<script>
$(document).ready( function () {
  $('.datatable').DataTable({
  });
});
</script>
@endsection
