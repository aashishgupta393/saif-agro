@extends('admin.admin')
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12 col-lg-6">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Upload Banner</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <p>Upload Banner Visible On Strorefront</p>
               <form action="{{url('/admin/upload-banner')}}" method="POST" enctype="multipart/form-data">
                  <div class="form-group">
                     <div class="custom-file">
                        <input type="file" name="banner" accept="image/x-png,image/jpeg" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
               </form>
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-lg-6">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Home Page Banner Preview</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <p>Check Home Page Banner Preview Here</p>
               <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                     @foreach(App\Banner::get() as $key=>$bn)
                     @php
                     $active="";
                     if($key == 0){
                     $active="active";
                     }
                     @endphp
                     <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="{{$active}}"></li>
                     @endforeach
                  </ol>
                  <div class="carousel-inner">
                     @foreach(App\Banner::get() as $bk=>$banner)
                     @php
                     $active="";
                     if($bk == 0){
                     $active="active";
                     }
                     @endphp
                     <div class="carousel-item {{$active}}">
                        <img src="{{$appUrl.'/'.$banner->path}}" class="d-block w-100" alt="#">
                     </div>
                     @endforeach
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <style>
        .delete-div {
        position: absolute;
        right: 0;
        margin-right: 3%;
        margin-top: 2%;
        }
      </style>
      <div class="col-sm-12 col-lg-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Bannner Images</h4>
               </div>
            </div>
            <div class="row">
              @foreach(App\Banner::get() as $bk=>$banner)
                <div class="iq-card-body col-lg-6">
                  <div class="delete-div">
                    <button data-id="{{$banner->id}}" type="button" class="btn btn-danger mb-3"><i class="ri-delete-bin-2-fill pr-0"></i></button>
                  </div>
                    <img src="{{$appUrl.'/'.$banner->path}}" class="img-fluid" alt="Responsive image">
                </div>
              @endforeach
            <div>
         </div>
      </div>
   </div>
</div>
<script>
$('.delete-div button').on('click',function(){
  $deleteURL = "{{url('/admin/delete')}}";
  $deleteId = $(this).data('id');
  var result = confirm("Want to delete?");
  if (result) {
    $.ajax({
      url:$deleteURL,
      data:{id:$deleteId,table:'banner'},
      dataType:'json',
      type:'get',
      success:function(respose){
        alert(respose.msg)
        location.reload();
      }
    })
  }
})
</script>
@endsection
