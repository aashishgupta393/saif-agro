@extends('admin.admin')
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12 col-lg-6">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Upload Product CSV</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <p>Upload Your Product CSV</p>
               <form  action="{{url('/admin/submit-upload')}}" method="post" enctype="multipart/form-data">
                 <div class="form-group">
                    <div class="custom-file">
                       <input type="file" class="custom-file-input" required  accept=".csv" name="csv">
                       <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                 </div>
                  <button type="submit" class="btn btn-primary">Upload</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
