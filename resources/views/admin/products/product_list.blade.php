@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Product List</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <div class="table-responsive">
                  <table id="datatable" class="table table-striped table-bordered" >
                     <thead>
                        <tr>
                          <th>Make Popular</th>
                          <th>Product Id</th>
                           <th>Product Name</th>
                           <th>Category</th>
                           <th>Sub category 1 </th>
                           <th>Sub category 2 </th>
                           <th>Product Image</th>
                           <th>Not Found Images</th>
                           <th>Product Price</th>
                           <th>Action</th>
                        </tr>
                     </thead>

                     <tbody>
                     @foreach(App\Products::with(['category','subcategory2','subcategory1'])->where(['company_id'=>Session::get('admin')->id])->get() as $key=>$product)
                     @if(!empty($product->product_title))
                        <tr>
                          <td>
                            <div class="custom-control custom-checkbox text-center">
                                <input type="checkbox" {{($product->is_popular == 1 ? "checked" : "")}}
                                class="custom-control-input" data-pid="{{$product->id}}" id="product{{$key}}">
                                <label class="custom-control-label" for="product{{$key}}"></label>
                            </div>

                          </td>
                           <td>{{$product->product_id}}</td>
                           <td>
                              {{$product->product_title}}
                              <a target="_blank" class="iq-bg-primary"  data-placement="top" title="" data-original-title="View"  href="{{$appUrl.'/product?id='.$product->id}}"  ><i class="ri-eye-line"></i></a>
                           </td>
                           <td>{{$product->category->title}}</td>
                           <td>{{isset($product->subcategory1) ? $product->subcategory1->title : ""}}</td>
                           <td>{{isset($product->subcategory2) ? $product->subcategory2->title : ""}}</td>
                           <td>
                             <?php
                             $notFound=[];
                             ?>
                             @if(!empty($product->images))
                             <?php
                             $images = json_decode($product->images);
                             foreach ($images as  $image) {
                               if (!@getimagesize($appUrl."/".$image)) {
                                 $notFound[]= str_replace('public/uploads/','',$image);
                               }else{
                               }
                              ?>
                                 <a href="{{$appUrl.'/'.$image}}" target="_blank">
                                   <img class="rounded img-fluid avatar-40" src="{{$appUrl.'/'.$image}}">
                                 </a>
                             <?php }
                             ?>
                             @endif

                           </td>
                           <td>{{((count($notFound) > 0 ? implode(',',$notFound) : ""))}}</td>
                           <td>&#8377; {{$product->product_amount}}</td>
                          <td>
                            <div style="display:flex;" class="flex align-items-center list-user-action">
                                <a class="iq-bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="{{url('/admin/edit-product?pid=')}}{{$product->id}}">
                                  <i class="ri-pencil-line"></i>
                                </a>
                               

                            <a class="iq-bg-primary"  data-placement="top"
                            title="" data-original-title="View"   href="#"
                            data-toggle="modal" data-target="#ProductsModal{{$product->id}}"
                                  >
                                    <i class="ri-eye-line"></i>
                                  </a>
                                <div class="modal fade" id="ProductsModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                   <div class="modal-dialog modal-lg" role="document">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Product Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <div class="modal-body">
                                           <h2>Variants</h2>
                                           <table class="table mb-0 table-borderless">
                                             <thead>
                                                <tr>
                                                   <th scope="col">Title</th>
                                                   <th scope="col">Image</th>
                                                   <th scope="col">Price</th>
                                                   <th scope="col">Part No</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                               <?php
                                               $variants =[];
                                               if(!empty($product->variants)){
                                                 $variants = json_decode($product->variants);
                                               }
                                               ?>
                                               <?php foreach ($variants as $key => $variant): ?>
                                                 <tr>
                                                   <td>{{$product->product_title}} {{$variant->titile}}</td>
                                                   <td>Products</td>
                                                   <td>&#x20b9; {{$variant->price}}</td>
                                                   <td>{{$variant->part_no}}</td>
                                                 </tr>
                                               <?php endforeach; ?>
                                             </tbody>
                                          </table>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>


                                <a class="iq-bg-primary delete-item" data-id="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#">
                                  <i class="ri-delete-bin-line"></i>
                                </a>
                            </div>
                          </td>
                        </tr>
                        @endif
                     @endforeach
                   </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready( function () {
  $('#datatable').DataTable();
  $('.custom-control-input').on('change',function(){
    $isPopular = 0;
    if($(this).prop('checked') == true){
      $isPopular = 1;
    }
    $pid = $(this).data('pid');
    $data = {is_popular:$isPopular,id:$pid};
    console.log($data);
  })
  $('.delete-item').on('click',function(){
    $deleteURL = "{{url('/admin/delete')}}";
    $deleteId = $(this).data('id');
    var result = confirm("Want to delete?");
    if (result) {
      $("#loading").show();
      $.ajax({
        url:$deleteURL,
        data:{id:$deleteId,table:'products'},
        dataType:'json',
        type:'get',
        success:function(respose){
          alert(respose.msg)
          location.reload();
        }
      })
    }
  })

});



</script>
@endsection
