@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Categories</h4>
               </div>
            </div>

            <div class="iq-card-body">
               <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="home-tab-two" data-toggle="tab" href="#home-two" role="tab" aria-controls="home" aria-selected="true">
                     Category List
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="profile-tab-two" data-toggle="tab" href="#profile-two" role="tab" aria-controls="profile" aria-selected="false">
                       Add Category
                     </a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent-1">
                  <div class="tab-pane fade show active" id="home-two" role="tabpanel" aria-labelledby="home-tab-two">
                     <div class="table-responsive">
                       <table id="datatable" class="table table-striped table-bordered" >
                          <thead>
                             <tr>
                               <th>Category Id</th>
                               <th>Category Title</th>
                                <th>Cat Image</th>
                                <th>Product Count</th>
                                <th>Action</th>
                             </tr>
                          </thead>
                          <tbody>
                          @foreach(App\Categories::with(['products'])->where(['company_id'=>Session::get('admin')->id])->get() as $key=>$cat)
                             <tr>
                                <td>{{$cat->id}}</td>
                                <td>{{$cat->title}}</td>
                                <td>
                                  <a href="{{$appUrl.'/public/uploads/'.$cat->image}}" target="_blank">
                                     <img class="rounded img-fluid avatar-40" src="{{$appUrl.'/public/uploads/'.$cat->image}}">
                                   </a>
                                </td>
                                <td>
                                  {{count($cat->products)}}
                                </td>
                                <td>
                                  <div style="display:flex;" class="flex align-items-center list-user-action">
                                      <a class="iq-bg-primary"  data-placement="top" title="" data-original-title="Edit"
                                        data-toggle="modal" data-target="#EditSubCategoryModal{{$cat->id}}"
                                        >
                                        <i class="ri-pencil-line"></i>
                                      </a>
                                      <a  class="iq-bg-primary {{!isset($subcats[$cat->id]) ? 'delete-item' : ''}}" data-id="{{$cat->id}}"    data-toggle="tooltip"  data-placement="top"  title=""    data-original-title="Delete" >
                                        <i class="ri-delete-bin-line"></i>
                                      </a>

                                      <div class="modal fade" id="EditSubCategoryModal{{$cat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                         <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                               <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel">Update Category Details</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                  </button>
                                               </div>
                                               <div class="modal-body">

                                                 <form method="post"  enctype="multipart/form-data" action="{{url('/admin/updatecategory')}}"
                                                     <input type="hidden" name="id" value="{{$cat->id}}">
                                                     <input type="hidden" name="table" value="category">
                                                     <table class="table mb-0 table-borderless">
                                                         <tr>
                                                           <th scope="col">Category Name</th>
                                                           <td>
                                                             <input type="text" class="form-control" value="{{$cat->title}}" name="title">
                                                           </td>
                                                         </tr>
                                                         <tr>
                                                           <th scope="col">Category Image </th>
                                                           <td>
                                                            <a href="{{$appUrl.'/public/uploads/'.$cat->image}}">
                                                             <img style="width:50px" src="{{$appUrl.'/public/uploads/'.$cat->image}}">
                                                           </a>
                                                           </td>
                                                         </tr>
                                                         <tr>
                                                           <th scope="col">Upload New Image</th>
                                                           <td>
                                                             <input type="file" class="form-control" name="image">
                                                           </td>
                                                         </tr>
                                                         <tr>
                                                           <td  style="text-align:right;" colspan="2">
                                                             <button type="submit" class="btn btn-warning">
                                                                Update
                                                             </button>
                                                           </td>
                                                         </tr>
                                                    </table>
                                                </form>
                                               </div>
                                               <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                               </div>
                                            </div>
                                         </div>
                                      </div>


                                  </div>
                                </td>
                             </tr>
                          @endforeach
                        </tbody>
                       </table>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="profile-two" role="tabpanel" aria-labelledby="profile-tab-two">
                      <form action="{{url('/admin/add-category')}}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email">Category Name :</label>
                            <input type="text" name="cat_name" class="form-control" >
                            <input type="hidden" name="table" value="category" >
                        </div>
                         <div class="form-group">
                           <label for="email">Category Image :</label>
                            <div class="custom-file">
                               <input type="file" name="banner" accept="image/x-png,image/jpeg" class="custom-file-input" id="customFile">
                               <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                         </div>
                         <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready( function () {
  $('#datatable').DataTable();
  $('.delete-item').on('click',function(){
    $deleteURL = "{{url('/admin/delete')}}";
    $deleteId = $(this).data('id');
    var result = confirm("Want to delete?");
    if (result) {
      $("#loading").show();
      $.ajax({
        url:$deleteURL,
        data:{id:$deleteId,table:'category'},
        dataType:'json',
        type:'get',
        success:function(respose){
          alert(respose.msg)
          location.reload();
        }
      })
    }
  })
});
</script>
@endsection
