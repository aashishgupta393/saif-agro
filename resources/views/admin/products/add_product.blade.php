@extends('admin.admin')
@section('content')
<style>
   .singlerow {
   margin-right: 2%;
   margin-left: 2%;
   border: 1px solid #f1f1f1;
   margin-bottom: 2%;
   margin-top: 3%;
   padding: 30px 15px;
   }
   .single-row-parent {
   position: relative;
   }
   .single-row-parent .btn.btn-danger.btn-sm {
   position: absolute;
   top: 10px;
   right: 30px;
   }
   #add-variant-wrapper {
   display: flex;
   }
   #add-variant-wrapper h5 {
   width: 80%
   }
   #add-variant-wrapper #btn-div {
   text-align: right;
   float: right;
   width: 20%;
   margin-right: 2%;
   }
</style>
<div class="container-fluid">
   <div class="row">
      <div class="col-lg-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Add New  Product</h4>
               </div>
            </div>
            <form method="post" action="{{url('admin/save-product')}}" enctype="multipart/form-data">
            <div class="iq-card-body">
               <div class="new-user-info">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <label for="product-title">Product Title:</label>
                           <input type="text" name="product_title" class="form-control"  id="product-title" placeholder="Product Title">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="category">Product Category:</label>
                           <select class="form-control" id="category" name="cat_id">
                           @foreach(App\Categories::where(['company_id'=>Session::get('admin')->id])->get() as $cat)
                              <option  value="{{$cat->id}}" >{{$cat->title}}</option>
                           @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="subcategory1">Product Category1:</label>
                           <select class="form-control" id="subcategory1" name="sub_cat1">
                              <option value="">Select Category 1</option>
                              @foreach(App\SubCategories::where(['company_id'=>Session::get('admin')->id])->get() as $sb1)
                              <option value="{{$sb1->id}}" >{{$sb1->title}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="subcategory2">Product Category2:</label>
                           <select class="form-control" id="subcategory2" name="sub_cat2">
                              <option value="">Select Category 2</option>
                              @foreach(App\SubCategories2::where(['company_id'=>Session::get('admin')->id])->get() as $sb2)
                              <option value="{{$sb2->id}}" >{{$sb2->title}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="product-amount">Product Amount:</label>
                           <input type="text" onkeypress="validate(event)" name="product_amount" class="form-control" id="product-amount" placeholder="Product amount">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="product-desc">Product Description:</label>
                           <textarea name="product_description" class="form-control" placeholder="Product Desc" ></textarea>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="backend-part-no">Backend Part no:</label>
                           <input type="text" name="backend_part_no"  class="form-control" id="backend-part-no" placeholder="Backend Part No">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="hsn-code">HSN Code</label>
                           <input type="text" name="hsn_code"  class="form-control" id="hsn-code" placeholder="HSN Code">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="tax-rate">Tax Rate</label>
                           <input type="text" name="tax_rate" class="form-control" id="tax-rate" placeholder="Tax Rate">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="part-no">Part no</label>
                           <input type="text" name="part_no"   class="form-control" id="part-no" placeholder="Part No">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="image1">Product Image 1
                           </label>
                           <input accept="image/*" type="file" name="image0" class="form-control" id="image1" >
                        </div>
                        <div class="form-group col-md-6">
                           <label for="image2">Product Image 2
                           </label>
                           <input accept="image/*" type="file" name="image1" class="form-control" id="image2" >
                        </div>
                        <div class="form-group col-md-6">
                           <label for="image3">Product Image 3
                           </label>
                           <input accept="image/*" type="file"   name="image2" id="image3" class="form-control" >
                        </div>
                     </div>
                     <hr>
                     <div id="add-variant-wrapper">
                        <h5 class="mb-3">Variants</h5>
                        <div id="btn-div" class="mb-3">
                           <button type="button" id="add-variants" class="btn btn-warning btn-sm">
                           <i class="fa fa-plus-circle"></i>
                           </button>
                        </div>
                     </div>
               </div>
                 <div id="variants">
                 </div>
                 <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
         </div>
      </div>
   </div>
</div>
</div>
<script type="text/javascript">
function validate(evt) {
  var theEvent = evt || window.event;
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
   function ReturnSingleparent(obj=null) {

   	var _length = $('.single-row-parent').length +1;

   	var _appURL = "{{$appUrl}}/";
   	var image="";

   	if(obj && obj.image != ""){
       var _imagePath = _appURL+obj.image;
       image+='<a target="_blank" href="'+_imagePath+'">';
       image+='<img class="rounded img-fluid avatar-40" src="'+_imagePath+'" alt="profile">';
       image+='</a>';

   	}


   	var _Singleparent = "";
   _Singleparent+='<div class="single-row-parent">';
    if(obj){
      _Singleparent+='<input type="hidden" name="variant_images[]" value="'+obj.image+'">';
    }

   _Singleparent+='<div class="row singlerow">';
   _Singleparent+='<div class="form-group col-md-6">';

   _Singleparent+='<label for="variant_title'+_length+'">Variant Title: '+image+'</label>';
   _Singleparent+='<input  type="text" value="'+(obj ? obj.titile : '')+'" name="variant_title[]" class="form-control" id="variant_title'+_length+'" placeholder="Variant name">';
   _Singleparent+='</div>';

   _Singleparent+='<div class="form-group col-md-6">';
   _Singleparent+='<label for="variant_image'+_length+'">Variant Image:</label>';
   _Singleparent+='<input accept="image/*"  type="file" id="variant_image'+_length+'" class="form-control" name="variant_image'+_length+'" >';
   _Singleparent+='</div>';


   _Singleparent+='<div class="form-group col-md-6">';
   _Singleparent+='<label for="varinat_price'+_length+'">Variant Price:</label>';
   _Singleparent+='<input onkeypress="validate(event)" name="variant_price[]" type="text"  value="'+(obj ? obj.price : '')+'" class="form-control" id="varinat_price'+_length+'" placeholder="Variant Price">';
   _Singleparent+='</div> ';


   _Singleparent+='<div class="form-group col-md-6">';
   _Singleparent+='<label for="varint_part_no'+_length+'">Variant Part No:</label>';
   _Singleparent+='<input name="variant_part_no[]" type="text" value="'+(obj ? obj.part_no : '')+'" id="varint_part_no'+_length+'" class="form-control"  placeholder="Variant Part No">';
   _Singleparent+='</div>';
   _Singleparent+='</div>';



   _Singleparent+='<button type="button" class="btn btn-danger btn-sm removerow">';
   _Singleparent+='<i class="fa fa-minus-circle"></i>';
   _Singleparent+='</button>';
   _Singleparent+='</div>';

   return _Singleparent;
   }
   $(document).on('click','.removerow',function(){
   	$(this).parents('.single-row-parent').remove();
   });

   $("#add-variants").click(function(){
   	var _length = $('.single-row-parent').length;
   	if(_length < 6){
   		$("#variants").append(ReturnSingleparent());
   	}else{
   		alert("Limit Reached To Add variants");
   	}
   })

</script>
@endsection
