@extends('admin.admin')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Upload Product Images</h4>
               </div>
            </div>
            <div class="iq-card-body">
              <form method="post" action="{{url('admin/dropzone')}}" enctype="multipart/form-data"
                class="dropzone" id="dropzone">
              @csrf
              </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready( function () {
  Dropzone.options.dropzone =
        {
           // maxFilesize: 12,
           // renameFile: function(file) {
           //     var dt = new Date();
           //     var time = dt.getTime();
           //    return time+file.name;
           // },
           acceptedFiles: ".jpeg,.jpg,.png",
           addRemoveLinks: true,
           timeout: 50000,
           removedfile: function(file)
           {
               var name = file.upload.filename;
               $.ajax({
                   type: 'POST',
                   url: '{{ url("admin/delete-image") }}',
                   data: {filename: name},
                   success: function (data){
                       console.log("File has been successfully removed!!");
                   },
                   error: function(e) {
                       console.log(e);
                   }});
                   var fileRef;
                   return (fileRef = file.previewElement) != null ?
                   fileRef.parentNode.removeChild(file.previewElement) : void 0;
           },
           success: function(file, response){
               console.log(response);
           },
           error: function(file, response)
           {
              return false;
           }
};

});
</script>
@endsection
