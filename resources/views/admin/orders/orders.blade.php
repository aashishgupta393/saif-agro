@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">My Orders</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="home-tab-two" data-toggle="tab" href="#home-two" role="tab" aria-controls="home" aria-selected="true">
                       Paid
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" id="profile-tab-two" data-toggle="tab" href="#profile-two" role="tab" aria-controls="profile" aria-selected="false">
                       Unpaid
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" data-toggle="tab" href="#exel-div" role="tab" aria-controls="profile" aria-selected="false">
                       Download Exel
                     </a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent-1">
                  <div class="tab-pane fade show active" id="home-two" role="tabpanel" aria-labelledby="home-tab-two">

                    <div class="iq-card-body">
                     <h3>Filter Paid Orders</h3>
                     <form action="{{url('/admin/order-list')}}" class="filtter-form">
                        <div class="form-row">
                           <div class="col">
                             From Date
                             <input type="date"  required name="from" class="form-control" >
                           </div>
                           <div class="col">
                             To Date
                             <input type="date"  required name="to" class="form-control" >
                             <input type="hidden"  name="type" value="paid">
                           </div>
                           <div class="col" style="margin-top: 3%;margin-left: 3%;">
                              <button type="submit" class="btn btn-info" >Search</button>
                           </div>
                        </div>
                     </form>
                    </div>



                     <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Order</th>
                                 <th>Date</th>
                                 <th>Customer</th>
                                 <th>Total</th>
                                 <th>Payment</th>
                                 <th>Items</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>

                              @php
                              $paidOrders = App\Orders::with(['customer','items'])->where(['company_id'=>Session::get('admin')->id,'payment_status'=>'paid'])->orderBy('id', 'DESC')->get();

                              if(isset($type) && $type == 'paid'){
                                $paidOrders = App\Orders::with(['customer','items'])
                                                          ->whereBetween('created_at', [$from, $to])
                                                          ->where(['company_id'=>Session::get('admin')->id,'payment_status'=>'paid'])
                                                          ->orderBy('id', 'DESC')->get();
                              }
                              @endphp

                              @foreach($paidOrders as $order)
                              <tr>
                                 <td>{{$order->order_no}}</td>
                                 <td>{{date('F d,Y',strtotime($order->created_at))}}</td>
                                 <td>{{$order->customer->username}}</td>
                                 <td>{{$order->order_amount}}</td>
                                 <td>
                                   <div class="badge badge-pill badge-primary">Paid</div>
                                 </td>
                                 <td>{{count($order->items)}}</td>
                                 <td>
                                  <div class="flex align-items-center list-user-action">
                                    <a class="iq-bg-primary"  data-placement="top"   title="" data-original-title="View"  href="#" data-toggle="modal" data-target="#ItemsModal{{$order->id}}" >
                                      <i class="ri-eye-line"></i>
                                    </a>
                                    <a class="iq-bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" href="{{url('/admin/invoice-print')}}?id={{$order->id}}">
                                      <i class="ri-printer-fill mr-2"></i>
                                    </a>

                                    <a class="iq-bg-primary"  data-placement="top" title="" data-original-title="View" href="#"data-toggle="modal" data-target="#ShippingModal{{$order->id}}">
                                      <i class="las la-shipping-fast"></i>
                                    </a>

                                    @if(!empty($order->pdf_invoice_path))
                                    <a class="iq-bg-primary"   target="_blank" href="{{$order->pdf_invoice_path}}" >
                                      <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                    </a>
                                    @endif

                                    <a class="iq-bg-primary"   href="{{url('/admin/single-order-export?id=')}}{{$order->id}}"     >
                                      <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    </a>
                                    <a class="iq-bg-primary"  data-placement="top" title="" data-original-title="View" href="#" data-toggle="modal" data-target="#e-way-bill-modal{{$order->id}}">
                                      <img style="height:20px" src="https://i.ibb.co/YLLKcPf/image.png">
                                    </a>

                                    <a class="iq-bg-primary"  data-placement="top" title="" data-original-title="View" href="#"
                                      data-toggle="modal" data-target="#TallyModel{{$order->id}}">
                                      <i class="fa fa-upload" aria-hidden="true"></i>
                                    </a>

                                  </div>

                                  <div class="modal fade" id="ItemsModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                     <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                           <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Order No. {{$order->order_no}}</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                              </button>
                                           </div>
                                           <div class="modal-body">
                                             <table class="table mb-0 table-borderless">
                                               <thead>
                                                  <tr>
                                                     <th scope="col">Item Name</th>
                                                     <th scope="col">Price</th>
                                                     <th scope="col">Qty</th>
                                                     <th scope="col">Total</th>
                                                  </tr>
                                               </thead>
                                               <tbody>
                                                 @foreach($order->items as $item)
                                                  <tr>
                                                     <td>{{$item->product_title}}</td>
                                                     <td>{{$item->price}}</td>
                                                     <td>{{$item->qty}}</td>
                                                     <td>{{$item->item_total}}</td>
                                                  </tr>
                                                  @endforeach
                                                  <tr>
                                                     <td colspan="3">Subtotal</td>
                                                     <td>{{$order->subtotal}}</td>
                                                  </tr>
                                                  <tr>
                                                     <td colspan="3">Packing Charges</td>
                                                     <td>{{$order->packing_charges}}</td>
                                                  </tr>
                                                  <tr>
                                                     <td colspan="3">Total</td>
                                                     <td>{{$order->order_amount}}</td>
                                                  </tr>
                                               </tbody>
                                            </table>
                                           </div>
                                           <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                           </div>
                                        </div>
                                     </div>
                                  </div>

                                  <div class="modal fade" id="ShippingModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                     <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                           <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Add Shipping Details</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                              </button>
                                           </div>
                                           <div class="modal-body">
                                               <form method="post"  enctype="multipart/form-data" action="{{url('/admin/UpdateOrder')}}" class="update-order">
                                                   <input type="hidden" name="order_id" value="{{$order->id}}">
                                                   <table class="table mb-0 table-borderless">
                                                       <tr>
                                                         <th scope="col">Booking date</th>
                                                         <td>
                                                           <input type="date" class="form-control" value="{{$order->shipping_date}}" name="shipping_date">
                                                         </td>
                                                       </tr>
                                                       <tr>
                                                         <th scope="col">Transport Name </th>
                                                         <td>
                                                           <input type="text" class="form-control" value="{{$order->shipping_providor}}" name="shipping_providor">
                                                         </td>
                                                       </tr>
                                                       <tr>
                                                         <th scope="col">LR No. </th>
                                                         <td>
                                                           <input type="text" class="form-control" value="{{$order->tracking_code}}" name="tracking_code">
                                                         </td>
                                                       </tr>
                                                       <tr style="display:none;">
                                                         <th scope="col">Shipping Tracking URL </th>
                                                         <td>
                                                           <input type="text" class="form-control" value="{{$order->tracking_url}}" name="tracking_url">
                                                         </td>
                                                       </tr>
                                                       <tr>
                                                         <th scope="col">Upload LR Copy </th>
                                                         <td>
                                                           <input type="file" class="form-control"     name="lr_copy">
                                                         </td>
                                                       </tr>
                                                       <tr>
                                                         <td  style="text-align:right;" colspan="2">
                                                           <button type="submit" class="btn @if(!empty($order->tracking_url)) btn-warning @else btn-primary @endif">
                                                                @if(!empty($order->tracking_url))
                                                                  Update
                                                                @else
                                                                  Save
                                                                @endif
                                                           </button>
                                                         </td>
                                                       </tr>
                                                  </table>
                                              </form>
                                           </div>
                                           <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                           </div>
                                        </div>
                                     </div>
                                  </div>

                                  <div class="modal fade" id="TallyModel{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                     <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                           <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Upload Tally Invoice</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                              </button>
                                           </div>
                                           <div class="modal-body">
                                               <form method="post"  enctype="multipart/form-data" action="{{url('/admin/UpdateOrder')}}" class="update-order">
                                                   <input type="hidden" name="order_id" value="{{$order->id}}">
                                                   <table class="table mb-0 table-borderless">
                                                       <tr>
                                                         <th scope="col">Upload Tally Invoice </th>
                                                         <td>
                                                           <input type="file" class="form-control"  name="pdf_invoice_path">
                                                         </td>
                                                    </tr>
                                                    <tr>
                                                      <td colspan="2" style="text-align:right">
                                                        <button type="submit" class="btn  btn-primary">Upload</button>
                                                      </td>
                                                 </tr>
                                                  </table>

                                              </form>
                                           </div>
                                           <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                           </div>
                                        </div>
                                     </div>
                                  </div>



                                  <div class="modal fade" id="e-way-bill-modal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                   <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add E way Bill</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <div class="modal-body">
                                            <form method="post"  enctype="multipart/form-data" action="{{url('/admin/UpdateOrder')}}" class="update-order">
                                               <input type="hidden" name="order_id" value="{{$order->id}}">
                                               <table class="table mb-0 table-borderless">

                                                  <tr>
                                                     <th scope="col">E Way Bill Number </th>
                                                     <td>
                                                        <input type="text" onKeyUp="numericFilter(this);"   maxlength="12" class="form-control" value="{{$order->e_way_bill}}" name="e_way_bill">
                                                     </td>
                                                  </tr>

                                                  </tr>
                                                  <tr>
                                                     <td  style="text-align:right;" colspan="2">
                                                        <button type="submit" class="btn @if(!empty($order->tracking_url)) btn-warning @else btn-primary @endif">
                                                        @if(!empty($order->tracking_url))
                                                          Update
                                                        @else
                                                         Save
                                                        @endif
                                                        </button>
                                                     </td>
                                                  </tr>
                                               </table>
                                            </form>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>

                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="profile-two" role="tabpanel" aria-labelledby="profile-tab-two">
                    <div class="iq-card-body">
                     <h3>Filter Pending Orders</h3>
                     <form action="{{url('/admin/order-list')}}" class="filtter-form">
                        <div class="form-row">
                           <div class="col">
                             From Date
                             <input type="date"  required name="from" class="form-control" >
                           </div>
                           <div class="col">
                             To Date
                             <input type="date"  required name="to" class="form-control" >
                             <input type="hidden"  name="type" value="pending">
                           </div>
                           <div class="col" style="margin-top: 3%;margin-left: 3%;">
                              <button type="submit" class="btn btn-info" >Search</button>
                           </div>
                        </div>
                     </form>
                    </div>
                    <div class="table-responsive">
                      <table id="datatable1" class="table table-striped table-bordered" >
                         <thead>
                            <tr>
                               <th>Order</th>
                               <th>Date</th>
                               <th>Customer</th>
                               <th>Total</th>
                               <th>Payment</th>
                               <th>Items</th>
                               <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>

                           @php
                           $pendingorders = App\Orders::with(['customer','items'])->where(['company_id'=>Session::get('admin')->id,'payment_status'=>'pending'])->orderBy('id', 'DESC')->get();

                           if(isset($type) && $type == 'paid'){
                             $paidOrders = App\Orders::with(['customer','items'])
                                                       ->whereBetween('created_at', [$from, $to])
                                                       ->where(['company_id'=>Session::get('admin')->id,'payment_status'=>'pending'])
                                                       ->orderBy('id', 'DESC')->get();
                           }
                           @endphp
                            @foreach($pendingorders as $pending_order)
                            <tr>

                               <td>{{$pending_order->order_no}}</td>
                               <td>{{date('F d,Y',strtotime($pending_order->created_at))}}</td>
                               <td>{{$pending_order->customer->username}}</td>
                               <td>{{$pending_order->order_amount}}</td>
                               <td>
                                 <div class="badge badge-pill badge-danger">Pending</div>
                               </td>
                               <td>{{count($pending_order->items)}}</td>
                               <td>
                                <div class="flex align-items-center list-user-action">
                                  <a class="iq-bg-primary"  data-placement="top"
                                  title="" data-original-title="View"
                                  href="#"
                                  data-toggle="modal" data-target="#ItemsModal{{$pending_order->id}}"
                                  >
                                    <i class="ri-eye-line"></i>
                                  </a>
                                  <a class="iq-bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print" href="{{url('/admin/invoice-print')}}?id={{$pending_order->id}}">
                                    <i class="ri-printer-fill mr-2"></i>
                                  </a>
                                  <a class="iq-bg-primary"  data-placement="top"
                                      title="" data-original-title="View"
                                      href="#"
                                      data-toggle="modal" data-target="#PayMentmodal{{$pending_order->id}}"
                                  >
                                    <i class="ri-money-dollar-circle-line"></i>
                                  </a>

                                </div>

                                <div class="modal fade" id="ItemsModal{{$pending_order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                   <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Order No. {{$pending_order->order_no}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <div class="modal-body">
                                           <table class="table mb-0 table-borderless">
                                             <thead>
                                                <tr>
                                                   <th scope="col">Item Name</th>
                                                   <th scope="col">Price</th>
                                                   <th scope="col">Qty</th>
                                                   <th scope="col">Total</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                               @foreach($pending_order->items as $item)
                                                <tr>
                                                   <td>{{$item->product_title}}</td>
                                                   <td>{{$item->price}}</td>
                                                   <td>{{$item->qty}}</td>
                                                   <td>{{$item->item_total}}</td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                   <td colspan="3">Subtotal</td>
                                                   <td>{{$pending_order->subtotal}}</td>
                                                </tr>
                                                <tr>
                                                   <td colspan="3">Packing Charges</td>
                                                   <td>{{$pending_order->packing_charges}}</td>
                                                </tr>
                                                <tr>
                                                   <td colspan="3">Total</td>
                                                   <td>{{$pending_order->order_amount}}</td>
                                                </tr>
                                             </tbody>
                                          </table>

                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>

                                <div class="modal fade" id="PayMentmodal{{$pending_order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                   <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                         <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Payment Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                         </div>
                                         <div class="modal-body">
                                             <form method="post" class="update-order" enctype="multipart/form-data" action="{{url('/admin/UpdateOrder')}}">
                                                 <input type="hidden" name="order_id" value="{{$pending_order->id}}">
                                                 <table class="table mb-0 table-borderless">
                                                     <tr>
                                                       <th scope="col">Payment Mode</th>
                                                       <td>
                                                         <select class="form-control" name="pay_mode" >
                                                            <option value="cash">Cash</option>
                                                            <option value="cheque">Cheque </option>
                                                            <option value="upi">UPI Payment </option>
                                                            <option value="rtgs">RTGS </option>
                                                            <option value="neft">NEFT </option>
                                                         </select>
                                                       </td>
                                                     </tr>
                                                     <tr>
                                                       <th scope="col">Reference No</th>
                                                       <td>
                                                         <input type="text" class="form-control" name="payment_id">
                                                       </td>
                                                     </tr>
                                                     <tr>
                                                       <td  style="text-align:right;" colspan="2">
                                                         <button type="submit" class="btn btn-primary">Save</button>
                                                       </td>
                                                     </tr>
                                                </table>
                                            </form>
                                         </div>
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                         </div>
                                      </div>
                                   </div>
                                </div>

                               </td>
                            </tr>
                            @endforeach
                         </tbody>
                      </table>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="exel-div" role="tabpanel" aria-labelledby="exel-div">
                    <div class="iq-card-body">
                     <p>Select month And year To Download Exelsheet</p>
                     <form action="{{URL('/admin/export')}}">
                        <div class="form-row">
                           <div class="col">
                            <select class="form-control"  name="month" id="exampleFormControlSelect1">
                                <option selected="" disabled="">Select your age</option>
                                <?php formMonth(); ?>
                            </select>
                           </div>
                           <div class="col">
                               <select class="form-control" name="year" id="exampleFormControlSelect1">
                                 <option selected="" disabled="">Select your age</option>
                                <?php formYear(); ?>
                               </select>
                           </div>
                           <div class="col">
                              <button type="submit" class="btn btn-warning" >Generate</button>
                           </div>
                        </div>
                     </form>
                    </div>

                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
}

   $(document).ready( function () {
     $('#datatable').DataTable();
     $('#datatable1').DataTable();
     $('#datatable2').DataTable();

      <?php
      if(isset($type) && $type=='pending'){ ?>
        $("#profile-tab-two").click();
      <?php }
      ?>
      <?php
      if(isset($type) && $type=='paid'){ ?>
        $("#home-tab-two").click();
      <?php }
      ?>
   });
</script>
@endsection
