<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Admin Login Page</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{ URL::asset('resources/assets/images/favicon.ico') }}" />
      <!-- Bootstrap CSS -->
      <link href="{{ URL::asset('resources/assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <!-- Typography CSS -->
      <link href="{{ URL::asset('resources/assets/css/typography.css') }}" rel="stylesheet">
      <!-- Style CSS -->
      <link href="{{ URL::asset('resources/assets/css/style.css') }}" rel="stylesheet">
      <!-- Responsive CSS -->
      <link href="{{ URL::asset('resources/assets/css/responsive.css') }}" rel="stylesheet">
   </head>
   <body>
      <!-- loader Start -->
      <div id="loading">
         <div id="loading-center">
         </div>
      </div>
      <!-- loader END -->
        <!-- Sign in Start -->
        <section class="sign-in-page">
          <div id="container-inside">
              <div class="cube"></div>
              <div class="cube"></div>
              <div class="cube"></div>
              <div class="cube"></div>
              <div class="cube"></div>
          </div>
            <div class="container p-0">
                <div class="row no-gutters height-self-center">
                  <div class="col-sm-12 align-self-center bg-primary rounded">
                    <div class="row m-0">
                      <div class="col-md-5 bg-white sign-in-page-data">
                          <div class="sign-in-from">
                              <h1 class="mb-0 text-center">Admin Login </h1>
                              <p style="display:none;" class="text-center text-dark">
                                Enter your email address and password to access admin panel.
                              </p>
                              <form class="mt-4" id="login-form">
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Email address</label>
                                      <input type="email" required class="form-control mb-0" id="exampleInputEmail1" name="email" placeholder="Enter email">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Your Company</label>
                                      <select class="form-control" name="company" >
                                            <option value="" >Select Compnay</option>
                                          @foreach(App\Companies::get() as $key=>$company)
                                            <option value="{{$company->id}}">{{$company->company_name}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form-group">
                                      <label for="exampleInputPassword1">Password</label>
                                      <a href="#" style="display:none;" class="float-right">Forgot password?</a>
                                      <input type="password" required class="form-control mb-0" id="exampleInputPassword1" name="password" placeholder="Password">
                                  </div>

                                  <div class="d-inline-block w-100">
                                      <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                          <input type="checkbox" class="custom-control-input" id="customCheck1">
                                          <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                      </div>
                                  </div>
                                  <div class="sign-info text-center">
                                      <button type="submit" class="btn btn-primary d-block w-100 mb-2">Sign in</button>
                                      <span style="visibility:hidden;" class="text-dark dark-color d-inline-block line-height-2">
                                        Don't have an account? <a href="#">Sign up</a>
                                      </span>
                                  </div>
                              </form>
                          </div>
                      </div>
                      
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </section>
        <!-- Sign in END -->
         <!-- color-customizer -->

       <!-- color-customizer END -->
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="{{ URL::asset('resources/assets/js/jquery.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/popper.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/bootstrap.min.js') }}"></script>
      <!-- Appear JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/jquery.appear.js') }}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/countdown.min.js') }}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/waypoints.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/jquery.counterup.min.js') }}"></script>
      <!-- Wow JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/wow.min.js') }}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/apexcharts.js') }}"></script>
      <!-- Slick JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/slick.min.js') }}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/select2.min.js') }}"></script>
      <!-- Owl Carousel JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/owl.carousel.min.js') }}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/jquery.magnific-popup.min.js') }}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/smooth-scrollbar.js') }}"></script>
      <!-- lottie JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/lottie.js') }}"></script>
      <!-- am core JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/core.js') }}"></script>
      <!-- am charts JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/charts.js') }}"></script>
      <!-- am animated JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/animated.js') }}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/kelly.js') }}"></script>
      <!-- am maps JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/maps.js') }}"></script>
      <!-- am worldLow JavaScrvipt -->
      <script src="{{ URL::asset('resources/assets/js/worldLow.js') }}"></script>
      <!-- Raphael-min JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/raphael-min.js') }}"></script>
      <!-- Morris JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/morris.js') }}"></script>
      <!-- Morris min JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/morris.min.js') }}"></script>
      <!-- Flatpicker Js -->
      <script src="{{ URL::asset('resources/assets/js/flatpickr.js') }}"></script>
      <!-- Style Customizer -->
      <script src="{{ URL::asset('resources/assets/js/style-customizer.js') }}"></script>
      <!-- Chart Custom JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/chart-custom.js') }}"></script>
      <!-- Custom JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/custom.js') }}"></script>

      <script>
        $(document).ready(function(){
          $(document).on("submit","#login-form",function(evt){
            evt.preventDefault();
            $.ajax({
              url:'{{url("admin/validate")}}',
              data:$(this).serialize(),
              dataType:'json',
              type:'post',
              success:function(response){
                if(response.code == 200){
                  window.location.href = '{{url("admin/")}}';
                }else{
                  alert(response.msg)
                }
              }
            })
          })
        });
    </script>

   </body>
</html>
