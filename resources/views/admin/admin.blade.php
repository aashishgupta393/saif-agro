<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Saif Agro Group</title>



      <!-- Favicon -->
      <link rel="shortcut icon" href="{{ URL::asset('resources/assets/images/favicon.ico') }}" />
      <!-- Bootstrap CSS -->
      <link href="{{ URL::asset('resources/assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <!-- Typography CSS -->
      <link href="{{ URL::asset('resources/assets/css/typography.css') }}" rel="stylesheet">
      <!-- Style CSS -->
      <link href="{{ URL::asset('resources/assets/css/style.css') }}" rel="stylesheet">
      <!-- Responsive CSS -->
      <link href="{{ URL::asset('resources/assets/css/responsive.css') }}" rel="stylesheet">
      <!-- Full calendar -->
      <link href="{{ URL::asset('resources/assets/fullcalendar/core/main.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('resources/assets/fullcalendar/daygrid/main.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('resources/assets/fullcalendar/timegrid/main.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('resources/assets/fullcalendar/list/main.css') }}" rel="stylesheet">

      <link href="{{ URL::asset('resources/assets/css/flatpickr.min.css') }}" rel="stylesheet">
      <script src="{{ URL::asset('resources/assets/js/jquery.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/popper.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/bootstrap.min.js') }}"></script>
      <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

   </head>
   <body>
     <div id="loading" >
        <div id="loading-center">
        </div>
     </div>
     <!-- loader END -->
     <!-- Wrapper Start -->
     <div class="wrapper">
        <!-- Sidebar  -->
          @include('admin.sidebar',['page'=>$page])
        <!-- TOP Nav Bar -->
          @include('admin.topbar')
        <!-- TOP Nav Bar END -->

        <!-- Page Content  -->
        <div id="content-page" class="content-page">
          	@yield('content')
        </div>
     </div>
     <!-- Wrapper END -->
      <!-- Footer -->
     <footer class="iq-footer">
        <div class="container-fluid">
           <div class="row">
              <div class="col-lg-6">
                 <ul class="list-inline mb-0">
                    <li class="list-inline-item"><a href="privacy-policy.html">Privacy Policy</a></li>
                    <li class="list-inline-item"><a href="terms-of-service.html">Terms of Use</a></li>
                 </ul>
              </div>
              <div class="col-lg-6 text-right">
                 Copyright 2020 <a href="#">Saif Agro</a> All Rights Reserved.
              </div>
           </div>
        </div>
     </footer>


      <!-- Appear JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/jquery.appear.js') }}"></script>
      <!-- Countdown JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/countdown.min.js') }}"></script>
      <!-- Counterup JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/waypoints.min.js') }}"></script>
      <script src="{{ URL::asset('resources/assets/js/jquery.counterup.min.js') }}"></script>
      <!-- Wow JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/wow.min.js') }}"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/apexcharts.js') }}"></script>
      <!-- Slick JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/slick.min.js') }}"></script>
      <!-- Select2 JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/select2.min.js') }}"></script>
      <!-- Owl Carousel JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/owl.carousel.min.js') }}"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/jquery.magnific-popup.min.js') }}"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/smooth-scrollbar.js') }}"></script>
      <!-- lottie JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/lottie.js') }}"></script>
      <!-- am core JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/core.js') }}"></script>
      <!-- am charts JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/charts.js') }}"></script>
      <!-- am animated JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/animated.js') }}"></script>
      <!-- am kelly JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/kelly.js') }}"></script>
      <!-- am maps JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/maps.js') }}"></script>
      <!-- am worldLow JavaScrvipt -->
      <script src="{{ URL::asset('resources/assets/js/worldLow.js') }}"></script>
      <!-- Raphael-min JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/raphael-min.js') }}"></script>
      <!-- Morris JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/morris.js') }}"></script>
      <!-- Morris min JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/morris.min.js') }}"></script>
      <!-- Flatpicker Js -->
      <script src="{{ URL::asset('resources/assets/js/flatpickr.js') }}"></script>
      <!-- Style Customizer -->
      <script src="{{ URL::asset('resources/assets/js/style-customizer.js') }}"></script>
      <!-- Chart Custom JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/chart-custom.js') }}"></script>
      <!-- Custom JavaScript -->
      <script src="{{ URL::asset('resources/assets/js/custom.js') }}"></script>
   </body>
</html>
