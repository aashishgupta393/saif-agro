@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">My Files</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <ul class="nav nav-tabs" id="myTab-two" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" id="home-tab-two" data-toggle="tab" href="#home-two" role="tab" aria-controls="home" aria-selected="true">
                       My Files
                     </a>
                  </li>
               </ul>
               <div class="tab-content" id="myTabContent-1">
                  <div class="tab-pane fade show active" id="home-two" role="tabpanel" aria-labelledby="home-tab-two">
                     <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered" >
                           <thead>
                              <tr>
                                 <th>Sr No.</th>
                                 <th>File Name</th>
                                 <th>Preview</th>
                                 <th>Linked With </th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($files as $key=>$file)
                              @php
                                $fileName = pathinfo($file)['basename'];
                                if($file->linked_with == 'product'){
                                  $LinkTitle = $file->link->product_title;
                                }else{
                                  $LinkTitle = $file->link->title;
                                }
                              @endphp
                              <tr>
                                 <td>{{$key+1}}</td>
                                 <td>
                                   <img src="{{$appUrl.'/public/uploads/'.$fileName}}" alt="profile" class="img-fluid rounded avatar-40 text-center">
                                 </td>
                                 <td>{{$fileName}}</td>
                                 <td>
                                   {{ucfirst($file->linked_with)}} --
                                   {{$LinkTitle}}
                                 </td>
                                 <td>{{$key+1}}</td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready( function () {
     $('#datatable').DataTable();
   })
</script>
@endsection
