@extends('admin.admin')
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-lg-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Update Company Details</h4>
               </div>
            </div>

            <?php
            $company = Session::get('admin');

            ?>

            <form method="post" action="{{url('admin/update-company-details')}}" enctype="multipart/form-data">
            <div class="iq-card-body">
               <div class="new-user-info">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <label for="company_name">Company Name:</label>
                           <input type="text" name="company_name" value="{{$company->company_name}}" class="form-control"  id="company_name" placeholder="Compnay Name">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="company_email">Company Email:</label>
                           <input type="text" name="company_email" value="{{$company->company_email}}" class="form-control"  id="company_email" placeholder="Company Email">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="company_contact_no">Company Contact No:</label>
                           <input type="text" name="company_contact_no" value="{{$company->company_contact_no}}" class="form-control"  id="company_contact_no" placeholder="Product Title">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="GSTIN">Company GSTIN:</label>
                           <input type="text" name="GSTIN" class="form-control" value="{{$company->GSTIN}}" id="GSTIN" placeholder="Product Title">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="address">Address:</label>
                           <textarea name="address" class="form-control" placeholder="Address 1" >{{$company->address}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="address1">Address 1:</label>
                           <textarea name="address1" class="form-control" placeholder="Address 2" >{{$company->address1}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="city">City</label>
                           <input type="text" name="city" class="form-control" value="{{$company->city}}"  id="city" placeholder="City">
                        </div>
                        <div class="form-group col-md-6">
                           <label for="category">State:</label>
                           <select class="form-control" id="category" name="state">
                           @foreach(App\States::orderBy('name','asc')->get() as $state)
                              <option  value="{{$state->name}}" {{$company->state == $state->name ? 'selected=selected': ''}} >{{$state->name}}</option>
                           @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-6">
                           <label for="zip_code">Zip Code</label>
                           <input type="text" name="zip_code" class="form-control"  value="{{$company->zip_code}}" id="zip_code" placeholder="Product Title">
                        </div>

                        <div class="form-group col-md-6">
                           <label for="password">New Password</label>
                           <input type="password" name="password" class="form-control"  >
                        </div>


                     </div>
                     <hr>
               </div>
                 <div id="variants">
                 </div>
                 <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
         </div>
      </div>
   </div>
</div>
</div>

@endsection
