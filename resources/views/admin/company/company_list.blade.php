@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">My Companies Details</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <div class="table-responsive">
                  <table id="datatable" class="table table-striped table-bordered" >
                     <thead>
                        <tr>
                           <th>Company Name</th>
                           <th>Company Email</th>
                           <th>Company Contact No.</th>
                           <th>Razorpay Payment Mode</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @foreach(App\Companies::where(['id'=>Session::get('admin')->id])->get() as $key=>$company)
                        <tr>
                           <td>{{$company->company_name}}</td>
                           <td>{{$company->company_email}}</td>
                           <td>{{$company->company_contact_no}}</td>
                           <td>
                             <div class="custom-control custom-switch custom-switch-text custom-switch-color custom-control-inline">
                              <div class="custom-switch-inner">
                                 <input type="checkbox" class="custom-control-input bg-success" {{($company->payment_mode == 1 ) ? "checked" : ""}} id="customSwitch-{{$key}}"  >
                                 <label class="custom-control-label" for="customSwitch-{{$key}}" data-on-label="Live" data-off-label="Test">
                                 </label>
                              </div>
                           </div>
                         </td>
                           <td>
                             <div class="flex align-items-center list-user-action">
                               <a class="iq-bg-primary"  data-placement="top"  title="" data-original-title="View" href="#"  data-toggle="modal" data-target="#ViewModal{{$company->id}}" >
                                 <i class="ri-eye-line"></i>
                               </a>
                               <a class="iq-bg-primary"  data-placement="top"  href="{{url('/admin/edit-company')}}"  >
                                 <i class="ri-pencil-line"></i>

                               </a>
                             </div>
                             <div class="modal fade" id="ViewModal{{$company->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                   <div class="modal-content">
                                      <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLabel">Company Details.
                                           </h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                         </button>
                                      </div>
                                      <div class="modal-body">
                                        <table class="table mb-0 table-borderless">
                                          <thead>
                                             <tr>
                                                <th scope="col">Copmany Name</th>
                                                <td>{{$company->company_name}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Email</th>
                                                <td>{{$company->company_email}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Contact No</th>
                                                <td>{{$company->company_contact_no}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Address1</th>
                                                <td>{{$company->address1}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Address2</th>
                                                <td>{{$company->address1}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">City</th>
                                                <td>{{$company->city}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">State</th>
                                                <td>{{$company->state}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Zip</th>
                                                <td>{{$company->zip_code}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Razorpay Key ID(Test)</th>
                                                <td>{{$company->test_key_id}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Razorpay Key Seceret(Test)</th>
                                                <td>{{$company->test_key_secrete}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Razorpay Key ID(Live)</th>
                                                <td>{{$company->live_key_id}}</td>
                                             </tr>
                                             <tr>
                                                <th scope="col">Razorpay Key Seceret(Live)</th>
                                                <td>{{$company->live_key_secrete}}</td>
                                             </tr>
                                          </thead>
                                       </table>
                                      </div>
                                      <div class="modal-footer">
                                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      </div>
                                   </div>
                                </div>
                             </div>

                           </td>
                        </tr>
                     @endforeach
                   </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready( function () {
     $('#datatable').DataTable();
     $('.custom-control-input').on('change',function(){
       $status = 0;
       if($(this).prop('checked') == true){
        $status = 1;
       }
       $.ajax({
         url: '{{URL("/admin/update-payment-status")}}',
         type: 'post',
         dataType: 'json',
         data: {status:$status},
         success: function(res) {
           alert(res.msg);
          location.reload();
         }
       })
     })
   });
</script>
@endsection
