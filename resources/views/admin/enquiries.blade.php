@extends('admin.admin')
@section('content')
<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">

<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="iq-card">
            <div class="iq-card-header d-flex justify-content-between">
               <div class="iq-header-title">
                  <h4 class="card-title">Enquiries</h4>
               </div>
            </div>
            <div class="iq-card-body">
               <div class="table-responsive">
                  <table id="datatable" class="table table-striped table-bordered" >
                     <thead>
                        <tr>
                          <th>Customer Name</th>
                          <th>Email</th>
                          <th>Contact no</th>
                          <th>Message</th>
                          <th>Enquire About</th>
                        </tr>
                     </thead>
                    @php
                    $accounts = [
                      "1" => ["acc_holder_name"=>"Saif Agro Tech","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8612192373","ifsc_code"=>"KKBK0008038","branch"=>"S.J.P ROAD"],
                      "2" => ["acc_holder_name"=>"TMM Trades","bank_name"=>"ICICI BANK","acc_no"=>"625105501953","ifsc_code"=>"ICIC0006251","branch"=>"NR Road"],
                      "3" => ["acc_holder_name"=>"SMS INDUSTRIAL COMPONENT","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8012868526","ifsc_code"=>"KKBK0000423","branch"=>"BANGALORE"],
                    ];
                    @endphp
                     <tbody>
                     @foreach(App\Enquiry::with('product')->where('company_id', Session::get('admin')->id)->get() as $key=>$enquiry)
                        <tr>
                           <td>{{$enquiry->name}}</td>
                           <td>{{$enquiry->email}}</td>
                           <td>{{$enquiry->mobile_no}}</td>
                           <td>{{$enquiry->message}}</td>
                           <td>{{$enquiry->product->product_title}}</td>
                        </tr>
                     @endforeach
                   </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready( function () {
  $('#datatable').DataTable();
});
</script>
@endsection
