@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">My Account Page</li>
			</ol>
		</div>
	</div>

  <style>
  #user-info,.bank-details {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    #user-info td, #user-info th ,.bank-details td,.bank-details th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    #user-info tr:nth-child(even) {
        background-color: #dddddd;
    }
    .accordion {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }
    .active, .accordion:hover {
        background-color: #ccc;
    }
    .panel {
        padding: 0 18px;
        display: none;
        background-color: white;
        overflow: hidden;
    }
    td.order_label {
        width: 50%;
        font-weight: bold;
    }
    .my-orders {
        width: 44%;
        margin-left: 3%;
    }
    .user-info {
        width: 44%;
        margin-left: 3%;
    }
    .logout-div {
        text-align: right;
        margin-right: 5%;
        margin-top: 2%;
        margin-bottom: 2%;
    }
    .account {
        display: flex;
        margin-bottom: 2%;
    }
    .account-heading {
        margin-bottom: 4%;
        text-align: center;
    }
  </style>
@if(Session::has('current_user'))
<div class="logout-div">
  Customer Account Page.
  <a href="{{url('/logout')}}"> Logout</a>
</div>
@php
$accounts = [
  "1" => ["acc_holder_name"=>"Saif Agro Tech","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8612192373","ifsc_code"=>"KKBK0008038","branch"=>"S.J.P ROAD"],
  "2" => ["acc_holder_name"=>"TMM Trades","bank_name"=>"ICICI BANK","acc_no"=>"625105501953","ifsc_code"=>"ICIC0006251","branch"=>"NR Road"],
  "3" => ["acc_holder_name"=>"SMS INDUSTRIAL COMPONENT","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8012868526","ifsc_code"=>"KKBK0000423","branch"=>"BANGALORE"],
];
@endphp

<style>
.select-box{
	font-size: 1em;
	padding: 0.9em 1em;
	width: 100%;
	color: #999;
	outline: none;
	border: 1px solid #E2DCDC;
	background: #FFFFFF;
	margin: 0em 0em 1em 0em;
}
.open-edit {
	cursor: pointer;
	font-size: 20px;
	float: right;
	font-weight: bold;
}
</style>

<div class="account" style="">
   <div class="user-info" >
      <h2 class="account-heading">My Profile   <a class="open-edit" >Edit Profile</a></h2>

			<form action="{{url('update-profile')}}" method="post">
      <table id="user-info">
         <tr>
            <th>Username</th>
            <td>
							<input type="text" class="edit-field" name="username" value="{{ Session::get('current_user')->username}}">
							<span class="static-span">{{ Session::get('current_user')->username}}</span>
						</td>
         </tr>
         <tr>
            <th>Contact No</th>
						<td>
							<input type="text" class="edit-field" name="contact_no" value="{{ Session::get('current_user')->contact_no}}">
							<span class="static-span">{{ Session::get('current_user')->contact_no}}</span>
						</td>
         </tr>
         <tr>
            <th>Email</th>
						<td>
							<input type="text" class="edit-field" name="email" value="{{ Session::get('current_user')->email}}">
							<span class="static-span">{{ Session::get('current_user')->email}}</span>
						</td>
         </tr>
         <tr>
            <th>Company Name</th>
						<td>
							<input type="text" class="edit-field" name="company_name" value="{{ Session::get('current_user')->company_name}}">
							<span class="static-span">{{ Session::get('current_user')->company_name}}</span>
						</td>
         </tr>
         <tr>
            <th>Company Address</th>
						<td>
							<input type="text" class="edit-field" name="company_address" value="{{ Session::get('current_user')->company_address}}">
							<span class="static-span">{{ Session::get('current_user')->company_address}}</span>
						</td>
         </tr>
         <tr>
            <th>City</th>
						<td>
							<input type="text" class="edit-field" name="city" value="{{ Session::get('current_user')->city}}">
							<span class="static-span">{{ Session::get('current_user')->city}}</span>
						</td>
         </tr>
         <tr>
            <th>State</th>
						<td>
							<select class="select-box edit-field" name="state" >
								 <option value=""> Please Select</option>
								 @foreach($cities as $state)
										<option value="{{$state->name}}" >{{$state->name}}</option>
								 @endforeach
							</select>
							<span class="static-span">{{ Session::get('current_user')->state}}</span>
						</td>
         </tr>
         <tr>
            <th>Zip Code</th>
						<td>
							<input type="text" class="edit-field" name="zip_code" value="{{ Session::get('current_user')->zip_code}}">
							<span class="static-span">{{ Session::get('current_user')->zip_code}}</span>
						</td>
         </tr>
         <tr>
            <th>GST No</th>
						<td>
							<input type="text" class="edit-field" name="gst_no" value="{{ Session::get('current_user')->gst_no}}">
							<span class="static-span">{{ Session::get('current_user')->gst_no}}</span>
						</td>
         </tr>
				 <tr class="edit-field">
						<td colspan="2" style="text-align:right;">
							<button  class="btn btn-info" > Update</button>
						</td>
         </tr>
      </table>
		</form>
   </div>
   <div class="my-orders">
     <h2 class="account-heading"> My Orders</h2>
     @foreach(App\Orders::with(['customer','items'])->where(['customer_id'=>Session::get('current_user')->id])->orderBy('id', 'DESC')->get() as $order)
     <button style="margin-bottom:2%" type="button" class="accordion">
       {{$order->order_no}} / {{date('F d,Y',strtotime($order->created_at))}}
     </button>
     <div class="panel">
        <h3> Order Items
					@if($order->payment_status == 'paid')
	          <a style="float:right;" href="{{url('/invoice-print')}}?id={{$order->id}}">
	             <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
	           </a>
					 @endif
         </h3>
         <p>Payment Status : {{$order->payment_status}} </p>
        <table class="table mb-0 table-borderless">
           <thead>
              <tr>
                 <th scope="col">Item Name</th>
                 <th scope="col">Price</th>
                 <th scope="col">Qty</th>
                 <th scope="col">Total</th>
              </tr>
           </thead>
           <tbody>
              @foreach($order->items as $item)
              <tr>
                 <td>{{$item->product_title}}</td>
                 <td>&#x20b9; {{$item->price}}</td>
                 <td>{{$item->qty}}</td>
                 <td>&#x20b9; {{$item->item_total}}</td>
              </tr>
              @endforeach
              <tr>
                 <td colspan="3">Subtotal</td>
                 <td>&#x20b9; {{$order->subtotal}}</td>
              </tr>
              <tr>
                 <td colspan="3">Packing Charges</td>
                 <td>&#x20b9; {{$order->packing_charges}}</td>
              </tr>
              <tr>
                 <td colspan="3">Total</td>
                 <td>&#x20b9; {{$order->order_amount}}</td>
              </tr>
           </tbody>
        </table>
         @if($order->payment_status == 'pending')
          <h2 class="account-heading">Bank Details</h2>
         @php
         $companyAccount = $accounts[$order->company_id];
         @endphp
        <table class="bank-details">
           <thead>
              <tr>
                <th>Bank Name</th>
                <td>{{$companyAccount['bank_name']}}</td>
              </tr>
              <tr>
                <th>Branch</th>
                <td>{{$companyAccount['branch']}}</td>
              </tr>
              <tr>
                <th>IFSC</th>
                <td>{{$companyAccount['ifsc_code']}}</td>
              </tr>
              <tr>
                <th>Account Name</th>
                <td>{{$companyAccount['acc_holder_name']}}</td>
              </tr>
              <tr>
                <th>Account Number</th>
                <td>{{$companyAccount['acc_no']}}</td>
              </tr>
           </thead>
         </table>
         @endif
     </div>
     @endforeach
   </div>
</div>
<style>
.edit-field{
	display: none;
}
</style>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function() {
   	this.classList.toggle("active");
   	var panel = this.nextElementSibling;
   	if (panel.style.display === "block") {
   		panel.style.display = "none";
   	} else {
   		panel.style.display = "block";
   	}
   });
   }
	 $(document).on('click','.open-edit',function(){
		 $('.edit-field,.static-span').toggle();
	 })
</script>
@else
<p> <a href="{{url('/sign-in')}}">Please Login </a> </p>
@endif
@endsection
