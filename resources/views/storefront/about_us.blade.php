@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
   <div class="container">
      <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
         <li><a href="{{url('/')}}">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
         </li>
         <li class="active">Privacy Policy</li>
      </ol>
   </div>
</div>
<div class="container">
   <h2 style="text-align: center;margin-top: 23px !important;padding-bottom: 54px;font-size: 31px;">ABOUT US</h2>
   <p>
      SAIF AGRO GROUP are one of the largest Dealers& Importers of <b>Agricultural Machinery, Engines, Water Pump, HTP, Compressors, Power Toolsand all Spares Parts.</b><br><br>
      Since 3 Generation we have been Catering to large sector of dealers and Farmers with Agricultural Machine and their Spare Parts. Pioneers in innovation and cost optimization methods in Agricultural machineries. <br><br>
      <b>Our Contributions:</b> Couple of decades back when agricultural sector was looking for new technologies and cost optimization methods, founders of the parent company “Reliance Tools” Shk Yusuf Bhai Madarwala and Mr. Hyder Madarwala to assist small farmers with spare parts and repair, got a chain saw machine and dis-assembled it and started selling the machine spares for reasonable rate. This avoided them from buying new machines which then were also imported and was costing much higher.<br><br>
      Selling Spares by dis-assembling the new machines became trend since then and we were one of the starters of these innovations. <br><br>
      We established our set up in Bangalore, Karnataka - India Since 2005. Today we have our range of products distributed based on Categories under 3 banners as listed below:<br><br>
   <div class= center_image>
      <img style="width: 65%;"src="/resources/views/storefront/about_image.png">
   </div>
   </p>
   <br><br>
</div>
<style>
   h1, h2, h3, h4, h5, h6 {
   margin: 0;
   font-family: 'Roboto';
   margin-top: 22px;
   margin-bottom: 22px;
   }
   .center_image {
   text-align: center;
   }
</style>
@endsection
