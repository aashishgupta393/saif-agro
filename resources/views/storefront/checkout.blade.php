@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
  <div class="container">
    <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
      <li><a href="{{url('/')}}">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
      </li>
      <li class="active">Your Cart</li>
    </ol>
  </div>
</div>
@php
function ReturnPackingAmount($amountToCheck)
{
  $amountToCheck = (int)$amountToCheck;
    $packingArray=[
      ['min_cart_amt'=>1,'max_cart_amt'=>2000,'min_pac_charge'=>0,'max_pac_charge'=>50,],
      ['min_cart_amt'=>2001,'max_cart_amt'=>10000,'min_pac_charge'=>50,'max_pac_charge'=>100],
      ['min_cart_amt'=>10001,'max_cart_amt'=>30000,'min_pac_charge'=>100,'max_pac_charge'=>150],
      ['min_cart_amt'=>30001,'max_cart_amt'=>100000,'min_pac_charge'=>150,'max_pac_charge'=>200,],
      ['min_cart_amt'=>100001,'max_cart_amt'=>200000,'min_pac_charge'=>250,'max_pac_charge'=>300,],
      ['min_cart_amt'=>200001,'max_cart_amt'=>500000,'min_pac_charge'=>250,'max_pac_charge'=>300,],
    ];
    $packing = 0;
    foreach($packingArray as $singlePacking){
      if($amountToCheck >= $singlePacking['min_cart_amt'] && $amountToCheck <=$singlePacking['max_cart_amt']){
        $minPackCharge = $singlePacking['min_pac_charge'];
        $maxPackCharge = $singlePacking['max_pac_charge'];
        $checkForMinimum = ($amountToCheck + $minPackCharge) / 50;
        if(is_int($checkForMinimum)){
        	if($minPackCharge == 0){
        	  $packing = $amountToCheck+$maxPackCharge;
        	}else{
        	  $packing = $amountToCheck+$minPackCharge;
        	}
        }else{
          $myVar2 =  $amountToCheck+$maxPackCharge;
          $nearestHundred = floor($myVar2/100)*100;
          $amtss = $nearestHundred-$amountToCheck;
          if($amtss >= $minPackCharge && $amtss <=$maxPackCharge){
            $packing =  ($nearestHundred-$amountToCheck)+$amountToCheck;
          }else{
            $packing =  ($nearestHundred-$amountToCheck+50)+$amountToCheck;
          }
        }
      }
    }
    return $packing;
}


$cartCount = 0;
if(Session::has('cart')){
  $cart = Session::get('cart');
  $cartCount = count($cart);
  $itmeByCompnay=[];
  foreach(App\Companies::get() as $cartompanies){
      $companyProducts=[];
     foreach ($cart as $pid =>$itm){
       $itm = (object)$itm;
       if($itm->company_id == $cartompanies->id){
         $companyProducts[$pid]=$itm;
       }
     }
     $cartompanies->cart_items=$companyProducts;
     $itmeByCompnay[]=$cartompanies;
  }
}
@endphp
<style>
.cart-company-heading {
    color: #157743;
    font-size: 40px;
    font-weight: bold;
    border-bottom: 5px solid red;
    margin-bottom: 3%;
}
.calculations {
    text-align: left;
    font-size: 20px;
    color: #000;
    padding-top: 28px !important;
    line-height: 40px;
}
input.razorpay-payment-button, .pay-later-btn {
    background: #157743;
    color: #fff;
    width: 40%;
    margin-top: 13px;
    border: 1px solid;
    padding: 5px;
    letter-spacing: 1px;
    border-radius: 9px;
}
input.razorpay-payment-button:hover, .pay-later-btn {
    background: #57ab41;
}
span.total_color {
    color: #157743;
    font-weight: 700;
}
.select-box {
    font-size: 1em;
    padding: 0.9em 1em;
    width: 100%;
    color: #999;
    outline: none;
    border: 1px solid #E2DCDC;
    background: #FFFFFF;
    margin: 0em 0em 1em 0em;
}
.custom-lable {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    width: 50%;
}
.custom-lable input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
.custom-lable .checkmark {
    position: absolute;
    top: 9px;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
}
.custom-lable:hover input~.checkmark {
    background-color: #ccc;
}
.custom-lable input:checked~.checkmark {
    background-color: #157743;
}
.custom-lable .checkmark:after {
    content: "";
    position: absolute;
    display: none;
}
.custom-lable input:checked~.checkmark:after {
    display: block;
}
.custom-lable .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
}
.payment-container {
    display: flex;
    width: 50%;
}
.single-form {
    display: none;
}
.custom-checkbox {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.custom-checkbox input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}
.custom-checkbox .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
}
.custom-checkbox:hover input~.checkmark {
    background-color: #ccc;
}
.custom-checkbox input:checked~.checkmark {
    background-color: #2196F3;
}
.custom-checkbox .checkmark:after {
    content: "";
    position: absolute;
    display: none;
}
.custom-checkbox input:checked~.checkmark:after {
    display: block;
}
.custom-checkbox .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>


<div class="shiping-address" style="display:none;">
  @include('storefront.shipping')
</div>
<div class="cart-items">
  <div class="container">
    <h3 class="wow fadeInUp animated" data-wow-delay=".5s">My Shopping Cart({{$cartCount}})</h3>

    @if($message = Session::get('error'))
        <div  style="opacity:1" class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Error!</strong> {{ $message }}
        </div>
    @endif
    @if($message = Session::get('success'))
        <div style="opacity:1" class="alert alert-success alert-dismissible fade {{ Session::has('success') ? 'show' : 'in' }}" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>Success!</strong> {{ $message }}
        </div>
    @endif


    @if(Session::has('cart'))
      @foreach($itmeByCompnay as $singleCompany)

            @if(count($singleCompany->cart_items) > 0)

            @php
            $key_id = $singleCompany->test_key_id;
            $key_secret = $singleCompany->test_key_secrete;
            if($singleCompany->payment_mode == 1){
              $key_id = $singleCompany->live_key_id;
              $key_secret = $singleCompany->live_key_secrete;
            }
            @endphp
            <h2 class="cart-company-heading"> Company Name : {{$singleCompany->company_name}}</h2>
            @php
            $singleCompanyTotal=0;
            @endphp
            @foreach($singleCompany->cart_items as $item)
            @php
              $productImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
              if (@getimagesize($appUrl.'/'.$item->image)) {
                $productImage = $appUrl.'/'.$item->image;
              }
              $singleProductTotal=$item->price*$item->qty;
              $singleCompanyTotal+=$singleProductTotal;
              $variant_key = $item->pid."--var--".$item->variant;
            @endphp
              <div class="cart-header">
                <a href="{{url('/remove')}}?id={{$variant_key}}">
                  <div class="alert-close"> </div>
                </a>
                <div class="cart-sec simpleCart_shelfItem">
                  <div class="cart-item cyc">
                    <a href="{{url('/product')}}?id={{$item->pid}}">
                      <img src="{{$productImage}}" class="img-responsive" alt="">
                    </a>
                  </div>
                  <div class="cart-item-info">
                    <h4>
                      <a href="{{url('/product')}}?id={{$item->pid}}">{{$item->title}} </a>
                    </h4>
                    <ul class="qty">
                      <li><p>Qty : {{$item->qty}}</p></li>
                    </ul>
                    <div class="delivery">
                      <p>Price : &#x20b9; {{$singleProductTotal}}</p>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
          @endforeach
          <div class="calculations">
            <p>
              Subtotal :  <span class="total_color">&#x20b9; {{$singleCompanyTotal}}/-</span><br>
              Packing Charges  : <span class="total_color">&#x20b9; {{ReturnPackingAmount($singleCompanyTotal)-$singleCompanyTotal}}/-</span><br>
              Total Paybale Amount : <span class="total_color">&#x20b9; {{ReturnPackingAmount($singleCompanyTotal)}}/-</span>
            </p>


            <h2>Payment Options</h2>
            <div class="payment-container">
              <label class="custom-lable">Pay Now
                <input type="radio"class="payment-radio"  value="now"  name="order{{$singleCompany->id}}">
                <span class="checkmark"></span>
              </label>
              <label class="custom-lable">Bank Transfer
                <input type="radio" class="payment-radio" value="later" name="order{{$singleCompany->id}}">
                <span class="checkmark"></span>
              </label>
            </div>

            <div class="pay-now-address single-form">
              <form action="{{ route('payment') }}"  method="POST" >
                  <input type="hidden" name="user_id" value="{{ Session::get('current_user')->id}}">
                  <input type="hidden" name="company_id" value="{{$singleCompany->id}}">
                  <input type="hidden" name="subtotal" value="{{$singleCompanyTotal}}">
                  <input type="hidden" name="subtotal" value="{{$singleCompanyTotal}}">
                  <input type="hidden" name="packing_charges" value="{{ReturnPackingAmount($singleCompanyTotal)-$singleCompanyTotal}}">
                  <input type="hidden" name="total" value="{{ReturnPackingAmount($singleCompanyTotal)}}">

                    @include('storefront.my_shipping')


                  <label class="custom-checkbox">Different Shipping Address
                    <input type="checkbox" name="different_shipping" class="required-shipping" value="yes" >
                    <span class="checkmark"></span>
                  </label>

                  @include('storefront.shipping')

                  @csrf
                  <script src="https://checkout.razorpay.com/v1/checkout.js"
                          data-key="{{$key_id}}"
                          data-amount="{{ReturnPackingAmount($singleCompanyTotal)*100}}"
                          data-buttontext="Pay With Bank"
                          data-name="Saif Agro Group"
                          data-description="Paymet For {{$singleCompany->company_name}}"
                          data-image="https://saifagrogroup.com/resources/views/storefront/main_logo.png"
                          data-prefill.contact="{{ Session::get('current_user')->contact_no}}"
                          data-prefill.email="{{ Session::get('current_user')->email}}"
                          data-theme.color="#ff752950">
                  </script>

              </form>
          </div>
          <div class="pay-later-address single-form">
            <form action="{{ route('payment') }}" method="POST" >
                <input type="hidden" name="user_id" value="{{ Session::get('current_user')->id}}">
                <input type="hidden" name="company_id" value="{{$singleCompany->id}}">
                <input type="hidden" name="subtotal" value="{{$singleCompanyTotal}}">
                <input type="hidden" name="packing_charges" value="{{ReturnPackingAmount($singleCompanyTotal)-$singleCompanyTotal}}">
                <input type="hidden" name="total" value="{{ReturnPackingAmount($singleCompanyTotal)}}">
                  @include('storefront.my_shipping')
                <label class="custom-checkbox">Different Shipping Address
                  <input type="checkbox" name="different_shipping" class="required-shipping" value="yes" >
                  <span class="checkmark"></span>
                </label>
                  @include('storefront.shipping')

                <button type="submit" class="btn btn-info pay-later-btn" > Pay Later</button>
                  @csrf
            </form>
          </div>

          </div>
          @endif
      @endforeach

    @else
      <p>Empty Cart</p>
    @endif

  </div>
</div>
  @if($message = Session::get('success'))
  <script>
   setTimeout(function(){
     <?php
      Session::forget('success');
     ?>
   }, 3000);
  </script>
  @endif
  <script>
  $('.required-shipping').on('change',function(){
    $('.shipping-container').hide();
    if($(this).prop('checked') == true){
      $(this).parents('form').find('.shipping-container').show();
    }
  });

  $('.payment-radio').on('change',function(){
    $radioVal= $(this).parents('.payment-container').find('.payment-radio:checked').val();
    $(this).parents('.calculations').find('.single-form').hide();
      switch($radioVal) {
        case 'now':
        $(this).parents('.calculations').find('.pay-now-address').show();
        break;
        case 'later':
        $(this).parents('.calculations').find('.pay-later-address').show();
        break;
        default:
      }
  })
  </script>
@endsection
