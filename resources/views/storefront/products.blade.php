@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">
          @php
					$showOnlyProducts=true;
            if(isset($cat_id)){
              $catData = App\Categories::where(['id'=>$cat_id])->first();
              echo "Products of Category  : ".$catData->title;
							$showOnlyProducts = false;
            }else{
							if(isset($sub_cat_id)){
								$catData = App\SubCategories::where(['id'=>$sub_cat_id])->first();
								$showOnlyProducts = false;
								echo "Products of Sub Category  : ".$catData->title;
							}else{
								if(isset($sub_cat2_id)){
									$catData = App\SubCategories2::where(['id'=>$sub_cat2_id])->first();
									$showOnlyProducts = false;
									echo "Products of Sub Category 2  : ".$catData->title;
									$showOnlyProducts = false;
								}else{
									echo "Products";
								}
							}

            }
          @endphp
        </li>
			</ol>
		</div>
	</div>
	<div class="products">
		<div class="container">
			@php
				$sequence = 3;
        $companies=[];
         foreach(App\Companies::get() as $company){
          $companies[$company->id]=$company;
        }
        if(isset($cat_id) || isset($sub_cat_id)){
            $parentClass = "col-md-12";
        }else{
            $parentClass = "col-md-9";
        }
			@endphp
			<div class="{{$parentClass}} product-model-sec">
          @php
          $allProducts = App\Products::get();
          if(isset($cat_id)){
            $allProducts = App\Products::where(['cat_id'=>$cat_id])->get();
          }
          if(isset($sub_cat_id)){
            $allProducts = App\Products::where(['sub_cat1'=>$sub_cat_id])->get();
          }
          if(isset($sub_cat2_id)){
            $allProducts = App\Products::where(['sub_cat2'=>$sub_cat2_id])->get();
          }

          @endphp
          @if(count($allProducts) > 0)
				  @foreach($allProducts as $ckt=>$product)
						@php
						$img = $ckt+1;
						$customClass="";
						if(($img+1) % $sequence == 0 && $img!=0) {
							$customClass = "product-grids-mdl";
						}

						$productImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
						 if(!empty($product->images)){
								 $mainImage = json_decode($product->images)[0];
								 if (@getimagesize($appUrl.'/'.$mainImage)) {
									 $productImage = $appUrl.'/'.$mainImage;
								 }
						 }

						@endphp
						<div data-company_id="{{$product->company_id}}"  class="product-grids <?=$customClass?> simpleCart_shelfItem">
							<div class="new-top">
								<a href="{{url('/product')}}?id={{$product->id}}">
									<img src="{{$productImage}}" class="img-responsive" alt=""/>
								</a>
								<div class="new-text">
									<ul>
										<li><a href="{{url('/product')}}?id={{$product->id}}">Quick View </a></li>
										<li><a class="item_add" href="{{url('/product')}}?id={{$product->id}}"> Add to cart</a></li>
										<li style="display:none;">
											<input  type="number" class="item_quantity" min="1" value="1">
										</li>
									</ul>
								</div>
							</div>
							<div class="new-bottom">
								<h5>
                  <a class="name" href="{{url('/product')}}?id={{$product->id}}">{{$product->product_title}} </a>
                </h5>
                @php
                $cdls = $companies[$product->company_id];
                @endphp
                <h6 class="company-name">{{$cdls->company_name}}</h6>
								@if(Session::has('current_user'))
									<div  class="ofr">
										<p><span class="item_price">&#x20b9; {{$product->product_amount}}</span></p>
									</div>
								@endif
							</div>
						</div>
					 @endforeach
           @else
            <p>No products Uploaded Yet For this Category/Subcategory</p>
           @endif
			</div>
      @if($showOnlyProducts && count($allProducts) >0)
        <div class="col-md-3 rsidebar">
  				<div class="rsidebar-top">
  					<div class="sidebar-row">
  						<h4>Filter By Companies</h4>
  						<div class="row row1 scroll-pane">
                 @foreach($companies as $company)
      							<label class="checkbox">
                      <input type="checkbox" value="{{$company->id}}" class="company-filter" name="checkbox"><i></i>{{$company->company_name}}
                    </label>
                @endforeach
  						</div>
  					</div>
  				</div>
  			</div>
      @endif
			<div class="clearfix"> </div>
		</div>
	</div>

<style>
.company-name {
color: red;
font-weight: 700;
font-size: 20px;
}
	.new-top img {
    height: 100px;
    margin-left: 27px;
    margin-right: 37px;
}
.new-bottom {
    text-align: center;
}
.new-bottom h5 a.name {
    font-size: 17px;
}
.new-bottom h5 {
    height: 57px;
}
.company-name {
    color: #86cb41 !important;
    font-size: 15px;
    margin-top: 12px;
    letter-spacing: 1px;
}

h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.new-text {
    background: #7ac63e;
}
</style>
<script>
$(document).on('change','.company-filter',function(evt) {
	$checked=[]
	$('.company-filter:checked').each(function(ind,chk){
		$checked.push(parseInt($(chk).val()));
	});
	if($checked.length > 0){
		$('.product-grids').hide();
		$('.product-grids').each(function(cn,prd){
			$company_id = $(prd).data('company_id');
			if($checked.indexOf($company_id) >=0){
				$(prd).show();
			}else{
				$(prd).hide();
			}
		})
	}else{
			$('.product-grids').show();
	}
})
</script>

  @endsection
