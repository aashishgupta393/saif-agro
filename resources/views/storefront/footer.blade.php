  <!-- Site footer -->
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-5">
            <a href="{{url('/')}}"><span style="color: #11ec11;"><img style="width: 50%; height: 56px;margin-bottom: 19px;"src="/resources/views/storefront/main_logo.png"></span></a>
            <p class="text-justify">SAIF AGRO GROUP are one of the largest Dealers& Importers of Agricultural Machinery, Engines, Water Pump, HTP, Compressors, Power Toolsand all Spares Parts.<br>
            Since 3 Generation we have been Catering to large sector of dealers and Farmers with Agricultural Machine and their Spare Parts. Pioneers in innovation and cost optimization methods in Agricultural machineries.</p>
             <a target="_blank" href="https://play.google.com/store/apps/details?id=com.theandroidclassroom.saifagro">
              <img class="footerplay_store" src="/resources/views/storefront/google-play.png">
            </a>
          </div>

          <div class="col-xs-12 col-md-3">
            <h6>INFORMATIONS</h6>
            <ul class="footer-links">
              <li><a href="about_us">About Us</a></li>
              <li><a href="{{url('/contact')}}">Contact Us</a></li>
              <li><a href="{{url('/account')}}">Accounts</a></li>
              <li><a href="{{url('/privacy')}}">Privacy Policy</a></li>
              <li><a href="{{url('/refunds_policys')}}">Refund Policy</a></li>
               <li><a href="{{url('/term_service')}}">Terms & Service</a></li>
            </ul>
          </div>

           <div class="col-xs-12 col-md-4">
            <h6>CONTACT DETAILS</h6>
            <ul class="footer-links">
              <li>
                <p style="position: absolute;"><i style=" font-size: 21px; color: #fff;" class="fa fa-map-marker" aria-hidden="true"></i></p>
                <p style="margin-left: 25px;">1st Floor, #33, SJP Road, Kalasipalayam, New Ext, Kumbarpet, Dodspet, Nagarathpete, Bengaluru, Karnataka 560002</p>
              </li>
              <li>
                <p style="position: absolute;"><i style=" font-size: 25px; color: #fff;" class="fa fa-mobile" aria-hidden="true"></i></p>
                <p style="margin-left: 24px;">+91 080 41227486</p></li>
              <li>
                <p style="position: absolute;left: 12px;"><i style=" font-size: 17px; color: #fff;" class="fa fa-envelope" aria-hidden="true"></i></p>
                <p style="margin-left: 25px;">enquiry@saifagrogroup.com</p></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by 
         <a style="color: #6ec441" href="phaseinfotech.in">Phase Infotech</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer>

<style>
  .footerplay_store{
    width: 130px;
    margin-right: 16px;
    margin-top: 3px;
  }
.site-footer {
    background-color: #26272b;
    padding: 45px 0 20px;
    font-size: 15px;
    line-height: 24px;
    color: #fff;
}
.site-footer hr
{
  border-top-color:#bbb;
  opacity:0.5
}
.site-footer hr.small
{
  margin:20px 0
}
.site-footer h6
{
  color:#fff;
  font-size:16px;
  text-transform:uppercase;
  margin-top:5px;
  letter-spacing:2px
}
.site-footer a
{
  color:#737373;
}
.site-footer a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links {
    padding-left: 0;
    list-style: none;
    margin-top: 22px;
}
.footer-links li {
    display: block;
    line-height: 30px;
    color: #fff;
}
.footer-links a {
    color: #fff;
}
.footer-links a:active,.footer-links a:focus,.footer-links a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links.inline li
{
  display:inline-block
}
.site-footer .social-icons
{
  text-align:right
}
.site-footer .social-icons a
{
  width:40px;
  height:40px;
  line-height:40px;
  margin-left:6px;
  margin-right:0;
  border-radius:100%;
  background-color:#33353d
}
.copyright-text
{
  margin:0
}
@media (max-width:991px)
{
  .site-footer [class^=col-]
  {
    margin-bottom:30px
  }
}
@media (max-width:767px)
{
  .site-footer
  {
    padding-bottom:0
  }
  .site-footer .copyright-text,.site-footer .social-icons
  {
    text-align:center
  }
}
.social-icons
{
  padding-left:0;
  margin-bottom:0;
  list-style:none
}
.social-icons li
{
  display:inline-block;
  margin-bottom:4px
}
.social-icons li.title
{
  margin-right:15px;
  text-transform:uppercase;
  color:#96a2b2;
  font-weight:700;
  font-size:13px
}
.social-icons a{
  background-color:#eceeef;
  color:#818a91;
  font-size:16px;
  display:inline-block;
  line-height:44px;
  width:44px;
  height:44px;
  text-align:center;
  margin-right:8px;
  border-radius:100%;
  -webkit-transition:all .2s linear;
  -o-transition:all .2s linear;
  transition:all .2s linear
}
.social-icons a:active,.social-icons a:focus,.social-icons a:hover
{
  color:#fff;
  background-color:#29aafe
}
.social-icons.size-sm a
{
  line-height:34px;
  height:34px;
  width:34px;
  font-size:14px
}
.social-icons a.facebook:hover
{
  background-color:#3b5998
}
.social-icons a.twitter:hover
{
  background-color:#00aced
}
.social-icons a.linkedin:hover
{
  background-color:#007bb6
}
.social-icons a.dribbble:hover
{
  background-color:#ea4c89
}
@media (max-width:767px)
{
  .social-icons li.title
  {
    display:block;
    margin-right:0;
    font-weight:600
  }
}
</style>