<div class="header">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<div class="top-header navbar navbar-default">
   <nav style="display: none;" class="navbar navbar-inverse">
      <div class="container-fluid">
         <div class="navbar-header">
            @if(Session::has('current_user'))
              <a href="{{url('/account')}}">
                <span 
                style="position: absolute;right: 85px;margin-top: 13px;font-size: 18px;color: #fff;" class="glyphicon glyphicon-user">
                  
                </span>
              </a>
              <a href="{{url('/checkout')}}"><span style="position: absolute;right: 55px;margin-top: 13px;font-size: 18px;color: #fff;" class="glyphicon glyphicon-shopping-cart"></span></a>
            @endif
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SAIF AGRO GROUP</a>
         </div>
         <div class="collapse navbar-collapse" id="myNavbar">
            <input style="margin-top: 11px; margin-bottom: 6px;" type="text" name="search" placeholder="Search Products.."><a href="#"><i class="fa fa-search" style="font-size:21px; margin-left: -36px;color: #157743;"></i></a>
            <ul class="nav navbar-nav">
               <li> <a href="{{url('/')}}">Home</a> </li>
               <li>  <a href="{{url('/category')}}"> Categories </a></li>
               <li>  <a href="{{url('/products')}}">All Products </a></li>
               <li><a href="{{url('/contact')}}">Contact Us</a></li>
            </ul>
          
            <ul class="nav navbar-nav navbar-right">
               <li> <a href="{{url('/register')}}"><span class="glyphicon glyphicon-user"></span> Signup</a></li>
               <li><a href="{{url('/sign-in')}}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.theandroidclassroom.saifagro">
      	    	<img class="mobile_play" src="/resources/views/storefront/google-play.png">
      	    </a>
         </div>
      </div>
   </nav>
   <div class="container mobile_hide">
      <div class="row">
         <div class="col-md-5">
            <div class="nav navbar-nav wow fadeInLeft animated" data-wow-delay=".5s">
               <p>Welcome to Saif Agro Group
                 @if(Session::has('current_user'))
                   <a href="{{url('/account')}}">My Account </a>
                 @else
                   <a href="{{url('/register')}}">Register </a> Or
                   <a href="{{url('/sign-in')}}">Sign In </a>
                 @endif
               </p>
            </div>
         </div>
         <div style="margin-top: -35px; margin-bottom: -35px;" class="col-md-5">
            <div class="nav navbar-nav header-two-left">
               <ul>
                  <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+91 080 41227486</li>
                  <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
                     <a href="mailto:saifagrogroup@gmail.com">enquiry@saifagrogroup.com</a>
                  </li>
               </ul>
            </div>
         </div>
         <div style="margin-top: -35px; margin-bottom: -35px;" class="col-md-2">
            <div class="header-right my-account">
               <a href="{{url('/contact')}}">
               <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> CONTACT US</a>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
   </div>
</div>
<div class="header-two navbar navbar-default">
   <div class="container">
      <div style="margin-top: 37px;" class="nav navbar-nav header-two-left mobile_hide">
         <input type="text" name="search" id="searchWord" placeholder="Search Products.."><a class="search-products" style="cursor:pointer"><i class="fa fa-search" style="font-size:21px; margin-left: -36px;color: #157743;"></i></a>
      </div>
      <script>
      $(document).on('click','.search-products',function(){
        $searchWord = $("#searchWord").val();
        if($searchWord == ""){
          alert("Please Fill The Seacrh Keyword");
          return false;
        }
        window.location.href = '{{url("/search")}}?search='+$searchWord;

      })
      </script>
      <div class="nav navbar-nav logo wow zoomIn animated" data-wow-delay=".7s">
         <h1><a href="{{url('/')}}"><span style="color: #11ec11;"><img style="width: 80%; height: 72px;"src="/resources/views/storefront/main_logo.png"></span></a></h1>
      </div>
 
      
      <div class="nav navbar-nav navbar-right header-two-right">
      	    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.theandroidclassroom.saifagro">
      	    	<img class="play_store" src="/resources/views/storefront/google-play.png">
      	    </a>
      	 @if(Session::has('current_user'))   
         <div style="margin-top: 55px;"  class="header-right cart">
            <a href="{{url('/account')}}">
            <i style="margin-right: 15px;" class="glyphicon glyphicon-user"></i>
            </a>
            <a href="{{url('/checkout')}}">
            <i class="glyphicon glyphicon-shopping-cart"></i>
            </a>
         </div>
         @endif
         <div class="clearfix"> </div>
      </div>
      
      <div class="clearfix"> </div>
   </div>
</div>
<div class="menus mobile_hide">
   <div class="container">
      <div class="row">
         <div class="col-md-5 nav navbar-nav header-two-left menus_alignment">
            <ul class="mobile_hide">
               <li> <a href="{{url('/')}}">Home</a> </li>
               <li>  <a href="{{url('/category')}}"> Categories </a></li>
               <li>  <a href="{{url('/products')}}">All Products </a></li>
            </ul>
         </div>
         <div class="col-md-7" style="text-align: right;">

            @foreach(App\Companies::get() as $ck=>$company)
                @php
                  $drpClass="";
                  switch ($ck) {
                    case 1:
                        $drpClass="two";
                    break;
                    case 2:
                      $drpClass="three";
                    break;
                    default:
                      $drpClass="";
                  }
                @endphp
              <div class="dropdown">
                 <button class="dropbtn">{{$company->company_name}}</button>
                 <div class="dropdown-content {{$drpClass}}">
                     @foreach(App\Categories::where(['company_id'=>$company->id])->get() as $ct)
                        <a href="{{url('/subcategories')}}?cat_id={{$ct->id}}">{{$ct->title}}</a>
                      @endforeach
                 </div>
              </div>
            @endforeach
         </div>
      </div>
   </div>
</div>
<style>
	.mobile_play{
     width: 116px;
     margin-right: 16px;
	}
	
	.play_store{
    width: 116px;
    margin-right: 16px;
    margin-top: 3px;
	}
	.new-top img {
    height: 150px !important;
    width: 100%;
    margin-left: 0px !important;
    margin-right: 0px !important;
}
   input[type=text] {
       width: 100%;
       box-sizing: border-box;
       border: 2px solid #157743;
       border-radius: 4px;
       font-size: 16px;
       background-color: white;
       /* background-image: url('searchicon.png'); */
       background-position: 10px 10px;
       background-repeat: no-repeat;
       padding: 12px 20px 12px 16px;
       -webkit-transition: width 0.4s ease-in-out;
       transition: width 0.4s ease-in-out;
   }
   input[type=text]:focus {
   width: 100%;
   }
   .top-header p a, .top-header h2 a {
   color: #157743;
   font-weight: 700;
   }
   .header-right span.glyphicon {
   color: #157743;
   }
   .dropbtn {
   background-color: #7dca40a1;
   color: white;
   padding: 21px;
   font-size: 16px;
   border: none;
   }
   .dropdown {
   position: relative;
   display: inline-block;
   }
   .dropdown-content a:hover {
   background: #157743;
   color: #fff;
   }
   .dropdown-content {
   display: none;
   position: absolute;
   background-color: #f1f1f1;
   min-width: 160px;
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
   z-index: 1;
   text-align: left;
   }
   .two {
   min-width: 267px;
   }
   .three {
   min-width: 140px;
   }
   .dropdown-content a {
   color: black;
   padding: 12px 16px;
   text-decoration: none;
   display: block;
   }
   .dropdown:hover .dropdown-content {display: block;}
   .menus_alignment ul li a {
   color: #fff;
   font-size: 17px;
   }
   .menus_alignment{
   margin-top: 1.6em;
   margin-bottom: 1.3em;
   }
   .menus {
   background: #157743;
   }
   .form-para {
   font-weight: bold;
   color: #000 !important;
   }
   .login-page input[type="submit"] {
   background: #11ec11 !important;
   border-radius: 45px;
   }
   h3.title {
   color: #11ec11 !important;
   }
   .breadcrumb1 li a, .breadcrumb1 li span {
   color: #11ec11 !important;
   }
   body{
   font-family: 'Roboto', sans-serif;
   }
   .flex-control-nav {
   position: absolute;
   bottom: 2%;
   text-align: center;
   left: 47%;
   }
   .header-right.cart {
   margin-right: 0 !important;
   padding-right: 0 !important;
   border-right: 1px solid transparent !important;
   margin-top: 2.3em !important;
   font-size: 21px !important;
   color: #80c83b !important;
   }
   h3.title {
   color: rgb(11 114 59 / 95%) !important;
   }
   .gallery-text p {
   color: #0b723b !important;
   }
   .login-page input[type="submit"] {
   background: rgb(11 114 59 / 95%) !important;
   }
   p.error {
   color: red;
   margin-bottom: 8px;
   font-weight: 600;
   letter-spacing: 1px;
   font-size: 15px;
   }
   .home-banner {
 
    background-size: 100% 100% !important;
}
   @media only screen and (max-width: 993px){
   	.play_store{
		   display: none !important;
	}
   .navbar-inverse{
   display: block !important;
   }
   .mobile_hide{
   display: none !important;
   }
   button.navbar-toggle {
   margin-right: 0;
   }
   .navbar-default {
   border-color: #fff;
   }
   .nav.navbar-nav.header-two-left {
   margin-top: -15px !important;
   }
   .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
   border-color: #89cd3b !important;
   background: #fff !important;
   }
   .navbar-inverse .navbar-brand {
   color: #ffffff !important;
   font-family: sans-serif;
   }
   .navbar-header {
   background: #167444!important;
   color: aliceblue;
   }
   .navbar-inverse {
   background-color: #fff !important;
   border-color: #89cd3b !important;
   }
   .navbar-default .navbar-nav > li > a {
   color: #000 !important;
   font-size: 17px !important;
   border-bottom: 1px solid #ccc;
   }
   .top-header .nav.navbar-nav {
   text-align: left !important;
   }
   .header-right.cart {
   display: none;
   }
   .new-top img {
   margin-right: 37px !important;
   margin-left: 0px !important;
   }
   h3.title {
   font-size: 31px !important;
   }
   .new-bottom h5 a.name {
    font-size: 13px !important;
}
.account {
    display: block !important;
}
.user-info {
    width: 90% !important;
  }
  .my-orders {
    width: 90% !important;
}
   }
</style>
