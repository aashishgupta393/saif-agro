<!DOCTYPE html>
<html>
   <head>
      <title>Saif Agro Group</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="" />
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
         function hideURLbar(){ window.scrollTo(0,1); }
      </script>
      <link href="{{ asset('public/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" >
      <link href="{{ asset('public/assets/css/style.css') }}" type="text/css" rel="stylesheet" media="all" >
      <?php
      if(!isset($page)){ ?>
        <link href="{{ asset('public/assets/css/flexslider.css') }}" type="text/css" rel="stylesheet" media="screen" >
      <?php } ?>
      <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}"></script>
      <script src="{{ asset('public/assets/js/modernizr.custom.js') }}"></script>
      <script src="{{ asset('public/assets/js/simpleCart.min.js') }}"></script>
      <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
      <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
      <link href='//fonts.googleapis.com/css?family=Pompiere' rel='stylesheet' type='text/css'>
      <link href='//fonts.googleapis.com/css?family=Fascinate' rel='stylesheet' type='text/css'>
      <link href="{{ asset('public/assets/css/animate.min.css') }}" type="text/css" rel="stylesheet" media="screen" >
      <script src="{{ asset('public/assets/js/wow.min.js') }}"></script>
      <script>
         new WOW().init();
      </script>
      <script src="{{ asset('public/assets/js/move-top.js') }}"></script>
      <script src="{{ asset('public/assets/js/easing.js') }}"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
         	$(".scroll").click(function(event){
         		event.preventDefault();
         		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
         	});
         });
      </script>
   </head>
   <body>

        @include('storefront.header')

        @yield('content')

         @include('storefront.footer')

      <script src="{{ asset('public/assets/js/main.js') }}"></script>
      <script type="text/javascript">
         $(document).ready(function() {
         	var defaults = {
         		containerID: 'toTop',
         		containerHoverID: 'toTopHover',
         		scrollSpeed: 1200,
         		easingType: 'linear'
         	};
         	$().UItoTop({ easingType: 'easeOutQuart' });
         });
      </script>
      <script src="{{ asset('public/assets/js/bootstrap.js') }}"></script>
   </body>
</html>
