<h2>Default Shipping Address </h2>
<p>Address : {{ Session::get('current_user')->company_address}},
  City : {{ Session::get('current_user')->city}},
  State : {{ Session::get('current_user')->state}},
  Zip Code : {{ Session::get('current_user')->zip_code}}
</p>
