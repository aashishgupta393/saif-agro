@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">
          @php
            $catData = App\Categories::where(['id'=>$cat_id])->first();
            echo "Subcategories Of Category  : ".$catData->title;
          @endphp
        </li>
			</ol>
		</div>
	</div>
	<div class="products">
		<div class="container">
			@php
				$sequence = 3;
			@endphp
			<div class="col-md-12 product-model-sec">
				  @foreach(App\SubCategories::with('subcategories')->where(['cat_id'=>$cat_id])->get() as $ckt=>$cat)
						@php
						$img = $ckt+1;
						$customClass="";
						if(($img+1) % $sequence == 0 && $img!=0) {
							$customClass = "product-grids-mdl";
						}
						$catImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
						if (@getimagesize($appUrl.'/public/uploads/'.$cat->image)) {
							$catImage = $appUrl.'/public/uploads/'.$cat->image;
						}

						$redirection = $appUrl."/products?sub_cat_id=".$cat->id;
						if(count($cat->subcategories) > 0){
							$redirection = $appUrl."/subcategories2?sub_cat_id=".$cat->id;
						}
						@endphp
						<div data-no="{{$img}}" class="product-grids <?=$customClass?> simpleCart_shelfItem wow fadeInUp animated" data-wow-delay=".5s">
							<div class="new-top">
								<a href="{{$redirection}}" >
									<img src="{{$catImage}}" class="img-responsive" alt=""/>
								</a>
								<div class="new-text">
									<ul>
										<li><a href="{{url('/products')}}?sub_cat_id={{$cat->id}}">Quick View </a></li>
									</ul>
								</div>
							</div>
							<div class="new-bottom">
								<h5>
									<a class="name" href="{{$redirection}}">
										{{$cat->title}}
									</a>
								</h5>
							</div>
						</div>
					 @endforeach
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

<style>
	.new-bottom {
    text-align: center;
    margin-top: 19px;
}
.new-top img {
    text-align: center;
    vertical-align: middle;
    width: 230px;
    height: 160px;
    margin-right: 50px;
    margin-left: 50px;
}
.product-grids .new-top {
    padding: 0;
}
.new-text {
    background: #7ac63e;
        display: none;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.new-bottom h5 a.name {
    font-size: 18px;
    letter-spacing: 1px;
}
</style>

  @endsection
