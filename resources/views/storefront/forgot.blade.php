@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow fadeInUp" data-wow-delay=".5s">
				<li>
          <a href="#">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
             Home
          </a>
        </li>
				<li class="active">
         Forgot Password
        </li>
			</ol>
		</div>
	</div>

	<div class="login-page">
		<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
			<h3 class="title">Forgot Your Password ??<span></span></h3>
		</div>
		<div class="widget-shadow">

      <div id="send-otp">
  			<div class="login-top" >
  				<h4>Enter Your Registed mobile No. </h4>
  			</div>
  			<div class="login-body wow fadeInUp animated" data-wow-delay=".7s">
  				<p class="error"></p>
  				<p class="success"></p>
  				<form id="send-form">
  					<input type="text" class="user" name="contact_no" id="contact_no" placeholder="Contact No." required="">
  					<input type="submit" name="Sign In" value="send">
  				</form>
  			</div>
			</div>


      <div id="update-form" style="display:none;">
        <div class="login-top" >
          <h4>Enter OTP Sent On Your Mobile </h4>
        </div>
			<div class="login-body" >
        <p class="error"></p>
        <p class="success"></p>
				<form id="validate-form">
          <p>Enter OTP </p>
					<input type="text" class="user" id="otp" name="otp" placeholder="Contact No." required="">
					<input type="submit" name="Sign In" value="validate">
				</form>
			</div>
		</div>
    <div id="confirm-password" style="display:none;">
      <div class="login-top" >
        <h4>Enter Passwords </h4>
      </div>
      <div class="login-body" >
        <p class="error"></p>
        <p class="success"></p>
        <form id="confirm">
          <p>Enter Password </p>
          <input type="password" class="user" id="password" name="pwd" placeholder="Contact No." required="">
          <p>Confirm  Password </p>
          <input type="password" class="user" id="confirm_password" name="mobile" placeholder="Contact No." required="">
            <span id='message'></span>
          <input type="submit" name="Sign In" value="Confirm">
        </form>
      </div>
    </div>
	</div>
	</div>
	<script>

	   $(document).ready(function(){
        $('#password, #confirm_password').on('keyup', function () {
          if ($('#password').val() == $('#confirm_password').val()) {
        $('#message').html('Matching').css('color', 'green');
        } else
          $('#message').html('Not Matching').css('color', 'red');
        });

	     $("#confirm").on('submit',function(evt){
          evt.preventDefault();
          $data={password:$("#password").val(),contact:window.contact_no}
          $.ajax({
          url: '{{URL("/api/update-password")}}',
          type: 'post',
          dataType: 'json',
          data:$data,
          success:function(res){
            if(res.code == 200){
              alert("Password Updated");
              window.location.href = '{{URL("/register")}}';
            }
          }
          })
       })


	     $("#validate-form").on('submit',function(evt){
	       evt.preventDefault();
         var otp = $("#otp").val();
         if(otp == window.otp){
           $("#update-form").hide();
           $("#confirm-password").show();
         }else{
           alert("OTP not Matched")
         }

       })


	     $("#send-form").on('submit',function(evt){
	       evt.preventDefault();
	        $.ajax({
	        url: '{{URL("/api/send-forgot-otp")}}',
	        type: 'post',
	        dataType: 'json',
	        data: $(this).serialize(),
	        success: function(res) {
	          if(res.code == 100){
	            $(".error:first").show().text(res.msg)
	            $('html, body').animate({
	              scrollTop: $(".error").offset().top
	            }, 2000);
	          }else{
              window.otp = res.otp;
              window.contact_no = $("#contact_no").val();
	            $(".success:first").show().text(res.msg)
	            $('html, body').animate({
	              scrollTop: $(".success").offset().top
	            }, 2000);
              $("#send-otp").hide();
              $("#update-form").show();
	          }
	        }
	        })
	     })
	   })
	</script>
@endsection
