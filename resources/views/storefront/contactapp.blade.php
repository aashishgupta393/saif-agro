@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}">
          <span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
        </li>
				<li class="active">Contact Us</li>
			</ol>
		</div>
	</div>
	<div class="contact">
		<div class="container">
			<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
				<h3 class="title">How To <span> Find Us</span></h3>
			</div>
	
			<iframe  class="wow zoomIn animated animated" data-wow-delay=".5s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.117857536739!2d77.57754580012124!3d12.96430949081583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae15e84f71fa73%3A0xdb395a847eb53f5a!2sSaif%20Agro%20Tech%20-%20Chain%20Saw%20-%20Brush%20Cutter%20-%20Sprayer%20%26%20All%20Its%20Spare%20Parts!5e0!3m2!1sen!2sin!4v1607002495662!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>
	</div>
	<div class="address"><!--address-->
		<div class="container">
			<div class="address-row">
				<div class="col-md-6 address-left wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<h4 class="wow fadeIndown animated" data-wow-delay=".5s">SUBMIT YOUR ENQUIRY</h4>
						<form>
							<input class="wow fadeIndown animated" data-wow-delay=".6s" type="text" placeholder="Name" required="">
							<input class="wow fadeIndown animated" data-wow-delay=".7s" type="text" placeholder="Email Id" required="">
							<input class="wow fadeIndown animated" data-wow-delay=".8s" type="text" placeholder="Subject" required="">
							<textarea class="wow fadeIndown animated" data-wow-delay=".8s" placeholder="Message" required=""></textarea>
							<input class="wow fadeIndown animated" data-wow-delay=".9s" type="submit" value="SEND">
						</form>
					</div>
				</div>
				<div class="col-md-6 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>ADDRESS</h4>
						<p>1st Floor, #33, SJP Road, Kalasipalayam, New Ext, Kumbarpet, Dodspet, Nagarathpete, Bengaluru, Karnataka 560002</p>
					</div>
					<div style="padding: 2em !important;" class="address-info address-mdl wow fadeInRight animated" data-wow-delay=".7s">
						<h4>PHONE </h4>
						<div class="row">
							<div class="col-md-7 col-xs-7 col-sm-7">
								<p>SAIF AGRO TECH</p>
						<p>SMS INDUSTRIAL COMPONENT</p>
						<p>TMM TRADES</p>
							</div>
							<div class="col-md-5 col-xs-5 col-sm-5">
						<p>+91 96637 54786</p>
						<p>+91 94260 76652</p>
						<p>+91 80560 17572</p>
							</div>
						</div>	
						
					</div>
					<div class="address-info wow fadeInRight animated" data-wow-delay=".6s">
						<h4>EMAIL US</h4>
						<p><a href="mailto:example@mail.com"> enquiry@saifagrogroup.com</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

<style>
.address {
    background: url(https://fams.asn.au/wp-content/uploads/2019/10/istockphoto-925857310-612x612.jpg);
    background-size: cover;
}
.address-grid, .address-info {
    background-color: rgb(74 161 191);
    padding: 4em;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.address h4 {
    font-size: 26px !important;
    font-family: fantasy;
    letter-spacing: 2px;
    font-weight: 200;
}
.address input[type="submit"] {
    background: rgb(25 48 62) !important;
    border: rgb(25 48 62) !important;
}
@media only screen and (max-width: 993px){
 .address-info {
    padding: 6px !important;
}
.address-info {
    padding: 2em !important;
}
}
.address-grid, .address-info {
    padding: 3em !important;
}
.navbar.navbar-default {
    display: none;
}
.breadcrumbs {
    display: none;
}
footer.site-footer {
    display: none;
}
</style>

  @endsection
