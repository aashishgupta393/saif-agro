@extends('storefront.website')
@section('content')
<script src="{{ asset('public/assets/js/jquery.flexslider.js') }}"></script>
<link href="{{ asset('public/assets/css/flexslider1.css') }}" type="text/css" rel="stylesheet" media="screen" >
<script src="{{ asset('public/assets/js/imagezoom.js') }}"></script>
<script src="{{ asset('public/assets/js/simpleCart.min.js') }}"></script>
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">{{$details->product_title}}</li>
			</ol>
		</div>
	</div>

	<style>
	.select-box {
	    font-size: 1em;
	    padding: 0.9em 1em;
	    width: 100%;
	    color: #999;
	    outline: none;
	    border: 1px solid #E2DCDC;
	    background: #FFFFFF;
	    margin: 0em 0em 1em 0em;
	}
	</style>
	<div class="single">
		<div class="container">
			<?php
			$variants =[];
			if(!empty($details->variants)){
				$variants = json_decode($details->variants);
			}
			?>
			<div class="single-info">
				<div class="col-md-6 single-top wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="flexslider">
						<ul class="slides">
						<?php if(!empty($details->images)){
								$images = json_decode($details->images);
								   foreach ($images as  $image) {
										  		?>
											 <li data-thumb="{{$appUrl.'/'.$image}}">
												 <div class="thumb-image">
													 <img src="{{$appUrl.'/'.$image}}" data-imagezoom="true" class="img-responsive" alt="">
													</div>
											 </li>
								 <?php }
								 	if(!empty($variants)){
										  foreach ($variants as  $variant) {
												if(!empty($variant->image)){
												?>
													<li data-thumb="{{$appUrl.'/'.$variant->image}}">
														<div class="thumb-image">
															<img src="{{$appUrl.'/'.$variant->image}}" data-imagezoom="true" class="img-responsive" alt="">
														 </div>
													</li>
												<?php
											}
										}
									}
						 } ?>

						</ul>
					</div>
				</div>
				<div class="col-md-6 single-top-left simpleCart_shelfItem wow fadeInRight animated" data-wow-delay=".5s">
					<h3>{{$details->product_title}} <span class="variant-title"></span></h3>
					<div class="single-rating">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5" checked>
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3">
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
						<p>5.00 out of 5</p>
						<a href="#">Add Your Review</a>
					</div>
					@if(Session::has('current_user'))
						<h6 class="item_price">&#x20b9; {{$details->product_amount}}</h6>
					@endif

					<p>{{$details->product_description}}</p>
					<p>Brand : {{$details->brand_name}}</p>

					<!-- @php
					echo "<pre>";
					print_r($details);
					echo "</pre>";
					@endphp -->

					<div class="clearfix"> </div>

						<p>Select Variant </p>
						<select class="select-box" id="variant" >
							@if(count($variants) > 0 )
							 @foreach($variants as $vk=>$variant)
							   <option value="{{$vk+1}}" data-title="{{$variant->titile}}" data-amount="{{$variant->price}}">
									 {{$details->product_title}} -- {{$variant->titile}}
									 	@if(Session::has('current_user'))
									  -- &#x20b9;{{$variant->price}}
										@endif
									</option>
							 @endforeach
							 @else
							 		<option value="0">{{$details->product_title}}
											@if(Session::has('current_user'))
										 -- &#x20b9;{{$details->product_amount}}
										 @endif
									 </option>
							 @endif
						 </select>

					<div class="quantity">
						<p class="qty"> Qty :  </p>
						<button type="button" class="minus" >-</button>
								<input min="1" step="1" max="100" type="number" value="1" class="item_quantity">
						<button type="button" class="plus" >+</button>
					</div>
					<div class="btn_form">
							@if(Session::has('current_user'))
								<a style="cursor:pointer;"  class="add-cart item_add add-item-to-cart">ADD TO CART</a>
							@else
									<a href="{{url('/sign-in')}}"  class="add-cart item_add">ADD TO CART</a>
							@endif
					</div>
				</div>
			   <div class="clearfix"> </div>
			</div>
			<!--collapse-tabs-->
			<div class="collpse tabs">
				<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default wow fadeInUp animated" data-wow-delay=".5s">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Description
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								{{$details->product_description}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<style>
	.collpse.tabs {
    padding-bottom: 35px;
}
.panel-default > .panel-heading {
    background-color: #157743;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
    color: #000;
    font-size: 14px;
}
.panel-default {
    border-color: #157743;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.single-top-left h3 {
    color: #157743;
}
.btn_form a {
    border: 3px solid rgb(21 119 67);
    color: #fff;
    background: #157743;
}
.single-info p {
    font-size: 1.2em;
    color: #000;
}
.btn_form a:hover {
    color: #000;
    border-color: #157743;
    background: transparent;
}
</style>

	<script>
	$(window).load(function() {
	  $('.flexslider').flexslider({
	    animation: "slide",
	    controlNav: "thumbnails"
	  });
	});

	$(document).on('click', '.plus,.minus', function() {
		var qty = $('.item_quantity');
		var val = parseFloat(qty.val());
		var max = parseFloat(qty.attr('max'));
		var min = parseFloat(qty.attr('min'));
		var step = parseFloat(qty.attr('step'));
		if ($(this).is('.plus')) {
				if (max && (max <= val)) {
						qty.val(max);
				} else {
						qty.val(val + step);
				}
		}else{
				if (min && (min >= val)) {
						qty.val(min);
				} else if (val > 1) {
						qty.val(val - step);
				}
		}
		});


	@if(Session::has('current_user'))
	$(document).on('change','#variant',function() {
			var selected = $(this).find('option:selected');
			$title = selected.data("title");
			$amount = selected.data("amount");
			$('.item_price').html('&#x20b9 '+$amount);
			$('.variant-title').text($title)
	})
	@endif

	$(document).on('click','.add-item-to-cart',function() {
		$pid = '{{$details->id}}';
		$qty = $('.item_quantity').val();
		$addURL = "{{url('/add')}}";
		$variant = $("#variant").val();
		$.ajax({
			url:$addURL,
			data:{pid:$pid,qty:$qty,variant:$variant},
			dataType:'json',
			type:'get',
			success:function(response){
				alert("Item Added To cart");
				window.location.href = "{{url('/checkout')}}";
			}
		})

	})
	</script>
  @endsection
