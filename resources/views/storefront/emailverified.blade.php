@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow fadeInUp" data-wow-delay=".5s">
				<li>
          <a href="{{url('/')}}">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
              Home
          </a>
        </li>
				<li class="active">
         Verified
        </li>
			</ol>
		</div>
	</div>
    <h2> your Account is Verified</h2>
  <script>
  setTimeout(function(){
    window.location.href = '{{URL("/account")}}'
  }, 10000);
  </script>

@endsection
