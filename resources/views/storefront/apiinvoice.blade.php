<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Invoice</title>
      <link href="{{ asset('public/assets/invoice/invoice.css') }}"  rel="stylesheet" >
   </head>
   <?php

   ?>
   <body>
      <section >
         <div class="container">
            <p style="text-align: right;margin-right: 9px;padding-top: 11px;font-weight: 900;font-size: 23px;">INVOICE</p>
            <div class="row">
               <div class="col1 row mar-bot">
                  <div class="logo-name">

                     <p>{{$company->company_name}}</p>
                     <div class="address">
                        <p>{{$company->address}}, {{$company->address1}}</p>
                        <p>{{$company->city}}, {{$company->state}} {{$company->zip_code}}</p>
                        <p>{{$company->company_contact_no}}, {{$company->fax}}</p>
                        <p>{{$company->company_email}}</p>
                     </div>
                  </div>
                  <div class="logo">
                     <img src="https://saifagrogroup.com/resources/views/storefront/main_logo.png" alt="logo">
                  </div>
                  <div class="invoice">
                     <div class="invoice-desc">
                        <p>INVOICE NO. {{$order->order_no}}</p>
                        <p>DATE {{date('d/M/Y',strtotime($order->created_at))}}</p>
                        <p>CUSTOMER ID {{$customer->id}}</p>
                     </div>
                  </div>
               </div>
               <div style="margin-top: -27px;margin: 2px;" id="border">
                  <div class="col1 row">
                     <div class="to">BILL TO</div>
                     <div class="shipto">SHIP TO</div>
                  </div>
                  <div class="col1 row border-bottom">
                     <div style="line-height: 22px;" class="col">
                        <p>{{$customer->username}}</p>
                        <P>{{$customer->company_address}},{{$customer->company_address2}}</P>
                        <p>{{$customer->city}}, {{$customer->state}} {{$customer->zip_code}}</p>
                        <P>GSTIN/UIN:{{$customer->gst_no}}</P>
                     </div>
                     <DIV style="line-height: 22px;" class="col">
                       <p>{{$customer->username}}</p>
                       <P>{{$customer->company_address}},{{$customer->company_address2}}</P>
                       <p>{{$customer->city}}, {{$customer->state}} {{$customer->zip_code}}</p>
                       <P>GSTIN/UIN:{{$customer->gst_no}}</P>
                     </DIV>
                  </div>
               </div>
               <div class="col1">
                  <table style="width: 100%;">
                     <thead>
                        <th>S NO</th>
                        <th>PART NO</th>
                        <th style="width: 32%;">DESCRIPTION</th>
                        <th>HSN CODE</th>
                        <th>QTY</th>
                        <th>AMOUNT</th>
                        <th>TAX RATE</th>
                        <th>TOTAL</th>
                     </thead>
                     <tbody>
                       @php
                       $orderGST = 0;
                       $withoutGstTotal = 0;
                       @endphp
                         @foreach($items as $ik=>$item)
                           @php
                           $gstAmount  = $item->price - ($item->price*(100/(100+18)));
                           $orderGST+=$gstAmount*$item->qty;
                           $netPrice = $item->price - $gstAmount;
                           $withoutGstTotal+=$netPrice*$item->qty;
                           $variantTitle = "";
                           $partNo = $item->product->part_no;
                           if($item->variant_no > 0){
                            $variants = json_decode($item->product->variants);
                            $variant = $variants[$item->variant_no-1];
                            $variantTitle = $variant->titile;
                            $partNo = $variant->part_no;
                           }
                           @endphp
                            <tr>
                               <td>001</td>
                               <td>{{$partNo}}</td>
                               <td> {{$item->product->product_title}} {{$variantTitle}}</td>
                               <td>{{$item->product->hsn_code}}</td>
                               <td>{{$item->qty}}</td>
                               <td>{{round($netPrice)}}</td>
                               <td>{{$item->product->tax_rate}}</td>
                               <td>{{round($netPrice*$item->qty)}}</td>
                            </tr>
                            @endforeach
                        <!-- <tr>
                           <td>002</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>003</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>001</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>004</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>005</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>006</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr>
                        <tr>
                           <td>007</td>
                           <td>SA2005</td>
                           <td>Chain Saw</td>
                           <td>8467</td>
                           <td>1.00</td>
                           <td>200.00</td>
                           <td>18%</td>
                           <td>200</td>
                        </tr> -->
                     </tbody>
                  </table>
               </div>
            </div>
            <div style="margin-top: 9px;margin-bottom: -25px;display:flex;margin-top: -1px;height: 60px;">
               <div style="width: 48%; float: left;border: 1px solid;padding-top: 13px;">
                  All Dispute Are Subject To {{$company->city}} Jurisdiction Only
               </div>
               <div style="width: 43%; float: left;text-align: center; border: 1px solid;padding-top: 13px;">Total Taxable Value</div>
               <div style="width: 9%; float: left;text-align: right;border: 1px solid;padding-top: 13px;padding-right: 3px;">{{round($withoutGstTotal)}}</div>
            </div>
            <div style="margin-top: 9px;margin-bottom: -25px;display: flex;margin-top: -1px;height: 100px;">
               <div style="width: 48%; float: left;border: 1px solid;padding-top: 35px;border-top: 1px;">
                  <B>Total Invoice Value in Word:</B><br>
                  Rupees {{getIndianCurrency($order->subtotal)}} Only
               </div>
               <div style="width: 43%; float: left;text-align: center; border: 1px solid;padding-top: 35px;border-top: 1px;">
                 @if(strtolower($company->state) == strtolower($customer->state))
                 SGST@ 9%<br>
                 IGST@ 9%
                 @else
                     IGST@ 18%
                 @endif
               </div>
               <div style="width: 9%; float: left;text-align: right;border: 1px solid;padding-right: 3px;border-top: 1px;padding-top: 35px;">
                 @if(strtolower($company->state) == strtolower($customer->state))
                   {{round($orderGST)/2}} <br/>
                   {{round($orderGST)/2}} <br/>
                 @else
                  {{round($orderGST)}} <br/>
                 @endif
               </div>



            </div>
            <div style="margin-top: 9px;margin-bottom: -25px;display: flex;margin-top: -1px;height: 55px;">
               <div style="width: 48%; float: left;border: 1px solid;padding-top: 35px;border-top: 1px;">
               </div>
               <div style="width: 43%; float: left;text-align: center; border: 1px solid;padding-top: 29px;border-top: 1px;">Packaging & Forwading</div>
               <div style="width: 9%; float: left;text-align: right;border: 1px solid;padding-right: 3px;border-top: 1px;padding-top: 29px;">
                 {{$order->packing_charges}}
               </div>
            </div>
            <div style="margin-top: 9px;margin-bottom: -25px;display: flex;margin-top: -1px;height: 89px;">
               <div style="width: 48%; float: left;border: 1px solid;padding-top: 35px;border-top: 1px;">
               </div>
               <div style="width: 43%; float: left;text-align: center; border: 1px solid;padding-top: 35px;border-top: 1px;">Total Invoice Value</div>
               <div style="width: 9%; float: left;text-align: right;border: 1px solid;padding-right: 3px;border-top: 1px;padding-top: 35px;">
                 {{$order->order_amount}}
               </div>
            </div>
            <br><br>
            <div style="margin-top: 9px;margin-bottom: -25px;">
               <div style="width: 50%;display: flex; float: left;">
                  Tax Amount Subject To Reverse Charge
               </div>
               <div>IGST: {{round($orderGST)}}</div>
            </div>
            <div style="height: 190px; margin-top: 36px !important;margin: 2px;">
               <div class="border1" style="width: 50%;display: flex;float: left;border: 1px solid;height: 120px;padding: 6px;">
                  <b style="width: 79%;display: flex;float: left;">E &.O.E</b><br><br><br>
                  <div style="margin-left: -130px;margin-top: 30px;"><b>Decalartion</b><br>We declare that this invoice shows the actual price of the goods described snd that all particulars are true and correct</div>
               </div>
               <div class="border2" style="text-align: right;border: 1px solid;height: 120px;padding: 6px;">
                  For {{$company->company_name}}<br><br><br><br><br>
                  <B>Authorized Signatory</B>
               </div>
            </div>
         </div>
      </section>
      <script src="https://www.google.com/cloudprint/client/cpgadget.js">
      </script>
      <script>
          window.onload=function(){
          var ua = navigator.userAgent.toLowerCase();
          var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
          if (isAndroid) {
           var gadget = new cloudprint.Gadget();
           gadget.setPrintDocument("url", "Invoice", window.location.href, "utf-8");
           gadget.openPrintDialog();
         } else {
           window.focus();
           window.print()
           window.close();
         }


          };
      </script>
   </body>
</html>
