@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
   <div class="container">
      <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
         <li>
            <a href="{{url('/')}}">

            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
            Home
            </a>
         </li>
         <li class="active">Register</li>
      </ol>
   </div>
</div>
<div class="login-page">
   <div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
      <h3 class="title">Register<span> Form</span></h3>
   </div>
   <div class="widget-shadow">
      <div class="login-top wow fadeInUp animated" data-wow-delay=".7s">
         <h4>Already have an Account ?<a href="{{url('/sign-in')}}">  Sign In »</a> </h4>
      </div>
      <style>
         .select-box{
           font-size: 1em;
           padding: 0.9em 1em;
           width: 100%;
           color: #999;
           outline: none;
           border: 1px solid #E2DCDC;
           background: #FFFFFF;
           margin: 0em 0em 1em 0em;
         }
         .form-para {
           font-weight: bold;
           color: red;
         }
         h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
h3.title {
    font-size: 37px;
    letter-spacing: 1px;
}
      </style>
      <div class="login-body">
         <p class="error form-para" style="display:none;"></p>
         <p class="success form-para" style="display:none;color:green"></p>
         <form class="wow fadeInUp animated" id="register-form" data-wow-delay=".7s">
            <p class="form-para">Company Name / Name<span style="color: red">*</span></p>
            <input type="text" placeholder="Company Name/ Name" name="name" required="">
            <p class="form-para">Email<span style="color: red">*</span></p>
            <input type="text" class="email"  placeholder="Email Address" name="email" required="">
            <p class="form-para">Mobile Number <span style="color: red">*</span></p>
            <input type="text"  placeholder="xxxxxxxxxx" name="contact_no" id="contact_no" onkeypress="return isNumberKey(event)"
             maxlength="10" required="">
             <p class="form-para">WhatsApp number same as Mobile Number?<span style="color: red">*</span></p>
             <label style="cursor:pointer"><input type="checkbox" class="same_no" value="yes"> Yes</label>
            <p class="form-para">WhatsApp No.<span style="color: red">*</span></p>
            <input type="text"  placeholder="xxxxxxxxxx" name="whats_app_no" id="whats_app_no" onkeypress="return isNumberKey(event)"
             maxlength="10" required="">
            <p class="form-para">Password<span style="color: red">*</span></p>
            <input type="password" required="" class="lock" name="password" placeholder="Password">
            <p class="form-para">Company Address</p>
            <input type="text" name="company_address" =""  placeholder="Company Address">
            <p class="form-para">State</p>
              <select class="select-box" name="state" id="state" ="">
                 <option value=""> Please Select</option>
                 @foreach($cities as $state)
                  <option value="{{$state->name}}">{{$state->name}}</option>
                 @endforeach
              </select>
            <p class="form-para">City</p>
            <input type="text" name="city"  id="city"  placeholder="City">
            <p class="form-para">Pincode / Zipcode<span style="color: red">*</span></p>
            <input type="text" name="zip_code" required=""  placeholder="Pincode/ Zipcode">
            <p class="form-para">Do You Have GST Number<span style="color: red">*</span></p>
            <select class="select-box" id="select-gst" name="has_gst" required="">
               <option value=""> Please Select</option>
               <option value="yes">Yes</option>
               <option value="no">No</option>
            </select>
            <div class="gst-field" style="display:none;">
              <p class="form-para">GST No.</p>
              <input type="text" maxlength="15" placeholder="GST No." name="gst_no" class="gst" >
            </div>
            <input type="submit" name="Register" value="Register">
         </form>
         <form class="wow fadeInUp animated" id="otp-form" style="display:none;" data-wow-delay=".7s">
            <p class="form-para">Enter OTP</p>
            <input type="text"  placeholder="xxxxxxxxxx" name="contact_no" id="otp" >
            <input type="submit" name="validate" value="ValidateOTP">
         </form>
      </div>
   </div>
</div>
<script id="states-json" type="application/json"><?=json_encode($cities)?></script>
<script>
    $states = JSON.parse($("#states-json").html());
   function isNumberKey(evt){
       var charCode = (evt.which) ? evt.which : evt.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;
       return true;
   }
   $(".same_no").on('change',function(){
     if($(this).prop('checked') == true){
       $("#whats_app_no").val($("#contact_no").val());
     }
   })
   // $("#state").on('change',function(){
   //   var array = $states[$(this).val()];
   //   $city='<option value=""> Please Select</option>';
   //   for (var i = 0; i < array.length; i++) {
   //     $city+='<option value="'+array[i].city+'">'+array[i].city+'</option>';
   //   }
   //   // $("#city").html($city);
   // })

   function RegistrationSuccess() {
       $.ajax({
       url: '{{URL("/register-user")}}',
       type: 'post',
       dataType: 'json',
       data: $("#register-form").serialize(),
       success: function(res) {
         if(res.code == 100){
           $(".error").show().text(res.msg)
           $('html, body').animate({
             scrollTop: $(".error").offset().top
           }, 2000);
         }else{
           $(".success").show().text(res.msg)
           $('html, body').animate({
             scrollTop: $(".success").offset().top
           }, 2000);
           $(this)[0].reset();
         }
       }
       })
   }

  $("#otp-form").on('submit',function(evt){
      evt.preventDefault();
       var otp = $("#otp").val();
       otp = otp.trim();
       $.ajax({
           type:"post",
           url:'{{URL("/validate-otp")}}',
           data:{otp:otp},
           dataType:'json',
           success:function(res){
             if(res.code == 100){
                 $(".error").show().text(res.msg)
                 $('html, body').animate({
                   scrollTop: $(".error").offset().top
                 }, 2000);
                 return false;
               }else{
                 $(".error").hide()
                 // $(".success").show().text(res.msg)
                 $('html, body').animate({
                   scrollTop: $(".success").offset().top
                 }, 2000);
                 $("#otp-form").hide();
                 RegistrationSuccess();
               }
           }
       })
   })

   // send-otp
   // validate-otp
   $(document).ready(function(){
     $("#register-form").on('submit',function(evt){
       evt.preventDefault();
          $.ajax({
          url: '{{URL("/send-otp")}}',
          type: 'post',
          dataType: 'json',
          data: $(this).serialize(),
          success: function(res) {
            if(res.code == 100){
              $(".error").show().html(res.msg)
              $('html, body').animate({
                scrollTop: $(".error").offset().top
              }, 2000);
            }else{
              $(".success").show().html(res.msg)
              $('html, body').animate({
                scrollTop: $(".success").offset().top
              }, 2000);
              $("#register-form").hide();
              $("#otp-form").show();
            }
          }
          })
     })
   })


     $("#select-gst").change(function () {
       var inputvalues = $(this).val();
       $('.gst-field').hide();
       $(".gst").attr('required',false)
       if(inputvalues == 'yes'){
         $(".gst").attr('required',true)
         $('.gst-field').show();
       }
     })
     $(".gst").change(function () {
     var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
       if (gstinformat.test(inputvalues)) {
         return true;
       } else {
         $(".gst").val('');
         $(".gst").focus();
       }
     });
</script>
@endsection
