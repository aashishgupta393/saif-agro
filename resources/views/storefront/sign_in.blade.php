@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow fadeInUp" data-wow-delay=".5s">
				<li>
          <a href="#">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
             Home
          </a>
        </li>
				<li class="active">
          Sign In
        </li>
			</ol>
		</div>
	</div>

	<div class="login-page">
		<div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
			<h3 class="title">SignIn<span> Form</span></h3>
		</div>
		<div class="widget-shadow">
			<div class="login-top wow fadeInUp animated" data-wow-delay=".7s">
				<h4>Welcome back to Saif Agro Group ! <br> Not a Member? <a href="{{url('/register')}}">  Register Now »</a> </h4>
			</div>
			<div class="login-body wow fadeInUp animated" data-wow-delay=".7s">
				<p class="error"></p>
				<p class="success"></p>
				<form id="login-form">
					<input type="text" class="user" name="email" placeholder="Enter your email" required="">
					<input type="password" name="password" class="lock" placeholder="Password">
					<input type="submit" name="Sign In" value="Sign In">
					<div class="forgot-grid">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Remember me</label>
						<div  class="forgot">
							<a href="{{url('/forgot-password')}}">Forgot Password?</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</form>
			</div>
		</div>
		<div style="display:none;" class="login-page-bottom">
			<h5> - OR -</h5>
			<div class="social-btn"><a href="#"><i>Sign In with Facebook</i></a></div>
			<div class="social-btn sb-two"><a href="#"><i>Sign In with Twitter</i></a></div>
		</div>
	</div>
	<style>
		h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
h3.title {
    font-size: 37px;
    letter-spacing: 1px;
}
</style>
	<script>

	   $(document).ready(function(){
	     $("#login-form").on('submit',function(evt){
	       evt.preventDefault();
	        $.ajax({
	        url: '{{URL("/validate-user")}}',
	        type: 'post',
	        dataType: 'json',
	        data: $(this).serialize(),
	        success: function(res) {
	          if(res.code == 100){
	            $(".error").show().text(res.msg)
	            $('html, body').animate({
	              scrollTop: $(".error").offset().top
	            }, 2000);
	          }else{
	            $(".success").show().text(res.msg)
	            $('html, body').animate({
	              scrollTop: $(".success").offset().top
	            }, 2000);
							window.location.href = '{{URL("/account")}}'
	          }
	        }
	        })
	     })
	   })



	</script>
@endsection
