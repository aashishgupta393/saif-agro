@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
  <div class="container">
    <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
      <li><a href="{{url('/')}}">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
      </li>
      <li class="active">Your Cart</li>
    </ol>
  </div>
</div>

<div class="cart-items">
  <div class="container">
    @if(isset($search))

    <div class="gallery">
       <div class="container">
          <div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
             <h3 class="title"> Search Results For  : {{$search}}</h3>
          </div>
          <div class="gallery-info">
            @if(count($products) > 0)
              @php
              $pdelay=0.5;
              $flip=[1,5];
      				$flip2=[2,6];
              if(count($products) > 5){
                $flip=range(1,count($products),4);
                $flip2=range(2,count($products),4);
              }
              @endphp
             @foreach($products as $pk=>$product)
             @php
               $productImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
               if (@getimagesize($appUrl.'/'.$product->product_main_image1)) {
                 $productImage = $appUrl.'/'.$product->product_main_image1;
               }
             @endphp
              <div class="col-md-3 gallery-grid <?php if(in_array($pk,$flip)){ echo "gallery-grid1";}?>
    						<?php if(in_array($pk,$flip2)){ echo "gallery-grid2";}?> wow flipInY animated" data-wow-delay=".{{$pdelay}}s">
                <a href="{{url('/product')}}?id={{$product->id}}">
                  <img src="{{$productImage}}" class="img-responsive" alt=""/>
                </a>
                <div class="gallery-text simpleCart_shelfItem">
                   <h5>
                  <a class="name"  href="{{url('/product')}}?id={{$product->id}}">
                     {{$product->product_title}}
                   </a>
                 </h5>
    						 		@if(Session::has('current_user'))
                   	<p><span class="item_price">&#x20b9;:{{$product->product_amount}}</span></p>
    							 @endif
                   	<h4 style="display:none;" class="sizes">Sizes: <a href="#"> s</a> - <a href="#">m</a> - <a href="#">l</a> - <a href="#">xl</a> </h4>
    							 <ul>
                      <li><a  href="{{url('/product')}}?id={{$product->id}}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></a></li>
                   </ul>
                </div>
             </div>
              @php
              $pdelay=$pdelay+0.2;
              @endphp
              @endforeach
             @endif
             <div class="clearfix"></div>
          </div>
       </div>
    </div>
    @endif

  </div>
</div>

@endsection
