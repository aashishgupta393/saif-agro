@extends('storefront.website')
@section('content')
<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="{{url('/')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Categories</li>
			</ol>
		</div>
	</div>
	<div class="products">
		<div class="container">
			@php
				$sequence = 3;
			@endphp
			<div class="col-md-12 product-model-sec">
				  @foreach(App\Categories::inRandomOrder()->get() as $ckt=>$cat)
						@php
						$img = $ckt+1;
						$customClass="";
						if(($img+1) % $sequence == 0 && $img!=0) {
							$customClass = "product-grids-mdl";
						}
						@endphp
						<div data-no="{{$img}}" class="product-grids <?=$customClass?> simpleCart_shelfItem wow fadeInUp animated" data-wow-delay=".5s">
							<div class="new-top">
								<a href="{{url('/subcategories')}}?cat_id={{$cat->id}}" >
									<img src="{{$appUrl.'/public/uploads/'.$cat->image}}" class="img-responsive" alt=""/>
								</a>
								<div class="new-text">
									<ul>
										<li><a href="{{url('/subcategories')}}?cat_id={{$cat->id}}">Quick View </a></li>
										<li style="display:none;"><a class="item_add" href=""> Add to cart</a></li>
										<li style="display:none;"><input  type="number" class="item_quantity" min="1" value="1"></li>
									</ul>
								</div>
							</div>
							<div class="new-bottom">
								<h5><a class="name" href="{{url('/subcategories')}}?cat_id={{$cat->id}}">{{$cat->title}} </a></h5>
							</div>
						</div>
					 @endforeach
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

<style>
	.new-bottom {
    text-align: center;
    margin-top: 19px;
}
.new-top img {
    text-align: center;
    vertical-align: middle;
    width: 230px;
    height: 160px;
    margin-right: 50px;
    margin-left: 50px;
}
.product-grids .new-top {
    padding: 0;
}
.new-text {
    background: #7ac63e;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.new-bottom h5 a.name {
    font-size: 18px;
    letter-spacing: 1px;
}
</style>

  @endsection
