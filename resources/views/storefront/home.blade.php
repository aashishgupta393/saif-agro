@extends('storefront.website')
@section('content')
<style>
.home-banner{
	padding: 26rem 0;
  background-size:cover;
  margin:0 auto;
  background-repeat:no-repeat;
}
@media only screen and (min-width: 767) {
  .home-banner {
    padding: 6rem 0;
    background-size: contain;
    margin: 0 auto;
    background-repeat: no-repeat;
  }
}
.new-grid.simpleCart_shelfItem.wow.flipInY.animated.animated {
    height: 334px !important;
    margin-top: 2% !important;
}
.new-top img {
    height: 186px;
    text-align: center;
    vertical-align: middle;
}
.new-bottom {
    text-align: center;
}
.rating {
    display: none;
}
.gallery-grid {

    height: 26.9em;
    border: 1px solid #155f35;
}
.gallery-grid img {
    height: 181px;
}
h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto';
}
.gallery-text.simpleCart_shelfItem a {
    font-size: 18px;
    letter-spacing: 1px;
}
.gallery-text p {
    color: #11ec11;
    font-weight: normal;
    letter-spacing: 1px;
}
.new-bottom h5 a.name {
    font-size: 20px;
    letter-spacing: 1px;
}
.new-grid {
    border: 2px solid #0f7d3e;
}
.new-info {
    border-bottom: none;
    padding-bottom: 7px;
}
.new-text {
    background: #7ac63e;
    display: none;
}
@media only screen and (max-width: 768px){
	.home-banner {
    background-size: 100% 100%;
    padding: 10rem 0;
}
ol.flex-control-nav.flex-control-paging {
    display: none;
}
.new-grid.simpleCart_shelfItem.wow.flipInY.animated.animated {
    height: 285px !important;
}
.new-top img {
    height: 150px;
}
.nav.navbar-nav.navbar-right.header-two-right {
    display: none;
}
.gallery-grid img {
    height: 124px;
}
.gallery-grid {
    height: 22.9em;
    border: 1px solid #155f35;
}
.gallery-text.simpleCart_shelfItem a {
    font-size: 15px;
}
}
h3.title {
    font-size: 38px;
    font-weight: 900;
    color: #FF590F;
}
</style>
<section class="slider grid">
  <div class="flexslider">
    <ul class="slides">
        @foreach(App\Banner::inRandomOrder()->get() as $bk=>$banner)
          <li>
            <div class="home-banner" style="background-image:url('{{$appUrl.'/'.$banner->path}}')">
            </div>
          </li>
        @endforeach
    </ul>
  </div>
</section>

<div class="new">
   <div class="container">
      <div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
         <h3 class="title">Popular <span>Categories</span></h3>
      </div>
      <div class="new-info">
        @php
        $deleay=0.5;
				$flip= range(1,App\Categories::count(),4);
				$flip2=range(2,App\Categories::count(),4);
        @endphp
        @foreach(App\Categories::orderBy('title', 'ASC')->get() as $ckt=>$cat)
				@php
					$catImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
					if (@getimagesize($appUrl.'/public/uploads/'.$cat->image)) {
						$catImage = $appUrl.'/public/uploads/'.$cat->image;
					}
					$redirection = $appUrl."/products?cat_id=".$cat->id;
					if(count($cat->subcategories) > 0){
						$redirection = $appUrl."/subcategories?cat_id=".$cat->id;
					}
				@endphp
         <div class="col-md-3 new-grid <?php if(in_array($ckt,$flip)){ echo "new-mdl";}?>
					 <?php if(in_array($ckt,$flip2)){ echo "new-mdl1";}?>  simpleCart_shelfItem wow flipInY animated" data-wow-delay="{{$deleay}}s">
            <div class="new-top">
               <a href="{{$redirection}}">
                  <img src="{{$catImage}}" class="img-responsive" alt=""/>
               </a>
               <div class="new-text">
                  <ul>
                     <li>
											 <a href="{{$redirection}}">Quick View </a>
										 </li>
                     <li>
											 <a href="{{$redirection}}">Show Products </a>
										 </li>
                  </ul>
               </div>
            </div>
            <div class="new-bottom">
               <h5><a class="name" href="{{$redirection}}">{{$cat->title}} </a></h5>
               <div class="rating">
                  <span class="on">☆</span>
                  <span class="on">☆</span>
                  <span class="on">☆</span>
                  <span class="on">☆</span>
                  <span>☆</span>
               </div>
               <div class="ofr" style="display:none;">
                  <p class="pric1"><del>$2000.00</del></p>
                  <p><span class="item_price">$500.00</span></p>
               </div>
            </div>
         </div>
         @php
          $deleay=$deleay+0.2;
         @endphp
         @endforeach

         <div class="clearfix"> </div>
      </div>
   </div>
</div>

<div class="gallery">
   <div class="container">
      <div class="title-info wow fadeInUp animated" data-wow-delay=".5s">
         <h3 class="title">Popular<span> Products</span></h3>
      </div>
      <div class="gallery-info">
          @php
          $pdelay=0.5;
					$flip=[1,5];
					$flip2=[2,6];

          @endphp
         @foreach(App\Products::inRandomOrder()->limit(8)->get() as $pk=>$product)
				 @php
					 $productImage = "https://saifagrogroup.com/public/uploads/Placeholder.png";
						if(!empty($product->images)){
								$mainImage = json_decode($product->images)[0];
								if (@getimagesize($appUrl.'/'.$mainImage)) {
									$productImage = $appUrl.'/'.$mainImage;
								}
						}
				 @endphp
					<div class="col-md-3 gallery-grid <?php if(in_array($pk,$flip)){ echo "gallery-grid1";}?>
						<?php if(in_array($pk,$flip2)){ echo "gallery-grid2";}?> wow flipInY animated" data-wow-delay=".{{$pdelay}}s">
            <a href="{{url('/product')}}?id={{$product->id}}">
              <img src="{{$productImage}}" class="img-responsive" alt=""/>
            </a>
            <div class="gallery-text simpleCart_shelfItem">
               <h5>
              <a class="name"  href="{{url('/product')}}?id={{$product->id}}">
                 {{$product->product_title}}
               </a>
             </h5>
						 		@if(Session::has('current_user'))
               	<p><span class="item_price">&#x20b9;:{{$product->product_amount}}</span></p>
							 @endif
               	<h4 style="display:none;" class="sizes">Sizes: <a href="#"> s</a> - <a href="#">m</a> - <a href="#">l</a> - <a href="#">xl</a> </h4>
							 <ul>
                  <li><a  href="{{url('/product')}}?id={{$product->id}}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></a></li>
               </ul>
            </div>
         </div>
          @php
          $pdelay=$pdelay+0.2;
          @endphp
         @endforeach
         <div class="clearfix"></div>
      </div>
   </div>
</div>

<script src="{{ asset('public/assets/js/jquery.flexslider.js') }}"></script>
<script type="text/javascript">
   $(window).load(function(){
     $('.flexslider').flexslider({
        animation: "pagination",
        start: function(slider){
          $('body').removeClass('loading');
        }
     });
   });
</script>
@endsection
