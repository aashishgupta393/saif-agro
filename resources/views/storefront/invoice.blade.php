<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link href="{{ asset('public/assets/invoice/print.css') }}" rel="stylesheet" media="print" type="text/css" >
          <title>Invoice Print</title>
   </head>
  
   <body>
      <table border="1" width="60%" style="margin-left:auto;margin-right:auto;">
         <caption>
            <span class="title">Tax Invoice</span>
            <span style="float:right;">(ORIGINAL FOR RECIPIENT)</span>
            <br/>
         </caption>
         <!-- 1 section -->
         <tr>
            <td rowspan="3" colspan="4">
                <b>{{$company->company_name}}</b><br />
                {{$company->address}} <br />
                {{$company->address1}} <br />
                {{$company->city}}, {{$company->zip_code}} <br />
               GSTIN/UIN: {{$company->GSTIN}}  <br />
               State Name: {{$company->state}}
            </td>
            <td colspan="2">
               Invoice No.<br />
               <strong>{{$order->order_no}}</strong>
            </td>
            <td colspan="3">
               Dated <br />
               <strong>{{date('d-M-Y',strtotime($order->created_at))}}</strong>
            </td>
         </tr>
         <tr>
            <td colspan="2">
               Delivery Note<br />
               &nbsp;
            </td>
            <td colspan="3">
               Mode/Terms of payment<br />
               &nbsp;
            </td>
         </tr>
         <tr>
            <td colspan="2">
               Supplier's Ref.<br />
               &nbsp;
            </td>
            <td colspan="3">
               Other Reference(s)<br />
               &nbsp;
            </td>
         </tr>
         <!-- 2 section -->
         <tr>
            <td rowspan="4" colspan="4">
                Buyer<br/>
               <strong>{{$customer->username}}</strong> <br />
              {{$customer->company_address}},{{$customer->city}} ,,{{$customer->zip_code}}<br />
               Mob:{{$customer->contact_no}}<br />
               GSTIN/UIN:{{$customer->gst_no}}<br />
               State Name:  {{$customer->state}}
            </td>
            <td colspan="2">
               Buyer's Order No.<br />
               &nbsp;
            </td>
            <td colspan="3">
               Dated <br />
               &nbsp;
            </td>
         </tr>
         <tr>
            <td colspan="2">
               Despatch Documnet No.<br />
               &nbsp;
            </td>
            <td colspan="3">
               Delivery Note Date<br />
               &nbsp;
            </td>
         </tr>
         <tr>
            <td colspan="2">
               Despatched through<br />
               &nbsp;
            </td>
            <td colspan="3">
               Destination<br />
               &nbsp;
            </td>
         </tr>
         <tr>
            <td colspan="5">
               Terms.of Delivery<br />
               &nbsp;<br />
               <br />
               <br />
            </td>
         </tr>
         <!-- 3 section -->
         <tr >
            <th>SI No.</th>
            <th>Description of Goods</th>
            <th>Part No.</th>
            <th>HSN/SAC</th>
            <th class="bqua">Quantity</th>
            <th>Rate</th>
            <th class="bper">per</th>
            <th>Disc.%</th>
            <th>Amount</th>
         </tr>
         @php
         $orderGST = 0;
         $withoutGstTotal = 0;
         @endphp
           @foreach($items as $ik=>$item)
             @php
             $gstAmount  = $item->price - ($item->price*(100/(100+18)));
             $orderGST+=$gstAmount*$item->qty;
             $netPrice = $item->price - $gstAmount;
             $withoutGstTotal+=$netPrice*$item->qty;
             @endphp
             <tr class="br-details">
                <td>{{$ik+1}}</td>
                <td>
                   <strong>{{$item->product_title}}</strong> <br />
                </td>
                <td>{{$item->product->backend_part_no}}</td>
                <td>{{$item->product->hsn_code}}</td>
                <td><strong>{{$item->qty}} Nos</strong></td>
                <td>{{round($netPrice)}}</td>
                <td>Nos</td>
                <td>&nbsp;</td>
                <td style="text-align:right">
                  {{round($netPrice*$item->qty)}}
                </td>
             </tr>
            @endforeach
            <tr class="br-details">
               <td>&nbsp;</td>
               <td>
                  <br />
                  <span class="gst" style="text-align:right">
                  @if(strtolower($company->state) == strtolower($customer->state))
                  CGST <br/>
                  SGST <br/>
                  @else
                    IGST <br/>
                    @endif
                  </span><br/>
                  <!-- Less: <span class="round">Round Off</span> -->
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td><strong>&nbsp;</strong></td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td style="text-align:right">
                  <strong>
                  {{round($withoutGstTotal)}} <br/>
                  <br/>
                  <span>
                    @if(strtolower($company->state) == strtolower($customer->state))
                        {{round($orderGST)/2}} <br/>
                        {{round($orderGST)/2}} <br/>
                    @else
                      {{round($orderGST)}} <br/>
                      @endif
                  </span><br/>
                  <!-- (-)0.04 -->
                  </strong>
               </td>
            </tr>

         <tr>
            <td></td>
            <td>Total</td>
            <td></td>
            <td></td>
            <td><strong>10 Nos</strong></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align:right"><strong>₹ {{$order->subtotal}}</strong></td>
         </tr>
         <!-- 4 section -->
         <tr class="">
            <td colspan="9">
               Amount Chargeable(in words) <span class="cond"> E. & O.E</span><br />
               <strong style="text-transform: capitalize;"> INR {{getIndianCurrency($order->subtotal)}} Only</strong>
            </td>
         </tr>
         <!-- HSN details section -->
         <tr>
            <th rowspan="2" colspan="3">HSN/SAC</th>
            <th rowspan="2" >Taxable Value</th>

            <th colspan="2">Central Tax</th>
            <th colspan="2">State Tax</th>
            <th rowspan="2" >Total Tax Amount</th>
         </tr>
         <tr>
            <td>Rate</td>
            <td>Amount</td>
            <td>Rate</td>
            <td>Amount</td>
         </tr>
         <tr>
            <td colspan="3">8467</td>
            <td>{{$order->subtotal}}</td>
            <td>

              9%
            </td>
            <td>
            @if(strtolower($company->state) == strtolower($customer->state))
                {{round($orderGST)/2}}
            @else

              @endif
            </td>
            <td>9%</td>
            <td>
            @if(strtolower($company->state) == strtolower($customer->state))
                {{round($orderGST)/2}}
            @else
            @endif
            </td>
            <td>{{round($orderGST)}}</td>
         </tr>
         <tr>
            <td colspan="3"> Total</td>
            <td>{{round($orderGST)}}</td>
            <td></td>
            <td>{{round($orderGST)/2}}</td>
            <td></td>
            <td>{{round($orderGST)/2}}</td>
            <td>{{round($orderGST)}}</td>
         </tr>
         <tr class="word">
            <td colspan="9" class="amnt">Tax Amount(in words):
              <strong> INR {{getIndianCurrency(round($orderGST))}} Only</strong><br/><br/></td>
         </tr>
         @php
         $accounts = [
           "1" => ["acc_holder_name"=>"Saif Agro Tech","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8612192373","ifsc_code"=>"KKBK0008038","branch"=>"S.J.P ROAD"],
           "2" => ["acc_holder_name"=>"TMM Trades","bank_name"=>"ICICI BANK","acc_no"=>"625105501953","ifsc_code"=>"ICIC0006251","branch"=>"NR Road"],
           "3" => ["acc_holder_name"=>"SMS INDUSTRIAL COMPONENT","bank_name"=>"KOTAK MAHINDRA BANK","acc_no"=>"8012868526","ifsc_code"=>"KKBK0000423","branch"=>"BANGALORE"],
         ];
         @endphp
         @php
         $companyAccount = $accounts[$order->company_id];
         @endphp
         <!-- Bank details section -->
         <tr class="bank">
            <td colspan="5" class="bblank"></td>
            <td colspan="4" class="bdetails">
               Company's Bank Details<br/>
               Bank Name :	<strong>{{$companyAccount['bank_name']}}</strong><br/>
               A/c No. : <strong>{{$companyAccount['acc_no']}}</strong><br/>
               Branch & IFS Code: <strong>{{$companyAccount['ifsc_code']}} {{$companyAccount['branch']}} </strong>
            </td>
         </tr>
         <tr class="declare">
            <td colspan="5" class="dd">
               <span style="text-decoration: underline;">Declaration </span><br/>
               We declare that this invoice shows the actual price of the goods <br/> described and that all particulars are true and 				correct.
            </td>
            <td colspan="4" class="bdetails">
               <span class="sign">for {{$company->company_name}} </span> <br/><br/><br/>
               <span class="autho">Authorised Signatory</span>
            </td>
         </tr>
      </table>
      <div style="text-align:center;margin-top:8px;margin-left:auto;margin-right:auto;">This is a Computer Generated Invoice</div>
      <script>
         var onPrintFinished=function(printed){
           setTimeout(function(){
             window.location.href = '{{url("/account")}}';
           },5000)
         }
         window.onload=function(){
           window.focus();
           onPrintFinished(window.print());
           window.close();
         };
      </script>
   </body>
</html>
