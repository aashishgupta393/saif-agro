<div class="shipping-container" style="display:none;">
<h2> Shipping Address</h2>
<div class="row">
  <div class="col-md-4">
      <label class="control-label">Full Name</label>
      <div class="controls">
         <input  name="full_name" type="text" placeholder="full name"  >
      </div>
  </div>
  <div class="col-md-4">
      <label class="control-label">Address Line 1</label>
      <div class="controls">
          <input name="address_line_1" type="text" placeholder="address line 1"   >
      </div>
  </div>
  <div class="col-md-4">
      <label class="control-label">Address Line 2</label>
      <div class="controls">
          <input  name="address_line_2" type="text" placeholder="address line 2"   >
      </div>
  </div>
  <div class="col-md-4">
      <label class="control-label">City / Town</label>
      <div class="controls">
          <input  name="city" type="text" placeholder="city">
      </div>
  </div>

  <div class="col-md-4">
      <label class="control-label">State / Province / Region</label>
      <div class="controls">
        <select class="select-box" name="state" id="state" ="">
           <option value=""> Please Select</option>
             @foreach($cities as $state=>$city)
              <option value="{{$state}}">{{$state}}</option>
             @endforeach
        </select>
      </div>
  </div>
  <div class="col-md-4">
      <label class="control-label">Zip / Postal Code</label>
      <div class="controls">
          <input  name="zip_code" type="text" placeholder="zip or postal code"     >
      </div>
  </div>
</div>
</div>
