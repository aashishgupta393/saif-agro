<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
   </head>
   <style>
      table tbody tr td{ padding-left:5px;padding-right:5px;padding-top: 5px;padding-bottom: 5px; }
      .hide-tr td{
        border-bottom:none !important;
        border-top:none !important;
      }
      .hide-tr {
        border-bottom:none !important;
        border-top:none !important;
      }
   </style>
   <body>
      @php
      $img = file_get_contents("https://saifagrogroup.com/resources/views/storefront/main_logo.png");
      $base64 = "data:image/png;base64,".base64_encode($img);
      $maxno = 7;
      if(count($items) >= $maxno){
        $remaining = 0;
      }else{
        $remaining = $maxno-count($items);
      }
      $shippingaddress = $customer;
      if(empty($order->shipping_address)){
          $shippingaddress = ReturnShippingAddress($order->id);
          $shippingaddress->username = $shippingaddress->f_name." ".$shippingaddress->l_name;
          $shippingaddress->company_address = $shippingaddress->address_1." ".$shippingaddress->adress_2;
          $shippingaddress->zip = $shippingaddress->zip_code;
          $shippingaddress->gst_no = $customer->gst_no;
          $shippingaddress->contact_no = $customer->contact_no;
      }
      @endphp
      <table border="1" width="100%"   style="border:2px solid #000; margin-left:auto;margin-right:auto;border-collapse: collapse;">
         <tbody>
            <tr>
               <td  colspan="3" rowspan="5" style="border-right: 1px solid #fff;" >
                  <h2 style="margin-bottom: 2px;margin-top: 2px;" >{{$company->company_name}}</h2>
                  {{$company->address}},<br/>
                  {{$company->address1}}<br/>
                  {{$company->city}}, {{$company->state}} {{$company->zip_code}}<br/>
                  Email : {{$company->company_email}}<br/>
                  Contact No : {{$company->company_contact_no}}<br/>
                  GST No : {{$company->GSTIN}}
               </td>
               <td colspan="3" rowspan="5" style="border-right: 1px solid #fff;">
                  <img src="{{$base64}}" width="225px" height="80px" style="margin-left: -70px;">
               </td>
               <td colspan="2" style="border-left: 1px solid #fff;border-right: 2px solid #000;border-bottom: 1px solid #000;">
                  <br/>
               </td>
            </tr>
            <tr>
               <td colspan="6" style="border-left: 2px solid #000;text-align:center;width:30%;">
                  EWAYBILL NO: {{$order->e_way_bill}}<br/>
               </td>
            </tr>
            <tr>
               <td colspan="6" style="border-left: 2px solid #000;text-align:center;">
                  INVOICE NO:  {{$order->order_no}}<br/>
               </td>
            </tr>
            <tr>
               <td colspan="6" style="border-left: 2px solid #000;text-align:center;">
                  DATE: {{date('d-M-Y',strtotime($order->created_at))}}<br/>
               </td>
            </tr>
            <tr>
               <td colspan="6" style="border-left: 2px solid #000;text-align:center;">
                  CUSTOMER ID: {{$customer->id}}
               </td>
            </tr>
            <tr>
               <td colspan="8" style="border-top: 1px solid #000;border-bottom: 1px solid #000;border-right: 2px solid #000;" >
                  <p style="text-align:center;margin-top: 4px;margin-bottom: 4px;font-size: 20px;"><strong>TAX INVOICE</strong></p>
               </td>
            </tr>
            <tr>
               <td  colspan="4" style="border-right: 1px solid #fff;">
                  <p style="margin-top: 4px;margin-bottom: 10px;font-size: 18px;">
                     <strong>BILL TO</strong>
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 9px;font-size: 14px;">
                     <strong>Name:</strong> {{$customer->username}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 9px;font-size: 14px;">
                     <strong>Address:</strong> {{$customer->company_address}},<br/>
                     {{$customer->city}}, {{$customer->zip_code}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;">
                     <strong>GSTIN/UIN:</strong> {{$customer->gst_no}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;"><strong>State:</strong>  {{$customer->state}}</p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;"><strong>MO:</strong> {{$customer->contact_no}}</p>
               </td>
               <td colspan="4" style="border-right: 2px solid #000;">
                  <p style="margin-top: 4px;margin-bottom: 10px;font-size: 18px;"><strong>SHIP TO</strong></p>
                  <p style="margin-top: 4px;margin-bottom: 9px;font-size: 14px;">
                     <strong>Name:</strong>
                     {{$shippingaddress->username}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 9px;font-size: 14px;">
                     <strong>Address:</strong> {{$shippingaddress->company_address}},<br/>
                     {{$shippingaddress->city}}, {{$shippingaddress->zip_code}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;">
                     <strong>GSTIN/UIN:</strong> {{$shippingaddress->gst_no}}
                  </p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;"><strong>State:</strong>  {{$shippingaddress->state}}</p>
                  <p style="margin-top: 4px;margin-bottom: 6px;font-size: 14px;"><strong>MO:</strong> {{$shippingaddress->contact_no}}</p>
               </td>
            </tr>
            <tr>
               <th style="width:6%;">S NO</th>
               <th style="width:7%;">PART NO</th>
               <th style="" >DESCRIPTION</th>
               <th style="width:11%;">HSN CODE</th>
               <th style="width:10%;">QTY</th>
               <th style="width:10%;">AMOUNT</th>
               <th style="width:10%;">TAX RATE</th>
               <th style="border-right: 2px solid #000;">TOTAL</th>
            </tr>
            @php
            $orderGST = 0;
            $withoutGstTotal = 0;
            @endphp
            @foreach($items as $ik=>$item)
            @php
            $gstAmount  = $item->price - ($item->price*(100/(100+18)));
            $orderGST+=$gstAmount*$item->qty;
            $netPrice = $item->price - $gstAmount;
            $withoutGstTotal+=$netPrice*$item->qty;
            $variantTitle = "";
            $partNo = $item->product->part_no;
            if($item->variant_no > 0){
            $variants = json_decode($item->product->variants);
            $variant = $variants[$item->variant_no-1];
            $variantTitle = $variant->titile;
            $partNo = $variant->part_no;
            }
            @endphp
            <tr class="br-details">
               <td style="text-align:center">{{$ik+1}}</td>
               <td>{{$partNo}}</td>
               <td><strong> {{$item->product->product_title}} {{$variantTitle}}</strong></td>
               <td style="text-align:center" >{{$item->product->hsn_code}}</td>
               <td style="text-align:center" >{{$item->qty}}</td>
               <td style="text-align:center">{{round($netPrice)}}</td>
               <td style="text-align:center">{{$item->product->tax_rate}}</td>
               <td style="text-align:center;border-right: 2px solid #000;">
                  <strong>
                  {{round($netPrice*$item->qty)}}
                  </strong>
               </td>
            </tr>
            @endforeach
            @if($remaining > 0)
            @for($n=0;$n<$remaining;$n++)
            <tr class="br-details hide-tr">
               <td style="text-align:center">&nbsp;</td>
               <td>&nbsp;</td>
               <td><strong>&nbsp;</strong></td>
               <td style="text-align:center" >&nbsp;</td>
               <td style="text-align:center" >&nbsp;</td>
               <td style="text-align:center">&nbsp;</td>
               <td style="text-align:center">&nbsp;</td>
               <td style="text-align:center;border-right: 2px solid #000;">
                  <strong>
                  &nbsp;
                  </strong>
               </td>
            </tr>
            @endfor
            @endif
            <tr>
               <td colspan="4" style="padding-top:10px;padding-bottom:10px;"> All Dispute Are Subject To  {{$company->city}} Jurisdiction Only </td>
               <td colspan="3" style="text-align:center"> Total Taxable Value</td>
               <td colspan="" style="text-align:center;border-right: 2px solid #000;"><strong>{{round($withoutGstTotal)}}</strong></td>
            </tr>
            <tr>
               <td colspan="4" style="padding-top:10px;padding-bottom:10px;"> <strong>Total Invoice Value in Word:</strong><br/>
                  Rupees {{getIndianCurrency($order->subtotal)}} Only
               </td>
               <td colspan="2" style="text-align:right">
                  @if(strtolower($company->state) == strtolower($shippingaddress->state))
                  SGST@ <br>IGST@
                  @else    IGST@
                  @endif
               </td>
               <td colspan="1" style="text-align:center"> @if(strtolower($company->state) == strtolower($shippingaddress->state))   9%<br>9%       @else 18%        @endif</td>
               <td colspan="" style="text-align:center;border-right: 2px solid #000;"><strong>
                  @if(strtolower($company->state) == strtolower($customer->state))
                  {{round($orderGST)/2}} <br/>
                  {{round($orderGST)/2}} <br/>
                  @else
                  {{round($orderGST)}} <br/>
                  @endif
                  </strong>
               </td>
            </tr>
            <tr>
               <td colspan="4"></td>
               <td colspan="3" style="text-align:center">Packaging & Forwading</td>
               <td colspan="" style="text-align:center;border-right: 2px solid #000;"><strong>{{$order->packing_charges}}</strong></td>
            </tr>
            <tr>
               <td colspan="4">			</td>
               <td colspan="3" style="text-align:center">Total Invoice Value</td>
               <td colspan="" style="text-align:center;border-right: 2px solid #000;"><strong>   {{$order->order_amount}}</strong></td>
            </tr>
            <tr>
               <td colspan="4" style="padding-top:10px;padding-bottom:10px;"><strong>Tax Amount Subject To Reverse Charge </strong></td>
               <td colspan="4" style="text-align:left;border-left: 1px solid #fff;border-right: 2px solid #000;"><strong>
                  @if(strtolower($company->state) == strtolower($shippingaddress->state))
                  SGST {{round($orderGST)/2}} &nbsp;&nbsp;&nbsp;&nbsp;
                  CGST {{round($orderGST)/2}}
                  @else
                  IGST {{round($orderGST)}}
                  @endif
                  </strong>
               </td>
            </tr>
            <tr class="declare">
               <td colspan="4">
                  <p style="margin-top: 4px;margin-bottom: 4px;"><strong> E &.O.E </strong></p>
                  <br/>
                  <span style="text-decoration: underline;"><strong>Declaration</strong> </span><br/>
                  We declare that this invoice shows the actual price of the <br/>goods described snd that all particulars are true and correct
               </td>
               <td colspan="4" style="border-right: 2px solid #000;">
                  <p style="text-align:right;margin-top: 4px;margin-bottom: 4px;">For {{$company->company_name}} </p>
                  <br/><br/><br/>
                  <p style="text-align:right;margin-top: 4px;margin-bottom: 4px;"><strong>Authorised Signatory</strong></p>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
   @if(isset($print))
     <script src="https://www.google.com/cloudprint/client/cpgadget.js">
     </script>
     <script>
       var onPrintFinished=function(printed){
         setTimeout(function(){
           window.location.href = '{{$redirect}}';
         },5000)
       }

         window.onload=function(){
         var ua = navigator.userAgent.toLowerCase();
         var isAndroid = ua.indexOf("android") > -1;
         if (isAndroid) {
          var gadget = new cloudprint.Gadget();
          gadget.setPrintDocument("url", "Invoice", window.location.href, "utf-8");
          gadget.openPrintDialog();
          }else{
            window.focus();
            onPrintFinished(window.print());
            window.close();
          }
         };
     </script>
   @endif

</html>
