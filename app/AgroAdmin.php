<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgroAdmin extends Model
{

  protected $table = 'admin';
  protected $fillable = ['username','password'];

}
