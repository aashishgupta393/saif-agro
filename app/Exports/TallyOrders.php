<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TallyOrders  implements FromCollection, WithHeadings
{
    private $orders;
    private $headings;

    use Exportable;
    public function __construct($data,$headings)
    {
      $this->orders = $data;
      $this->headings = $headings;
    }
    public function collection()
    {
        return collect($this->orders);
    }

    public function headings(): array
    {
        return $this->headings;
    }


}
