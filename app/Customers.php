<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{

  protected $table = 'customers';
  protected $casts = [
      'created_at' => 'datetime:d/m/Y',
  ];
  protected $fillable = [
    'username',
    'contact_no',
    'email',
    'company_address',
    'state',
    'city',
    'has_gst',
    'gst_no',
    'zip_code',
  ];

  public function orders()
  {
      return $this->hasMany('App\Orders');
  }


}
