<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
  protected $table = 'category';

  public function subcategories(){
    return $this->hasMany('App\SubCategories', 'cat_id');
  }

  public function products() {
    return $this->hasMany('App\Products','cat_id');
  }

}
