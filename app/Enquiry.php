<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Enquiry extends Model
{
  protected $table = 'tbl_enquiry';

  public function product()
   {
       return $this->belongsTo('App\Products','pid');
   }

}
