<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use View;
use Response;

class CheckType
{

    public function handle($request, Closure $next)
    {
        if (!Session::has('admin')){
          return Response::view('admin.login');
          exit;
        }
        return $next($request);
    }

}
