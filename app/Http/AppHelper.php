<?php

use DB;
if (!function_exists('sendsms')) {
  function sendsms($message,$no){
    $number = '91'.$no;
    $senderID  = "ISDTPL";
    $user  = "saifagro";
    $password  = "saif@123";
    $msg = urlencode($message);
    $url = "http://txtsms.themisq.com/vendorsms/pushsms.aspx?user=".$user."&password=".$password."&msisdn=".$number."&sid=".$senderID."&msg=".$msg."&fl=0&gwid=2";
    $file = file_get_contents($url);
    // return $file;
  }
}

function SendOtpToCutomer($data)
{
    // dd($data);
    Mail::send('mail', $data, function($message)use($data){
      $message->to($data['email'],$data['name'])->subject($data['subject']);
      $message->from('admin@saifagrogroup.com','Admin');
    });
  }

function getFinacialYear()
{
  if (date('m') <= 3) {
    $financial_year = (date('Y')-1) . '-' . date('Y');
  } else {//After June 2015-2016
    $financial_year = date('Y') . '-' . (date('Y') + 1);
  }
  return $financial_year;
}
function formMonth(){
    $month = strtotime(date('Y').'-'.date('m').'-'.date('j').' - 8 months');
    $end = strtotime(date('Y').'-'.date('m').'-'.date('j').' + 8 months');
    while($month < $end){
        $selected = (date('F', $month)==date('F'))? ' selected' :'';
        echo '<option'.$selected.' value="'.date('m', $month).'">'.date('F', $month).'</option>'."\n";
        $month = strtotime("+1 month", $month);
    }
}
function formYear(){
    for($i=2020; $i<=date('Y'); $i++){
        $selected = ($i==date('Y'))? ' selected' :'';
        echo '<option'.$selected.' value="'.$i.'">'.$i.'</option>'."\n";
    }
}
function SendConfirmationEmailAndUpdateInvoice($data)
{

    Mail::send('invoice.invoice', $data, function($message)use($data){
      $message->to($data['email'],$data['name'])->subject($data['subject']);
      $message->from('admin@saifagrogroup.com','Admin');
    });

    $pdf = PDF::loadView('invoice.invoice', $data);
    $output = $pdf->output();
    $destinationPath = 'public/invoices/';
    $file = time().rand(0000,9999)."-invoice.pdf";
    $file_link = $destinationPath.$file;
    file_put_contents($file_link, $output);
    $invoicepath =  URL::to("/")."/".$file_link;
    DB::table('tbl_order')->where('id', $data['id'])->update(['pdf_invoice_path' => $invoicepath]);
}

function ReturnShippingAddress($id)
{
  return DB::table('shipping_address')->where('order_no', $id)->first();
}

function returnInvoiceNumber($company_id)
{
    $count = DB::table('tbl_order')->where(['company_id'=>$company_id])->count();
    $intials = "SA";
    switch ($company_id) {
      case '1':
       $intials = "SA";
        break;
      case '2':
       $intials = "TMM";
        break;
      case '3':
       $intials = "SMS";
        break;
    }
   return $intials.getFinacialYear()."/".$count;
}



function getIndianCurrency(float $number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred','thousand','lakh', 'crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}


?>
