<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use View;
use App\AgroAdmin;
use App\Companies;
use App\Customers;
use App\Banner;
use App\Categories;
use App\Products;
use App\Orders;
use App\SubCategories;
use App\SubCategories2;
use App\Items;
use DB;
use URL;
use Mail;
use PDF;

class ApiController extends Controller
{

  public function generateOTP()
  {
    $otp = mt_rand(1000,9999);
    return $otp;
  }
  public function SedOtp(Request $request)
  {
        $email = $request->email;
        $contact_no = $request->contact_no;
        $has_gst = $request->has_gst;
        $old_email_count=Customers::where('email',$email)->count();
        if($old_email_count > 0){
          $response=['code'=>100,'msg'=>'Email already Registered.'];
          return response()->json($response);
          exit;
        }
        $contactExist=Customers::where('contact_no',$contact_no)->count();
        if($contactExist > 0){
          $response=['code'=>100,'msg'=>'Contact no Already Exists'];
          return response()->json($response);
          exit;
        }
        if($has_gst == 'yes'){
            $gst_no = $request->gst_no;
            $gestExist=Customers::where('gst_no',$gst_no)->count();
            if($gestExist > 0){
              $response=['code'=>100,'msg'=>'GST No. alredy Registered'];
              return response()->json($response);
              exit;
            }
        }
        $mobile = $contact_no;
      if($mobile==''){
        return response()->json(['code'=>400,'msg'=>"Mobile number not valid".$mobile]);
        exit;
      }
      $otp = $this->generateOTP();
      $message = 'Your Otp for Saif Agro Group will be '.$otp;
      sendsms($message,$mobile);
      $data['email'] =$email;
      $data['subject'] = 'OTP For Registration';
      $data['name'] = $request->name;
      $data['msg'] = $message;
      SendOtpToCutomer($data);

      return response()->json(['code'=>200,'msg'=>'otp sent successfully','otp'=>$otp]);
  }


  public function UpdateCustomerProfile(Request $request)
  {

        $email = $request->email;
        $contact_no = $request->contact_no;
        $has_gst = $request->has_gst;
        $old_email_count=Customers::where('email',$email);
        if($old_email_count->count() > 0){
          $udata = $old_email_count->first();
          if($udata->id != $request->id){
            $response=['code'=>100,'msg'=>'Email already Registered.'];
            return response()->json($response);
            exit;
          }
        }
        $contactExist=Customers::where('contact_no',$contact_no);
        if($contactExist->count() > 0){
          $udata = $contactExist->first();
            if($udata->id != $request->id){
            $response=['code'=>100,'msg'=>'Contact no Already Exists'];
            return response()->json($response);
            exit;
          }
        }
        if($has_gst == 'yes'){
            $gst_no = $request->gst_no;
            $gestExist=Customers::where('gst_no',$gst_no);
            if($gestExist->count() > 0){
                $udata = $gestExist->first();
                if($udata->id != $request->id){
                  $response=['code'=>100,'msg'=>'GST No. alredy Registered'];
                  return response()->json($response);
                  exit;
                }
            }
        }

    $customer = Customers::find($request->id);
    $customer->username = $request->name;
    $customer->contact_no = $request->contact_no;
    $customer->email = $request->email;
    $customer->password = $request->password;
    $customer->company_name = $request->company_name;
    $customer->company_address = $request->company_address;
    $customer->state = $request->state;
    $customer->city = $request->city;
    $customer->has_gst = $request->has_gst;
    $customer->gst_no = $request->gst_no;
    $customer->zip_code = $request->zip_code;
    $customer->whats_app_no = $request->whats_app_no;
    $customer->save();
    $response=['code'=>200,'msg'=>'Profile Updated'];
    return response()->json($response);
  }



  public function RegisterUser(Request $request)
  {


    $customer = new Customers;
    $customer->username = $request->name;
    $customer->contact_no = $request->contact_no;
    $customer->email = $request->email;
    $customer->password = $request->password;
    $customer->company_address = $request->company_address;
    $customer->state = $request->state;
    $customer->city = $request->city;
    $customer->has_gst = $request->has_gst;
    $customer->gst_no = $request->gst_no;
    $customer->zip_code = $request->zip_code;
    $customer->save();
    $data = [
      'name'=>$request->name,
      'email'=>$request->email,
      'subject'=>"Verification mail by Saif Agro Admin",
      'verification_url'=>URL::to("/")."/verify/".base64_encode($request->email),
    ];
    $msg = "Hello Saif Agro Group one New user has registered on your website/App. kindly Approve/disapprove from the admin";
    sendsms($msg,"8041227486");
      Mail::send('mails.verify_email', $data, function($message)use($data){
        $message->to($data['email'],$data['name'])->subject($data['subject']);
        $message->from('admin@saifagrogroup.com','Admin');
      });

      Mail::send('mails.approve_email', $data, function($message) {
         $message->to('admin@saifagrogroup.com', 'Admin')->subject('New Registration');
         $message->from('admin@saifagrogroup.com','Admin');
      });
    $response=['code'=>200,'msg'=>'Registration Done Successfully. please Wait For Admin Approval '];
    return response()->json($response);
  }
  public function HomePageData(Request $request)
  {
    $return =[];
    $return['banner']=Banner::get();
    $return['categories']=Categories::with('subcategories')->get();
    return response()->json($return);
  }

  public function GetSubcategories1(Request $request)
  {
    $return =[];
    $return['sucategories']= SubCategories::with('subcategories2')->where(['cat_id'=>$request->cat_id])->get();
    return response()->json($return);
  }

  public function GetSubcategories2(Request $request)
  {
    $return =[];
    $return['sucategories2']=SubCategories2::where(['sub_cat1'=>$request->sub_cat_id])->get();
    return response()->json($return);
  }

  public function ReturnProductForAPI($product,$comp)
  {
      $pimages =[];
      if(!empty($product->images)){
        $images = json_decode($product->images);
        foreach ($images as  $image) {
          $pimages[]=$image;
        }
      }
      $mvariants = [];
      if(!empty($product->variants)){
        $variants = json_decode($product->variants);
        foreach ($variants as  $variant) {
          $mvariants[]=$variant;
          $pimages[]=$variant->image;
        }
      }
      $product->variants = $mvariants;
      $productAmount = (count($mvariants) > 0 ? NULL : $product->product_amount);
      $product->product_amount = $productAmount;
      $product->images = $pimages;
      $product->company_info = $comp[$product->company_id];
    return $product;
  }
  public function ProductsBySubCat(Request $request)
  {
    $sub_cat = $request->sub_cat;
    $cmpanies = Companies::get();
    $comp=[];
    foreach ($cmpanies as $smp) {
      $comp[$smp->id]=$smp;
    }
    $return =[];
    $page = $request->has('page') ? $request->get('page') : 1;
    $limit = $request->has('limit') ? $request->get('limit') : 10;
    $products = DB::table('products')->where(['sub_cat1'=>$sub_cat])->limit($limit)->offset(($page - 1) * $limit)->get();

    foreach ($products as $key => $product) {
        $product = $this->ReturnProductForAPI($product,$comp);
    }
    $return['products'] = $products;
    return response()->json($return);
  }

    public function UpdatePassword(Request $request)
    {

      $contactExist=Customers::where(['password' => $request->password,'contact_no'=>$request->contact])->count();
      if($contactExist > 0){
        $response=['code'=>100,'msg'=>'Current password And Requested Passwords Are same'];
        return response()->json($response);
        exit;
      }else{
        if(Customers::where('contact_no', $request->contact)->count() > 0){
            $update = Customers::where('contact_no', $request->contact)->update(['password' => $request->password]);
            if($update){
              $response=['code'=>200,'msg'=>'Password Updated SucessFully Please Login'];
              return response()->json($response);
              exit;
            }else{
              $response=['code'=>100,'msg'=>'Somthing Went Wrong'];
              return response()->json($response);
              exit;
            }
        }else{
          $response=['code'=>100,'msg'=>'Account Not Exist'];
          return response()->json($response);
          exit;
        }
      }
    }

    public function SendOTPForForgot(Request $request)
    {
        $contact_no = $request->contact_no;
        $contactExist=Customers::where('contact_no',$contact_no);
        if($contactExist->count() == 0){
          $response=['code'=>100,'msg'=>'Not A Registered user'];
          return response()->json($response);
          exit;
        }

        $customer = $contactExist->first();
        $mobile = $contact_no;
        if($mobile==''){
          return response()->json(['code'=>400,'msg'=>"Mobile number not valid".$mobile]);
          exit;
        }
      $otp = $this->generateOTP();
      $message = 'Your Otp for Saif Agro Group will be '.$otp;
      sendsms($message,$mobile);

      $data['email'] = $customer->email;
      $data['subject'] = 'OTP For Registration';
      $data['name'] = $customer->name;
      $data['msg'] = $message;
      SendOtpToCutomer($data);

      return response()->json(['code'=>200,'msg'=>'otp sent successfully','otp'=>$otp]);

    }


  public function Validateuser(Request $request)
  {
      $email = $request->email;
      $password = $request->password;
      $old_email_count=Customers::where('email',$email)->count();
      if($old_email_count == 0){
        $response=['code'=>100,'msg'=>'Not a Register user'];
        return response()->json($response);
        exit;
      }
      $exist = Customers::where(['email'=>$email,'password'=>$password])->count();
      if($exist == 0){
        $response=['code'=>100,'msg'=>'Invalid User name or Password'];
        return response()->json($response);
        exit;
      }else{
        $data = Customers::where(['email'=>$email,'password'=>$password])->first();
        switch ($data->status){
          case 0:
            $response=['code'=>100,'msg'=>'Acccount not activated yet Please Wail'];
            break;
          case 1:
            $response=['code'=>200,'msg'=>'Authorised','user'=>$data];
            break;
          case 2:
            $response=['code'=>100,'msg'=>'Account Not Approved'];
            break;
          default:
            break;
        }
        return response()->json($response);
        exit;
      }
  }

  public function GetKeys()
  {
      $return=[];
      $return['comapnies']=Companies::get();
      return response()->json($return);
  }

  public function CreateOrder(Request $request)
  {
    $response = file_get_contents('php://input');
    $data = json_decode($response);
    $mainArray=[];
    $mainArray['customer_id']= $data->customer_id;
    $mainArray['company_id']= $data->company_id;
    $mainArray['order_no']= "ORD--".$data->company_id."--".rand();
    $mainArray['order_amount']= $data->order_amount;
    $mainArray['subtotal']= $data->subtotal;
    $mainArray['packing_charges']= $data->packing_charges;
    $mainArray['payment_id']= $data->payment_id;
    $mainArray['payment_status']= $data->payment_status;
    $mainArray['created_at']= date('Y-m-d H:i:s');
    $mainArray['shipping_address']= json_encode($data->shipping_address);
    $mainArray['order_from'] = 'app';

    // dd($mainArray);
    DB::table('tbl_order')->insert($mainArray);
    $orderID = DB::getPdo()->lastInsertId();
    $itemsArray=[];
    foreach ($data->items as $key => $item) {
      $itemsArray[] = [
        'orders_id' => $orderID,
        'product_id' => $item->product_id,
        'product_title' => $item->product_title,
        'variant_no' => $item->variant_no,
        'price' => $item->price,
        'qty' => $item->quantity,
        'item_total' => $item->quantity*$item->price,
      ];
    }
    DB::table('tbl_order_item')->insert($itemsArray);
    if(isset($data->address1)){
      $shippingArray = [
        "order_no"=>$orderID,
        "address_1"=> $data->address1,
        "adress_2"=> $data->address2,
        "city"=>$data->city,
        "state"=>$data->state,
        "zip_code"=>$data->zip,
      ];
      DB::table('shipping_address')->insert($shippingArray);
    }

    SendConfirmationEmailAndUpdateInvoice($this->GenOrderData(["id"=>$orderID]));

    $response=['code'=>200,'msg'=>'Order Data Saved'];
    return response()->json($response);
  }
  public function GenOrderData($pass)
  {
    $orderId = $pass['id'];
    $data['id'] = $orderId;
    $data['order'] = Orders::where('id', $orderId)->first();
    $data['customer'] = Customers::where('id', $data['order']->customer_id)->first();
    $data['company'] = Companies::where('id', $data['order']->company_id)->first();
    $data['items'] = Items::with(['product'])->where('orders_id', $orderId)->get();
    $data['email'] = $data['customer']->email;
    $data['subject'] = 'Order Confirmation';
    $data['name'] = $data['customer']->username;
    return $data;
  }

  public function SubmitEnquiry(Request $request)
  {
    $saveArray=[];
    $saveArray['name'] = $request->name;
    $saveArray['mobile_no'] = $request->mobile_no;
    $saveArray['email'] = $request->email;
    $saveArray['message'] = $request->message;
    $saveArray['pid'] = $request->pid;
    $saveArray['company_id'] = $request->company_id;
    $saveArray['created_at'] = date('Y-m-d H:i:s');
    DB::table('tbl_enquiry')->insert($saveArray);
    $response=['code'=>200,'msg'=>'Enquiry Save We Will get back You Soon'];
    return response()->json($response);
  }

  public function GetUserOrders(Request $request)
  {
    $user_id=$request->user;
    $orders = DB::table('tbl_order')->where(['customer_id'=>$user_id])->get();
    foreach ($orders as $key => $order) {
      $order->shipping_address = json_decode($order->shipping_address);
      $order->order_items = DB::table('tbl_order_item')->where(['order_id'=>$order->id])->get();
    }
    $response=['code'=>200,'orders'=>$orders];
    return response()->json($response);
  }

  public function Search(Request $request)
  {
    if(isset($request->search)){
      $cmpanies = Companies::get();
      $comp=[];
      foreach ($cmpanies as $smp) {
        $comp[$smp->id]=$smp;
      }

      $data['search']=$request->search;
      $searchWord = strtolower($request->search);
       $products =  DB::select("SELECT * FROM products WHERE lower(product_id) like '%".$searchWord."%'
      OR lower(product_title) like '%".$searchWord."%'
      OR lower(product_description) like '%".$searchWord."%'
       OR lower(hsn_code) like '%".$searchWord."%'
       OR lower(part_no) like '%".$searchWord."%'
       OR lower(brand_name) like '%".$searchWord."%'
       OR lower(backend_part_no) like '%".$searchWord."%'");
         foreach ($products as $key => $product) {
           $product = $this->ReturnProductForAPI($product,$comp);
         }
         $data['products'] = $products;
       $data['categories'] = DB::select("SELECT * FROM category WHERE lower(title) like '%".$searchWord."%'");
       $data['subcategories'] = DB::select("SELECT * FROM sub_category1 WHERE lower(title) like '%".$searchWord."%'");
       $response=['code'=>200,'data'=>$data];
    }else{
      $response=['code'=>100,'data'=>[]];
    }
    return response()->json($response);
  }

  public function AllProducts(Request $request)
  {

    $page = $request->has('page') ? $request->get('page') : 1;
    $limit = $request->has('limit') ? $request->get('limit') : 10;
    $where=[];
    if(isset($request->cat_id)){
      $where['cat_id'] = $request->cat_id;
    }
    if(isset($request->sub_cat1)){
      $where['sub_cat1'] = $request->sub_cat1;
    }
    if(isset($request->sub_cat2)){
      $where['sub_cat2'] = $request->sub_cat2;
    }
    if(isset($request->page) && isset($request->limit)){
      $products = Products::with(['company'])->where($where)->limit($limit)->offset(($page - 1) * $limit)->get();
    }else {
      $products = Products::with(['company'])->where($where)->get();
    }
    $return=[];
    $cmpanies = Companies::get();
    $comp=[];
    foreach ($cmpanies as $smp) {
      $comp[$smp->id]=$smp;
    }
    foreach ($products as $key => $product) {
       $product = $this->ReturnProductForAPI($product,$comp);
    }
    $return['products'] = $products;
    return response()->json($return);
  }


  public function ProductById(Request $request)
  {

    $product = Products::with(['company'])->where(['id'=>$request->id])->first();
    if($product){
      $cmpanies = Companies::get();
      $comp=[];
      foreach ($cmpanies as $smp) {
        $comp[$smp->id]=$smp;
      }
      $product = $this->ReturnProductForAPI($product,$comp);
    }
    $return['product'] = $product;
    return response()->json($return);
  }


  public function MyOrders(Request $request)
  {
    $orders=[];
    $orders['paid'] = Orders::with(['items','company'])
                      ->where(['payment_status'=>'paid'])
                      ->where(['customer_id'=>$request->id])
                      ->orderBy('id', 'DESC')
                      ->get();

    $orders['pending'] = Orders::with(['items','company'])
                      ->where(['payment_status'=>'pending'])
                      ->where(['customer_id'=>$request->id])
                      ->orderBy('id', 'DESC')
                      ->get();
    return response()->json($orders);
    // dd($orders);
  }


  public function Print(Request $request){
    $orderId = $request->id;
    $data['id'] = $orderId;
    $data['order'] = Orders::where('id', $orderId)->first();
    $data['customer'] = Customers::where('id', $data['order']->customer_id)->first();
    $data['company'] = Companies::where('id', $data['order']->company_id)->first();
    $data['items'] = Items::with(['product'])->where('orders_id', $orderId)->get();
    $data['email'] = $data['customer']->email;
    $data['subject'] = 'Order Confirmation';
    $data['name'] = $data['customer']->username;

    $pdf = PDF::loadView('invoice.invoice', $data);
    $output = $pdf->output();
    $destinationPath = 'public/invoices/';
    $file = time().rand(0000,9999)."-invoice.pdf";
    $file_link = $destinationPath.$file;
    file_put_contents($file_link, $output);
    $invoicepath =  URL::to("/")."/".$file_link;
    DB::table('tbl_order')->where('id', $data['id'])->update(['pdf_invoice_path' => $invoicepath]);
  }

}
