<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use View;
use DB;
use App\AgroAdmin;
use App\Companies;
use App\Customers;
use Illuminate\Support\Facades\Response;
use Mail;
use Session;
use URL;
use App\Orders;
use App\Items;
use App\Enquiry;
use App\SubCategories;
use App\Categories;
use App\Products;
use Razorpay\Api\Api;
use PDF;

class StoreFront extends Controller
{
    public function RegisterUser(Request $request)
    {
      $customer = new Customers;
      $customer->username = $request->name;
      $customer->contact_no = $request->contact_no;
      $customer->email = $request->email;
      $customer->password = $request->password;
      $customer->company_address = $request->company_address;
      $customer->state = $request->state;
      $customer->city = $request->city;
      $customer->has_gst = $request->has_gst;
      $customer->gst_no = $request->gst_no;
      $customer->zip_code = $request->zip_code;
      $customer->whats_app_no = $request->whats_app_no;
      $customer->save();
      $response=['code'=>200,'msg'=>'Thank you for Registering with Saif Agro Group. Kindly wait for your approval email, before you attempt to Sign In. Meanwhile for any urgent enquiry or orders, please click on Contact us.'];
      $data = ['name'=>$request->name,'email'=>$request->email,'subject'=>"Verification mail by Saif Agro Admin",'verification_url'=>URL::to("/")."/verify/".base64_encode($request->email),];
      $msg = "Hello Saif Agro Group one New user has registered on your website/App. kindly Approve/disapprove from the admin";
      sendsms($msg,"8041227486");
      Mail::send('mails.verify_email', $data, function($message)use($data){
        $message->to($data['email'],$data['name'])->subject($data['subject']);
        $message->from('admin@saifagrogroup.com','Admin');
      });

      Mail::send('mails.approve_email', $data, function($message) {
         $message->to('admin@saifagrogroup.com', 'Admin')->subject('New Registration');
         $message->from('admin@saifagrogroup.com','Admin');
      });

      return response()->json($response);
    }

    public function ValidateOTP(Request $request)
    {
        $otp = trim($request->otp);
        if($otp==''){
          return response()->json(['code'=>100,'msg'=>'Please Enter OTP']);
          exit;
        }else{
          if($otp == session('otp')){
            return response()->json(['code'=>200,'msg'=>'sucess']);
            exit;
          }else{
            return response()->json(['code'=>100,'msg'=>'OTP not valid']);
            exit;
          }
        }
    }

    public function Validateuser(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $old_email_count=Customers::where('email',$email)->count();
        if($old_email_count == 0){
          $response=['code'=>100,'msg'=>'Not a Register user'];
          return response()->json($response);
          exit;
        }
        $exist = Customers::where(['email'=>$email,'password'=>$password])->count();
        if($exist == 0){
          $response=['code'=>100,'msg'=>'Invalid User name or Password'];
          return response()->json($response);
          exit;
        }else{
          $data = Customers::where(['email'=>$email,'password'=>$password])->first();
          switch ($data->status) {
            case 0:
              $response=['code'=>100,'msg'=>'Acccount not activated yet Please Wait'];
              break;
            case 1:
              Session::put('current_user', $data);
              $response=['code'=>200,'msg'=>'Authorised'];
              break;
            case 2:
              $response=['code'=>100,'msg'=>'Account Not Approved'];
              break;
            default:
              break;
          }
          return response()->json($response);
          exit;
        }
    }

    public function SendOTP(Request $request)
    {
        $email = $request->email;
        $contact_no = $request->contact_no;
        $has_gst = $request->has_gst;
        $old_email_count=Customers::where('email',$email)->count();
        if($old_email_count > 0){
          $response=['code'=>100,'msg'=>'Email already Registered.'];
          return response()->json($response);
          exit;
        }
        $contactExist=Customers::where('contact_no',$contact_no)->count();
        if($contactExist > 0){
          $response=['code'=>100,'msg'=>'Contact no Already Exists'];
          return response()->json($response);
          exit;
        }
        if($has_gst == 'yes'){
            $gst_no = $request->gst_no;
            $gestExist=Customers::where('gst_no',$gst_no)->count();
            if($gestExist > 0){
              $response=['code'=>100,'msg'=>'GST No. alredy Registered'];
              return response()->json($response);
              exit;
            }
        }
        $mobile = $contact_no;
      if($mobile==''){
        return response()->json(['code'=>400,'msg'=>"Mobile number not valid".$mobile]);
        exit;
      }
      $otp = $this->generateOTP();
      $message = 'Your Otp for Saif Agro Group will be '.$otp;
      sendsms($message,$mobile);


      $data['email'] =$email;
      $data['subject'] = 'OTP For Registration';
      $data['name'] = $request->name;
      $data['msg'] = $message;
      SendOtpToCutomer($data);

      session(['otp' => $otp]);
      return response()->json(['code'=>200,'msg'=>'otp sent successfully']);
  }

    public function generateOTP(){
        $otp = mt_rand(1000,9999);
        return $otp;
    }

    public function VerifyEmail(Request $request)
    {
      $email = base64_decode(request()->segment(2));
      $update = Customers::where('email', $email)->update(['email_verified' => 1]);
      return view('storefront.emailverified');
    }

    public function test(Request $request)
    {

    }

      public function addToCart(Request $request)
      {
        $id = $request->pid;
        $qty = $request->qty;
        $product = DB::select('select * from products where id='.$id)[0];
        $cart = Session::get('cart');
        $variant_key = $id."--var--".$request->variant;
        if(!isset($cart[$variant_key])){
            $image = json_decode($product->images)[0];
            $title = $product->product_title;
            $amount = $product->product_amount;

          if($request->variant > 0){
            $variant = json_decode($product->variants,true)[$request->variant-1];
            if(!empty($variant['image'])){
              $image = $variant['image'];
            }
            $amount = $variant['price'];
            $title = $title." ".$variant['titile'];
          }

          $addableArray=["pid" => $product->id,"variant" => $request->variant,"image" => $image,"company_id" => $product->company_id,
            "title" => $title,"price" =>$amount,"qty" => $qty];
          $cart[$variant_key] = $addableArray;
        }else{
          $cart[$variant_key]['qty'] = $cart[$variant_key]['qty']+$qty;
        }
        Session::put('cart', $cart);
        return response()->json(['code'=>200,'msg'=>'Item Added To cart']);

      }


     public function deleteCart(Request $request)
     {
        $id = $request->id;
        $cart = Session::get('cart');
        unset($cart[$id]);
        Session::put('cart', $cart);
        return redirect()->back();
     }

     public function UpdateProdile(Request $request){
        $update = [
        "username" => $request->username,
        "contact_no" => $request->contact_no,
        "email" => $request->email,
        "company_name" => $request->company_name,
        "company_address" => $request->company_address,
        "city" => $request->city,
        "state" => $request->state,
        "zip_code" => $request->zip_code,
        "gst_no" => $request->gst_no,
      ];
        DB::table('customers')->where(['id'=>Session::get('current_user')->id])->update($update);
        $data = Customers::where(['id'=>Session::get('current_user')->id])->first();
        Session::put('current_user', $data);
        return redirect()->back();
     }


     public function CreateOrder(Request $request)
     {
       $customer_id = $request->user_id;
       $company_id = $request->company_id;
       $companyDetails = Companies::where(['id'=>$company_id])->first();
         $key_id = $companyDetails->test_key_id;
         $key_secret = $companyDetails->test_key_secrete;
         if($companyDetails->payment_mode == 1){
           $key_id = $companyDetails->live_key_id;
           $key_secret = $companyDetails->live_key_secrete;
         }

       try {
           if(isset($request->razorpay_payment_id)){
             $payment_id = $request->razorpay_payment_id;
             $api = new Api($key_id, $key_secret);
             $payment = $api->payment->fetch($payment_id);
             $response = $api->payment->fetch($payment_id)->capture(array('amount'=>$payment['amount']));
           }
           $compnayItems =[];
           $cart = Session::get('cart');
           foreach ($cart as $pid =>$itm){
             $itm = (object)$itm;
             if($itm->company_id == $company_id){
               $compnayItems[$pid]=$itm;
             }
           }

          $orderNo = returnInvoiceNumber($company_id);

          $mainArray=[
            'customer_id'=>$customer_id,
            'company_id'=>$company_id,
            'order_no'=>$orderNo,
            'order_amount'=>$request->total,
            'subtotal'=>$request->subtotal,
            'packing_charges'=>$request->packing_charges,
            'payment_status'=>'pending',
            'shipping_address'=>json_encode(Session::get('current_user')),
            'created_at'=>date('Y-m-d H:i:s'),
          ];
           if(isset($request->razorpay_payment_id)){
            $compnayName = $companyDetails->company_name;
            $msg = "Dear $compnayName you have received the amount of $request->total form your website/App for more details please check admin panel.";
            sendsms($msg,$companyDetails->company_contact_no);
            $mainArray['payment_id'] = $request->razorpay_payment_id;
            $mainArray['payment_status'] = 'paid';
            $mainArray['pay_mode'] = 'online';
            $mainArray['pay_date'] = date('Y-m-d H:i:s');
           }

          $mainArray['order_from'] = 'web';
          DB::table('tbl_order')->insert($mainArray);

          $orderID = DB::getPdo()->lastInsertId();
          foreach ($compnayItems as $key => $item){
            $item=(object)$item;
            $variant_key = $item->pid."--var--".$item->variant;
            $itemsArray = [
              'orders_id' => $orderID,
              'product_id' => $item->pid,
              'product_title' => $item->title,
              'price' => $item->price,
              'qty' => $item->qty,
              'variant_no' => $item->variant,
              'item_total' => $item->qty*$item->price
            ];
            DB::table('tbl_order_item')->insert($itemsArray);
            unset($cart[$variant_key]);
          }

          if(!empty($request->address_line_1)){
            $shippingArray = [
              "order_no"=>$orderID,
              "f_name"=> $request->full_name,
              "address_1"=> $request->address_line_1,
              "adress_2"=> $request->address_line_2,
              "city"=>$request->city,
              "state"=>$request->state,
              "zip_code"=>$request->zip_code,
            ];
            DB::table('shipping_address')->insert($shippingArray);
          }
          SendConfirmationEmailAndUpdateInvoice($this->GenOrderData(["id"=>$orderID]));
          Session::put('cart', $cart);
       } catch (\Exception $e) {
           return  $e->getMessage();
           Session::put('error',$e->getMessage());
           return redirect()->back();
       }
       Session::put('success', 'Order Details Saved');
       return redirect()->back();
    }

    public function GenOrderData($pass)
    {
      $orderId = $pass['id'];
      $data['id'] = $orderId;
      $data['order'] = Orders::where('id', $orderId)->first();
      $data['customer'] = Customers::where('id', $data['order']->customer_id)->first();
      $data['company'] = Companies::where('id', $data['order']->company_id)->first();
      $data['items'] = Items::with(['product'])->where('orders_id', $orderId)->get();
      $data['email'] = $data['customer']->email;
      $data['subject'] = 'Order Confirmation';
      $data['name'] = $data['customer']->username;
      return $data;
    }



    public function Search(Request $request)
    {
      $data['appUrl']=URL::to('/');
      if(isset($request->search)){
        $data['search']=$request->search;
        $searchWord = strtolower($request->search);
        $data['products'] =  DB::select("SELECT * FROM products WHERE lower(product_id) like '%".$searchWord."%'
        OR lower(product_title) like '%".$searchWord."%'
        OR lower(product_description) like '%".$searchWord."%'
         OR lower(hsn_code) like '%".$searchWord."%'
         OR lower(part_no) like '%".$searchWord."%'
         OR lower(brand_name) like '%".$searchWord."%'
         OR lower(backend_part_no) like '%".$searchWord."%'");
         $data['categories'] = DB::select("SELECT * FROM category WHERE lower(title) like '%".$searchWord."%'");
         $data['subcategories'] = DB::select("SELECT * FROM sub_category1 WHERE lower(title) like '%".$searchWord."%'");

      }
      return View::make("storefront.search",$data);
    }
    public function Print(Request $request){
      $data = $this->GenOrderData(["id"=>$request->id]);
      $data['print']='print';
      $data['redirect']=URL::to('/')."/account";
      return View::make("invoice.invoice",$data);
    }



}
