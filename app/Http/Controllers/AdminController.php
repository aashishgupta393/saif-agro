<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use View;
use App\AgroAdmin;
use App\Companies;
use App\Customers;
use App\Orders;
use App\Items;
use App\Enquiry;
use App\SubCategories;
use App\SubCategories2;
use App\Categories;
use App\Products;
use DB;
use URL;
use Mail;
use File;
use App\Exports\TallyOrders;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;


class AdminController extends Controller
{


  public function index(Request $request)
  {
    $data['page']='dashboard';
    return View::make('admin.dashboard',$data);
  }

  public function Login(Request $request)
  {
    return redirect()->route('admin/');
  }

  public function ValidateAdminLogin(Request $request)
  {
    $email=$request->email;
    $password=$request->password;
    $company=$request->company;
    $isvalidemail = DB::table('companies')->where(['company_email'=>$email])->count();
    if($isvalidemail == 0){
      return response(['code'=>100,'msg'=>'User Name does not Exists'])->header('Content-Type', 'application/json');
      exit;
    }
    $ispwdvalid = DB::table('companies')->where(['company_email'=>$email,'password'=>$password,'id'=>$company]);
    if($ispwdvalid->count() == 0){
      return response(['code'=>100,'msg'=>'User Name does not Exists'])->header('Content-Type', 'application/json');
      exit;
    }else{
      $data = $ispwdvalid->first();
      Session::put('admin', $data);
      return response(['code'=>200,'msg'=>'Username And Password Validated'])->header('Content-Type', 'application/json');
    }
  }

  public function UpdateCompany(Request $request)
  {
    $company_id = Session::get('admin')->id;
    $update = Companies::where('id', $company_id)->update(['payment_mode' => $request->status]);
    return response(['code'=>200,'msg'=>'Payment Status Updated'])->header('Content-Type', 'application/json');
  }
  public function UpdateCompanyDetails(Request $request)
  {
    $company_id = Session::get('admin')->id;
    $updarray = [
              "company_name" => $request->company_name,
              "company_email" => $request->company_email,
              "company_contact_no" => $request->company_contact_no,
              "GSTIN" => $request->GSTIN,
              "address" => $request->address,
              "address1" => $request->address1,
              "city" => $request->city,
              "state" => $request->state,
              "zip_code" => $request->zip_code
            ];
    if(!empty($request->password)){
        $updarray['password']=$request->password;
    }
    $update = Companies::where('id', $company_id)
                        ->update($updarray);
    $data = Companies::where('id', $company_id)->first();
    Session::put('admin', $data);
    return redirect()->back();
  }
  public function UpdateUserStatus(Request $request)
  {
    $customer_id = $request->user_id;
    $update = Customers::where('id', $customer_id)->update(['status' => $request->status]);
    if($request->status == 1){
      $customerData = Customers::where('id', $customer_id)->first();
      $data = ['name'=>$customerData->username,'email'=>$customerData->email,'subject'=>"Approval mail by Saif Agro Admin",'verification_url'=>URL::to("/")."/sign-in/",];
      $message = 'Your Saif Agro  Account Has Been Approved. Please Login to your store from URL : '.URL::to("/").'/sign-in/';
      sendsms($message,$customerData->contact_no);
      Mail::send('mails.account_approved', $data, function($message)use($data){
        $message->to($data['email'],$data['name'])->subject($data['subject']);
        $message->from('admin@saifagrogroup.com','Admin');
      });
    }
    return response(['code'=>200,'msg'=>'Approved'])->header('Content-Type', 'application/json');
  }

  public function Delete(Request $request)
  {
    $delete = DB::table($request->table)->where(['id'=>$request->id])->delete();
    if($delete){
        return response(['code'=>200,'msg'=>'Data Deleted'])->header('Content-Type', 'application/json');
    }else{
        return response(['code'=>200,'msg'=>'Something Went Wrong'])->header('Content-Type', 'application/json');
    }
  }

  public function UpdateProduct(Request $request)
  {
    $images =[];
    $productImages=[];
    for ($i=0; $i < 3; $i++) {
      $image = $request->hasFile('image'.$i);
      if($image){
        $file = $request->file('image'.$i);
        $fileName = "Product--".rand().".".$file->getClientOriginalExtension();
        $destinationPath = 'public/uploads';
        if($file->move($destinationPath,$fileName)){
          $productImages[] = 'public/uploads/'.$fileName;
        }
      }else{
        if(isset($request['oimg'.$i])){
          $productImages[]=$request['oimg'.$i];
        }
      }
    }
    $saveArray=["cat_id"=>$request->cat_id,"sub_cat1"=>$request->sub_cat1,"sub_cat2"=>$request->sub_cat2,"product_title"=>$request->product_title,"product_amount"=>$request->product_amount,"product_description"=>$request->product_description,"hsn_code"=>$request->hsn_code,"tax_rate"=>$request->tax_rate,"part_no"=>$request->part_no,"backend_part_no"=>$request->backend_part_no,"brand_name"=>$request->brand_name,];
    $variants=[];
  foreach ($request->variant_title as $key => $variant) {
    $singleVariant=[];
    $singleVariant['titile']=$variant;
    $singleVariant['price']=$request->variant_price[$key];
    $singleVariant['part_no']=$request->variant_part_no[$key];
    $variantImage = $request->hasFile('variant_image'.$key);
    if($variantImage){
      $file = $request->file('variant_image'.$i);
      $fileName = "Variant--".rand().".".$file->getClientOriginalExtension();
      $destinationPath = 'public/uploads';
      try {
        $file->move($destinationPath,$fileName);
        $singleVariant['image'] = 'public/uploads/'.$fileName;
      } catch (\Exception $e) {
        $singleVariant['image']="";
      }
    }else{
      if(isset($request->variant_images[$key])){
        $singleVariant['image']=$request->variant_images[$key];
      }else{
        $singleVariant['image']="";
      }
    }
    $variants[]=$singleVariant;
  }
  if(!empty($variants)){
    $saveArray['variants'] = json_encode($variants);
  }
  if(!empty($productImages)){
    $saveArray['images'] = json_encode($productImages);
  }
    Products::where(['id'=>$request->p_id])->update($saveArray);
    return redirect()->back();
  }


  public function AddProduct(Request $request)
  {
    $images =[];
    $productImages=[];
    for ($i=0; $i < 3; $i++) {
      $image = $request->hasFile('image'.$i);
      if($image){
        $file = $request->file('image'.$i);
        $fileName = "Product--".rand().".".$file->getClientOriginalExtension();
        $destinationPath = 'public/uploads';
        if($file->move($destinationPath,$fileName)){
          $productImages[] = 'public/uploads/'.$fileName;
        }
      }else{
        if(isset($request['oimg'.$i])){
          $productImages[]=$request['oimg'.$i];
        }
      }
    }
    $saveArray=[
      "cat_id"=>$request->cat_id,
      "sub_cat1"=>$request->sub_cat1,
      "sub_cat2"=>$request->sub_cat2,
      "product_title"=>$request->product_title,
      "product_amount"=>$request->product_amount,
      "product_description"=>$request->product_description,
      "hsn_code"=>$request->hsn_code,
      "tax_rate"=>$request->tax_rate,
      "part_no"=>$request->part_no,
      "backend_part_no"=>$request->backend_part_no,
      "brand_name"=>$request->brand_name,
    ];
    $variants=[];
  foreach ($request->variant_title as $key => $variant) {
    $singleVariant=[];
    $singleVariant['titile']=$variant;
    $singleVariant['price']=$request->variant_price[$key];
    $singleVariant['part_no']=$request->variant_part_no[$key];
    $variantImage = $request->hasFile('variant_image'.$key);
    if($variantImage){
      $file = $request->file('variant_image'.$i);
      $fileName = "Variant--".rand().".".$file->getClientOriginalExtension();
      $destinationPath = 'public/uploads';
      try {
        $file->move($destinationPath,$fileName);
        $singleVariant['image'] = 'public/uploads/'.$fileName;
      } catch (\Exception $e) {
        $singleVariant['image']="";
      }
    }else{
      if(isset($request->variant_images[$key])){
        $singleVariant['image']=$request->variant_images[$key];
      }else{
        $singleVariant['image']="";
      }
    }
    $variants[]=$singleVariant;
  }

  $company_id = Session::get('admin')->id;
  $saveArray['company_id'] = $company_id;
  $six_digit_random_number = mt_rand(100000, 999999);
  $saveArray['product_id'] = "COMP--".$company_id."--".$six_digit_random_number;

  if(!empty($variants)){
    $saveArray['variants'] = json_encode($variants);
  }
  if(!empty($productImages)){
    $saveArray['images'] = json_encode($productImages);
  }
  DB::table('products')->insert($saveArray);
  return redirect()->back();
}

  public function UploadBanner(Request $request)
  {
      $file = $request->file('banner');
      $fileName = "Banner--".rand().".".$file->getClientOriginalExtension();
      $destinationPath = 'public/uploads';
      if($file->move($destinationPath,$fileName)){
        $saveArray=[
          "path"=>'public/uploads/'.$fileName,
        ];
        echo "Banner File Uploaded";
        DB::table('banner')->insert($saveArray);
      }else{
        echo "Something Went Wrong Please Try Later";
      }
      return redirect()->back();
  }

  public function AddCategory(Request $request)
  {
      $company_id = Session::get('admin')->id;
      $file = $request->file('banner');
      $fileName = $file->getClientOriginalName();
      $destinationPath = 'public/uploads';
      if($file->move($destinationPath,$fileName)){
        $table = $request->table;
        $saveArray=[
          "title"=>$request->cat_name,
          "image"=>$fileName,
          "company_id"=>$company_id,
        ];
        if(isset($request->cat_id)){
          $saveArray['cat_id'] = $request->cat_id;
        }
        if(isset($request->sub_cat1)){
          $saveArray['sub_cat1'] = $request->sub_cat1;
        }
        DB::table($table)->insert($saveArray);
      }else{
        echo "Something Went Wrong Please Try Later";
      }
      return redirect()->back();
  }

  public function UpdateCatgory(Request $request)
  {
      $saveArray=["title"=>$request->title];
      $file = $request->file('image');
      if ($file !=null){
        $fileName = $file->getClientOriginalName();
        $destinationPath = 'public/uploads';
        if($file->move($destinationPath,$fileName)){
          $saveArray["image"]=$fileName;
        }
      }
        switch ($request->table) {
          case 'category':
              $update = Categories::where('id', $request->id)->update($saveArray);
            break;
          case 'sub_category1':
              $update = SubCategories::where('id', $request->id)->update($saveArray);
            break;
          case 'sub_category2':
              $update = SubCategories2::where('id', $request->id)->update($saveArray);
            break;

          default:
            // code...
            break;
        }

      return redirect()->back();
  }
  public function GenOrderData($pass)
  {
    $orderId = $pass['id'];
    $data['id'] = $orderId;
    $data['order'] = Orders::where('id', $orderId)->first();
    $data['customer'] = Customers::where('id', $data['order']->customer_id)->first();
    $data['company'] = Companies::where('id', $data['order']->company_id)->first();
    $data['items'] = Items::with(['product'])->where('orders_id', $orderId)->get();
    $data['email'] = $data['customer']->email;
    $data['subject'] = 'Order Confirmation';
    $data['name'] = $data['customer']->username;
    return $data;
  }
  public function Print(Request $request){
    $data = $this->GenOrderData(["id"=>$request->id]);
    $data['print']='print';
    $data['redirect']=URL::to('/')."/admin/order-list";
    return View::make("invoice.invoice",$data);
  }

  public function UploadCsv(Request $request)
  {

      $file = $request->file('csv');
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $tempPath = $file->getRealPath();
      $fileSize = $file->getSize();
      $valid_extension = array("csv");
      $data = array_map('str_getcsv', file($tempPath));
       $company_id = Session::get('admin')->id;
       $maxFileSize = 2097152;
       if(in_array(strtolower($extension),$valid_extension)){
         if($fileSize <= $maxFileSize){
           foreach ($data as $key => $singleRow) {

             if($key > 1){
               $six_digit_random_number = mt_rand(100000, 999999);
               $productImages=[];
                 if(!empty($singleRow[9])){
                   $productImages[]='public/uploads/'.$singleRow[9];
                 }
                 if(!empty($singleRow[10])){
                   $productImages[]='public/uploads/'.$singleRow[10];
                 }
                 if(!empty($singleRow[11])){
                   $productImages[]='public/uploads/'.$singleRow[11];
                 }
                 $variants=[];
                 if(!empty($singleRow[13])){
                   $variants[] = ['titile'=>$singleRow[13],'image'=>(!empty($singleRow[14]) ? 'public/uploads/'.$singleRow[14] : ''),'price'=>$singleRow[15],'part_no'=>$singleRow[16]];
                 }
                 if(!empty($singleRow[17])){
                   $variants[] = ['titile'=>$singleRow[17],'image'=>(!empty($singleRow[18]) ? 'public/uploads/'.$singleRow[18] : ''),'price'=>$singleRow[19],'part_no'=>$singleRow[20]];
                 }
                 if(!empty($singleRow[21])){
                    $variants[] = ['titile'=>$singleRow[21],'image'=>(!empty($singleRow[22]) ? 'public/uploads/'.$singleRow[22] : ''),'price'=>$singleRow[23],'part_no'=>$singleRow[24]];
                 }
                 if(!empty($singleRow[25])){
                    $variants[] = ['titile'=>$singleRow[25],'image'=>(!empty($singleRow[26]) ? 'public/uploads/'.$singleRow[26] : ''),'price'=>$singleRow[27],'part_no'=>$singleRow[28]];
                 }
                 if(!empty($singleRow[29])){
                    $variants[] = ['titile'=>$singleRow[29],'image'=>(!empty($singleRow[30]) ? 'public/uploads/'.$singleRow[30] : ''),'price'=>$singleRow[31],'part_no'=>$singleRow[32]];
                 }
                 if(!empty($singleRow[33])){
                      $variants[] = ['titile'=>$singleRow[33],'image'=>(!empty($singleRow[34]) ? 'public/uploads/'.$singleRow[34] : ''),'price'=>$singleRow[35],'part_no'=>$singleRow[36]];
                 }
                 $amount = $singleRow[5];
                 $amounts = explode('-',$singleRow[5]);
                 if(count($amounts) > 1){
                   $amount = $amounts[1];
                 }
               $saveArray=[
                 "product_id"=>"COMP--".$company_id."--".$six_digit_random_number.$key,
                 "cat_id"=>$singleRow[1],
                 "sub_cat1"=>$singleRow[2],
                 "sub_cat2"=>$singleRow[3],
                 "product_title"=>$singleRow[4],
                 "product_amount"=>$amount,
                 "product_description"=>$singleRow[8],

                 "hsn_code"=>$singleRow[6],
                 "tax_rate"=>$singleRow[7],
                 "part_no"=>$singleRow[12],


                 "backend_part_no"=>$singleRow[37],

                 "brand_name"=>$singleRow[38],
                 "company_id"=>$company_id,
               ];
               if(!empty($variants)){
                 $saveArray['variants'] = json_encode($variants);
               }
               if(!empty($productImages)){
                 $saveArray['images'] = json_encode($productImages);
               }
               DB::table('products')->insert($saveArray);
             }
           }
           return redirect()->back();
         }else{
            return redirect()->back();
         }
       }else{
         return redirect()->back();
       }
  }


  public function Enquiries(Request $request)
  {
    $data['page']='enquiries';
    return View::make("admin.enquiries",$data);
  }

  public function DropZone(Request $request)
  {
    $file = $request->file('file');
    $fileName = $file->getClientOriginalName();
    if(File::exists(public_path('uploads/'.$fileName))){
      File::delete(public_path('uploads/'.$fileName));
    }
    $fileName = $file->getClientOriginalName();
    $destinationPath = 'public/uploads';
    if($file->move($destinationPath,$fileName)){
      echo json_encode(['code'=>200,'msg'=>URL::to('/')."/public/uploads/".$fileName]);
    }else{
      echo json_encode(['code'=>100,'msg'=>"File Not Uploaded"]);
    }
  }
  public function DeleteImage(Request $request)
  {
      $filename =  $request->filename;
      $path=public_path().'/uploads/'.$filename;
      if (file_exists($path)) {
        unlink($path);
      }
      return $filename;
  }

  public function UpdateOrder(Request $request)
  {
    $orderID = $request->order_id;
    if(isset($request->payment_id)){
      $update = ['payment_id'=>$request->payment_id,'pay_mode'=>$request->pay_mode,'payment_status' => 'paid','pay_date' => date('Y-m-d H:i:s'),];
      $return = ['code'=>200,'msg'=>"Order Marked as paid"];
    }else{
      if(isset($request->shipping_date)){
        $update = ['shipping_date'=>$request->shipping_date,'shipping_providor'=>$request->shipping_providor,'tracking_code' => $request->tracking_code,'tracking_url' => $request->tracking_url,];
        $file = $request->file('lr_copy');
        $fileName = "LR--".rand().".".$file->getClientOriginalExtension();
        $destinationPath = 'public/uploads';
        if($file->move($destinationPath,$fileName)){
          $update['lr_path'] = 'public/uploads/'.$fileName;
        }
        $return = ['code'=>200,'msg'=>"Shipping Details Added For the Order"];
        $tracking = true;
      }else{
          if(isset($request->e_way_bill)){
            $update['e_way_bill'] = $request->e_way_bill;
          }else{
            $file = $request->file('pdf_invoice_path');
            $fileName = "Tally--".rand().".".$file->getClientOriginalExtension();
            $destinationPath = 'public/invoices';
            if($file->move($destinationPath,$fileName)){
              $update['pdf_invoice_path'] = 'public/invoices/'.$fileName;
            }
          }
      }
    }
    DB::table('tbl_order')->where('id',$request->order_id)->update($update);

    if(isset($tracking)){
      $order = Orders::with(['customer'])->where(['id'=>$orderID])->first();
      $message = "Hi ".$order->customer->username.", your package has now been shipped. View tracking details: LR No.".$order->tracking_code."  LR Receipt : ".URL::to("/")."/".$order->lr_path;
      sendsms($message,$order->customer->contact_no);
    }

    return redirect()->back();
  }

  public function AllFiles(Request $request)
  {
       $company_id = Session::get('admin')->id;
      $files = File::allFiles(public_path('uploads'));
      $products = Products::where(['company_id'=>$company_id])->get();
      $myProduts=[];
      foreach ($products as $product) {
        $pimages =[];
        if(!empty($product->images)){
          $images = json_decode($product->images);
          foreach ($images as  $image) {
            $pimages[]=str_replace('public/uploads/','',$image);
          }
        }
        if(!empty($product->variants)){
          $variants = json_decode($product->variants);
          foreach ($variants as  $variant) {
            if(!empty($variant->image)){
              $pimages[]=str_replace('public/uploads/','',$variant->image);
            }
          }
        }
        $myProduts[$product->id]=['images'=>$pimages,'product'=>$product];
      }

     $myFiles=[];
    foreach ($files as $file) {
      $file->company_id = "";
      $fileName = pathinfo($file)['basename'];
      $isProduct = false;
      foreach ($myProduts as $singlep) {
        if(in_array($fileName,$singlep['images'])){
          $isProduct = $singlep['product'];
        }
      }

      if($isProduct){
        $file->linked_with = 'product';
        $file->link = $isProduct;
        $file->company_id =$isProduct->company_id;
      }
      $isCategory = Categories::where('image', 'like', '%'.$fileName.'%')->first();
      if($isCategory){
        $file->linked_with = 'category';
        $file->link = $isCategory;
        $file->company_id =$isCategory->company_id;
      }
      $isSubCategory1 = SubCategories::where('image', 'like', '%'.$fileName.'%')->first();
      if($isSubCategory1){
        $file->linked_with = 'subcategory1';
        $file->link = $isSubCategory1;
        $file->company_id =$isSubCategory1->company_id;
      }
      $isSubCategory2 = SubCategories2::where('image', 'like', '%'.$fileName.'%')->first();
      if($isSubCategory2){
        $file->linked_with = 'subcategory2';
        $file->link = $isSubCategory2;
        $file->company_id =$isSubCategory2->company_id;
      }
      if($file->company_id == $company_id){
        $myFiles[]=$file;
      }
    }
    $data['page']='files';
    $data['files']=$myFiles;
    $data['appUrl']=URL::to('/');
    return View::make("admin.files.files",$data);
  }



  public function Export(Request $request)
  {
      $headings = ['Sl No','Date','Voucher Type','Voucher No','E Way Bill No','Party Name','Adress','Contact Person name','Contact No','Email','GSTIN','Item Name','Qty','Rate','Total Amount','Disc%','Dis amt','Taxble Amount','SGST %','SGST amt','CGST %','CGST Amt','IGST %','IGST Amt','Grand Total',];
      $month = $request->month;
      $year = $request->year;
      $orders = Orders::with(['customer','items','company'])->where(['company_id'=>Session::get('admin')->id,'payment_status'=>'paid'])->whereMonth('created_at',$month)->whereYear('created_at',$year)->orderBy('id', 'DESC')->get();
      $output = $this->ReturnOrderData($orders);
      return Excel::download(new TallyOrders($output,$headings), 'Orders--'.$month.'--'.$year.'.xlsx');
  }

  public function ReturnOrderData($orders)
  {
    $output=[];
      if($orders){
        foreach ($orders as $ok=>$order) {
          $customer = $order->customer;
            foreach ($order->items as $item) {
              $gstAmount  = $item->price - ($item->price*(100/(100+18)));
              $netPrice = $item->price - $gstAmount;
              $itemGST = $gstAmount*$item->qty;
              $isSGST = false;
              if(strtolower($order->company->state) == strtolower($customer->state)){
                $isSGST = true;
              }

              $output[]=[
                'Sl No' => $ok+1,
                'Date' => date('d.m.Y',strtotime($order->created_at)),
                'Voucher Type' => "Sale",
                'Voucher No' => $order->order_no,
                'E Way Bill No' => $order->e_way_bill,
                'Party Name' => $customer->username,
                'Adress' =>  $customer->company_address.",".$customer->city.",".$customer->zip_code,
                'Contact Person name' => $customer->username,
                'Contact No' => $customer->contact_no,
                'Email' => $customer->email,
                'GSTIN' => $customer->gst_no,
                'Item Name' => $item->product_title,
                'Qty' => $item->qty,
                'Rate' => round($netPrice),
                'Total Amount' => round($netPrice*$item->qty),
                'Disc%' => "",
                'Dis amt' => "",
                'Taxble Amount'  => round($netPrice*$item->qty),
                'SGST %'  => "9%",
                'SGST amt'  => (($isSGST) ? round($itemGST/2): ""),
                'CGST %'  => "9%",
                'CGST Amt'  => (($isSGST) ? round($itemGST/2): ""),
                'IGST %'  => (!($isSGST) ? "18%": ""),
                'IGST Amt'  => (!($isSGST) ? round($itemGST): ""),
                'Grand Total'  =>($item->qty * $item->price),
              ];
            }
        }
      }
      return $output;
  }

  public function ExportSingleOrder(Request $request)
  {
      $headings = ['Sl No','Date','Voucher Type','Voucher No','E Way Bill No','Party Name','Adress','Contact Person name','Contact No','Email','GSTIN','Item Name','Qty','Rate','Total Amount','Disc%','Dis amt','Taxble Amount','SGST %','SGST amt','CGST %','CGST Amt','IGST %','IGST Amt','Grand Total',];

      $orders = Orders::with(['customer','items','company'])
                        ->where(['id'=>$request->id])
                        ->get();
      $output = $this->ReturnOrderData($orders);
      return Excel::download(new TallyOrders($output,$headings), 'Orders-Single--'.$request->id.'.xlsx');
  }

  public function RenderEditProductpage(Request $request){
    $product = Products::where('id', $request->pid)->first();
     $data = ['appUrl'=>URL::to('/'),'page'=>'product','product'=>$product,'subpage'=>'add-product'];
    return View::make("admin.products.edit_product",$data);
  }


  public function LoadOrders(Request $request)
  {
    $data = ['appUrl'=>URL::to('/'),'page'=>'orders'];
    if(isset($request->type)){
      $data['type']=$request->type;
      $data['from']=$request->from;
      $data['to']=$request->to;
    }
    return View::make("admin.orders.orders",$data);
  }




}
