<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\AgroAdmin;
use App\Companies;
use App\Customers;
use Illuminate\Support\Facades\Response;
use Mail;
use Session;
use URL;
use Razorpay\Api\Api;



class PaymentController extends Controller
{
      public function create()
      {
          return view('storefront.payment');
      }
      public function payment(Request $request)
      {
          dd($request->all());
          $input = $request->all();
          $api = new Api("rzp_test_6Tq29Qwt1CLTRs", "EmVu6FNUyrluXN3iAHjUhcYM");
          $payment = $api->payment->fetch($input['razorpay_payment_id']);
          if(count($input)  && !empty($input['razorpay_payment_id'])) {
              try {
                  $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount']));
              } catch (\Exception $e) {
                  return  $e->getMessage();
                  Session::put('error',$e->getMessage());
                  return redirect()->back();
              }
          }

          Session::put('success', 'Payment successful');
          return redirect()->back();
      }


}
