<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
  protected $table = 'sub_category1';
  public function products()
   {
       return $this->hasMany('App\Products','sub_cat1');
   }
  public function subcategories()
   {
       return $this->hasMany('App\SubCategories2','sub_cat1');
   }
  public function subcategories2()
   {
       return $this->hasMany('App\SubCategories2','sub_cat1');
   }
  public function category()
   {
       return $this->belongsTo('App\Categories','cat_id');
   }

}
