<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories2 extends Model
{
  protected $table = 'sub_category2';
  public function products()
   {
       return $this->hasMany('App\Products','sub_cat2');
   }
  public function category()
   {
       return $this->belongsTo('App\Categories','cat_id');
   }
  public function subcat1()
   {
       return $this->belongsTo('App\SubCategories','sub_cat1');
   }

}

?>
