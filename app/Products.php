<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    public function category() {
     return $this->belongsTo('App\Categories','cat_id');
   }
    public function subcategory1() {
      return $this->belongsTo('App\SubCategories','sub_cat1');
    }
    public function subcategory2() {
      return $this->belongsTo('App\SubCategories2','sub_cat2');
    }
    public function company() {
      return $this->belongsTo('App\Companies','company_id');
    }
    

}
