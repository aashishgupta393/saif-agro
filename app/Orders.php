<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Orders extends Model
{

  protected $table = 'tbl_order';

  protected $casts = [
      'shipping_address' => 'array',
      'created_at' => 'datetime:d/m/Y',
      'shipping_date' => 'datetime:d/m/Y',
  ];

  public function customer()
   {
       return $this->belongsTo('App\Customers');
   }

  public function items()
   {
       return $this->hasMany('App\Items');
   }
  public function company()
   {
       return $this->belongsTo('App\Companies','company_id');
   }


}
