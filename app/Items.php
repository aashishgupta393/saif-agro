<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{

  protected $table = 'tbl_order_item';

  public function product() {
   return $this->belongsTo('App\Products','product_id');
 }

}
