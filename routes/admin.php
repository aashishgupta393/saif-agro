<?php

use Illuminate\Support\Facades\Route;
use Session;
use DB;
use URL;
use Illuminate\Http\Request;

Route::post('admin/validate', 'AdminController@ValidateAdminLogin')->name('validate');
Route::get('admin/logout', function(){
 Session::forget('admin');
 return redirect('/admin/');
});


Route::group(['prefix' => 'admin','middleware' => 'isadmin'], function(){

  Route::post('approve-user', 'AdminController@UpdateUserStatus');
  Route::post('update-product', 'AdminController@UpdateProduct');
  Route::post('save-product', 'AdminController@AddProduct');
  Route::post('update-payment-status', 'AdminController@UpdateCompany');
  Route::post('add-category', 'AdminController@AddCategory');
  Route::post('dropzone', 'AdminController@DropZone');
  Route::post('delete-image', 'AdminController@DeleteImage');
  Route::post('upload-banner', 'AdminController@UploadBanner');
  Route::post('submit-upload', 'AdminController@UploadCsv');
  Route::post('updatecategory', 'AdminController@UpdateCatgory');
  Route::post('UpdateOrder', 'AdminController@UpdateOrder');
  Route::post('update-company-details', 'AdminController@UpdateCompanyDetails');


  Route::get("company-list", function(){
    return View::make("admin.company.company_list",['page'=>'company-list']);
  });

  Route::get("upload-product", function(){
    $data = ['page'=>'product','subpage'=>'import-products'];
    return View::make("admin.products.upload_csv",$data);
  });

  Route::get("add-product", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'product','subpage'=>'add-product'];
    return View::make("admin.products.add_product",$data);
  });
  Route::get("product-list", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'product','subpage'=>'products'];
    return View::make("admin.products.product_list",$data);
  });
  Route::get("category", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'product','subpage'=>'category'];
    return View::make("admin.products.category",$data);
  });
  Route::get("subcategories", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'product','subpage'=>'subcategory'];
    return View::make("admin.products.subcategories",$data);
  });
  Route::get("banner", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'settings'];
    return View::make("admin.settings.banner",$data);
  });

  Route::get('order-list', 'AdminController@LoadOrders');

  Route::get("upload-image", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'product','subpage'=>'upload-image'];
    return View::make("admin.products.dropzone",$data);
  });
  Route::get("edit-company", function(){
    $data = ['appUrl'=>URL::to('/'),'page'=>'company-list'];
    return View::make("admin.company.edit_company",$data);
  });
  Route::get("user-list", function(){
    return View::make("admin.users.users_list",['page'=>'users-list']);
  });

  Route::get('/', 'AdminController@index');
  Route::get('login', 'AdminController@Login');
  Route::get('delete', 'AdminController@Delete');
  Route::get('export', 'AdminController@Export');
  Route::get('single-order-export', 'AdminController@ExportSingleOrder');
  Route::get("invoice-print", 'AdminController@Print');
  Route::get("edit-product", 'AdminController@RenderEditProductpage');
  Route::get('files', 'AdminController@AllFiles');
  Route::get('enquiries', 'AdminController@Enquiries');

});
