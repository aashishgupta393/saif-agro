<?php
use Illuminate\Support\Facades\Route;
use Session;
use DB;
use URL;
use Illuminate\Http\Request;

Route::group(['prefix' => 'api'], function(){
  Route::post('/send-otp', 'ApiController@SedOtp');
  Route::post('/register', 'ApiController@RegisterUser');
  Route::post('/login', 'ApiController@Validateuser');
  Route::get('/home', 'ApiController@HomePageData');
  Route::get('/subcategories', 'ApiController@GetSubcategories1');
  Route::get('/subcategories2', 'ApiController@GetSubcategories2');
  Route::get('/orders', 'ApiController@MyOrders');
  Route::get('/product-by-sub-cat', 'ApiController@ProductsBySubCat');
  Route::get('/product-details', 'ApiController@ProductById');
  Route::get('/all-products', 'ApiController@AllProducts');
  Route::post('/send-forgot-otp', 'ApiController@SendOTPForForgot');
  Route::post('/update-password', 'ApiController@UpdatePassword');
  Route::post('/create-order', 'ApiController@CreateOrder');
  Route::get('/get-payment-keys', 'ApiController@GetKeys');
  Route::get('/get-user-orders', 'ApiController@GetUserOrders');
  Route::post('/submit-enquiry', 'ApiController@SubmitEnquiry');
  Route::get('/search', 'ApiController@Search');
  Route::get('/print', 'ApiController@Print');
  Route::post('/update-profile', 'ApiController@UpdateCustomerProfile');
});
