<?php
use Illuminate\Support\Facades\Route;
use Session;
use DB;
use URL;
use Illuminate\Http\Request;
Route::get('/',function(){
  $data['appUrl']=URL::to('/');
  return View::make("storefront.home",$data);
});

Route::get('/invoice-print','StoreFront@Print');
Route::get('/search','StoreFront@Search');
Route::post('register-user', 'StoreFront@RegisterUser');
Route::post('send-otp', 'StoreFront@SendOTP');
Route::post('validate-otp', 'StoreFront@ValidateOTP');

Route::post('validate-user', 'StoreFront@Validateuser');
Route::post('update-profile', 'StoreFront@UpdateProdile');

Route::get('/test', 'StoreFront@test');

Route::get('/add', 'StoreFront@addToCart');
Route::get('/remove', 'StoreFront@deleteCart');
Route::get('/logout', function() {
  Session::forget('current_user');
  if(!Session::has('current_user')){
      return redirect()->route('sign-in');
   }
 });

Route::post('payment', 'StoreFront@CreateOrder')->name('payment');

Route::get('/register',function(){
  $cts=DB::table('cities')->get();
  $cities=[];
  $states = DB::table('states')->orderBy('name','asc')->get();
  return View::make("storefront.register",['cities'=>$states]);
});
Route::get('/verify/{any}','StoreFront@VerifyEmail');
Route::get('/sign-in',function(){
  return View::make("storefront.sign_in");
})->name('sign-in');

Route::get('/contact',function(){
  return View::make("storefront.contact");
});
Route::get('/contactapp',function(){
  return View::make("storefront.contactapp");
});

Route::get('/privacy',function(){
  return View::make("storefront.privacy");
});
Route::get('/about_us',function(){
  return View::make("storefront.about_us");
});
Route::get('/term_service',function(){
  return View::make("storefront.term_service");
});
Route::get('/refunds_policys',function(){
  return View::make("storefront.refunds_policys");
});
Route::get('/category',function(){
  $data['appUrl']=URL::to('/');
  return View::make("storefront.category",$data);
});

Route::get('/subcategories',function(Request $request){
  $data['appUrl']=URL::to('/');
  $data['cat_id'] = $request->cat_id;
  return View::make("storefront.subcategories",$data);
});

Route::get('/subcategories2',function(Request $request){
  $data['appUrl']=URL::to('/');
  $data['sub_cat_id'] = $request->sub_cat_id;
  return View::make("storefront.subcategories2",$data);
});

Route::get('/products',function(Request $request){
  $data['appUrl']=URL::to('/');
  if(isset($request->cat_id)){
    $data['cat_id'] = $request->cat_id;
  }
  if(isset($request->sub_cat_id)){
    $data['sub_cat_id'] = $request->sub_cat_id;
  }
  if(isset($request->sub_cat2_id)){
    $data['sub_cat2_id'] = $request->sub_cat2_id;
  }
  return View::make("storefront.products",$data);
});


Route::get('/about',function(){
  return View::make("storefront.about");
});
Route::get('/product',function(Request $request){
    $pid = $request->id;
    $data['details']=DB::table('products')->where(['id'=>$pid])->first();
    $data['appUrl']=URL::to('/');
    $data['page']='product';
    return View::make("storefront.product",$data);
});
Route::get('/checkout',function(){
  $data['appUrl']=URL::to('/');
  $cts=DB::table('cities')->get();
  $cities=[];
  foreach ($cts as $ct) {if(!isset($cities[$ct->state])){$cities[$ct->state] = [];}$cities[$ct->state][] = $ct;}
  $data['cities']=$cities;
  return View::make("storefront.checkout",$data);
});

Route::get('/faq',function(){
  return View::make("storefront.faq");
});
Route::get('/account',function(){
  $states = DB::table('states')->orderBy('name','asc')->get();
  return View::make("storefront.account",['cities'=>$states]);
})->name('account');
Route::get('/forgot-password',function(){
  return View::make("storefront.forgot");
});
